###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import absolute_import

import os, sys, signal
"""
Create PID PDFs from calibration ntuples, as well as some auxiliary
PDFs (for systematic and stat. error evaluation)
"""

cwd = os.getcwd()

from . import ConfigMC


def signal_handler(signal, frame):
    print('Exiting')
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)

if len(sys.argv) > 1:
    configname = sys.argv[1]
else:
    print(
        "Usage: CreatePIDPdf [config] [option1:option2:...] [dataset1:dataset2:...]"
    )
    print("  configs are: ")
    for i in sorted(ConfigMC.configs.keys()):
        print("    ", i)
    print("  options are: ")
    print("     dry - Dry run (do not submit jobs, just print command lines)")
    print(
        "     continue - Continue unfinished jobs (only run on the datasets where control plots are missing in eos)"
    )
    print(
        "     polarity - Create separate templates for positive and negative track polarities"
    )
    sys.exit(0)

opt = ""
if len(sys.argv) > 2:
    opt = sys.argv[2]
if "polarity" in opt.split(":"):
    file_list2 = []
    for f in file_list:
        file_list2 += [f + "_M"]
        file_list2 += [f + "_P"]
    file_list = file_list2

config = ConfigMC.configs[configname]
eosdir = ConfigMC.eosdir
eosrootdir = ConfigMC.eosrootdir
tmpdir = ConfigMC.tmpdir

scale_default = config['scale_default']
scale_syst = config['scale_syst']
scale_pid = config['scale_pid']
var = "PID"
toystat = config['toystat']
controlstat = config['controlstat']
nbootstrap = config['nbootstrap']
samplename = config['sample']
sample = ConfigMC.samples[samplename]
datasets = sample["datasets"]

if len(sys.argv) > 3:
    file_list = sys.argv[3].split(":")
else:
    file_list = list(datasets.keys())

limits = None
if 'limits' in config:
    limits = config['limits']

scr = "bsub -q 2nw lb-run -c x86_64-slc6-gcc62-opt Urania/v7r0 python %s/PIDPdf.py" % (
    cwd)

ready_list = []
if "continue" in opt.split(":"):
    print("ls -1 %s/%s/control/ > all.txt" % (eosdir, configname))
    os.system("ls -1 %s/%s/control/ > all.txt" % (eosdir, configname))
    f = open("all.txt")
    for l in f:
        ready_list += ["control/" + l.strip()]
    f.close()
    print(ready_list)

for filename in file_list:

    infile = configname + "/" + filename + ".root"
    outdir = eosrootdir + "/" + configname + "/"

    nominalfile = filename + "_distrib.root"
    controlfile = "control/" + filename + "_control.root"
    figfile = "plots/" + filename + "_control.png"

    command = "%s -s %f -i %f -v %s -o %s -t %d -p %d -b %s -c %s -f %s %s/%s" % \
            (scr, scale_default, scale_pid, var, outdir, toystat, controlstat, nominalfile, controlfile, figfile, eosrootdir, infile)
    if limits:
        command += " -l %f -u %f" % (limits[0], limits[1])

    if controlfile not in ready_list:
        print(command)
        if ("dry" not in opt.split(":")): os.system(command)

    systfile = filename + "_syst_1.root"
    controlfile = "control/" + filename + "_syst_1_control.root"
    figfile = "plots/" + filename + "_syst_1_control.png"

    if controlfile not in ready_list and scale_syst:
        command = "%s -s %f -i %f -v %s -o %s -t %d -p %d -b %s -c %s -f %s %s/%s" % \
              (scr, scale_syst, scale_pid, var, outdir, toystat, controlstat, systfile, controlfile, figfile, eosrootdir, infile)
        if limits:
            command += " -l %f -u %f" % (limits[0], limits[1])
        print(command)
        if ("dry" not in opt.split(":")): os.system(command)

    for i in range(0, nbootstrap):

        statfile = filename + "_stat_%d.root" % i
        tmpfile = "%s/%s_stat_%d_tmp.root" % (tmpdir, filename, i)
        controlfile = "control/" + filename + "_stat_%d_control.root" % i
        figfile = "plots/" + filename + "_stat_%d_control.png" % i
        command = "%s -a %s -r %d -s %f -i %f -v %s -o %s -t %d -p %d -b %s -c %s -f %s %s/%s" % \
                (scr, tmpfile, i+1, scale_default, scale_pid, var, outdir, toystat, controlstat, statfile, controlfile, figfile, eosrootdir, infile)
        if limits:
            command += " -l %f -u %f" % (limits[0], limits[1])

        if controlfile not in ready_list:
            print(command)
            if ("dry" not in opt.split(":")): os.system(command)
