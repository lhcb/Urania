###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
import uproot
import sys
import numpy as np

file = uproot.open(sys.argv[1])
tree = file["jpsi/nt"]

ntr = tree.array("nTracks")

ep_vars = tree.arrays(["ep_*", "nTracks"])
em_vars = tree.arrays(["em_*", "nTracks"])

for i in list(ep_vars.keys()):
    print((i, i[:3], i[3:]))
    if i[:3] == b"ep_":
        print((i, i[3:], b"probe_" + i[3:]))
        ep_vars[b"probe_" + i[3:]] = ep_vars.pop(i)
ep_vars[b"probe_Brunel_charge"] = np.ones_like(ep_vars[b"probe_PT"])

for i in list(em_vars.keys()):
    print((i, i[:3], i[3:]))
    if i[:3] == b"em_":
        print((i, i[3:], b"probe_" + i[3:]))
        em_vars[b"probe_" + i[3:]] = em_vars.pop(i)
em_vars[b"probe_Brunel_charge"] = np.ones_like(em_vars[b"probe_PT"]) * (-1)

with uproot.recreate(sys.argv[2]) as f:
    f["tree"] = uproot.newtree({i: "float32" for i in list(ep_vars.keys())})
    f["tree"].extend({
        i: np.concatenate((em_vars[i], ep_vars[i]))
        for i in list(ep_vars.keys())
    })
