###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import
import sys, os

eosdir = "/eos/lhcb/wg/PID/PIDGen/Data/Run2"
eosrootdir = "root://eoslhcb.cern.ch/" + eosdir

tmpdir = "/tmp"

datasets = {
    'MagDown_2018':
    ("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/PIDCALIB.ROOT/00082949/0000/00082949_%8.8d_1.pidcalib.root",
     436),
    'MagUp_2018':
    ("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/PIDCALIB.ROOT/00082947/0000/00082947_%8.8d_1.pidcalib.root",
     437),
    'MagDown_2017':
    ("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/PIDCALIB.ROOT/00075643/0000/00075643_%8.8d_1.pidcalib.root",
     249),
    'MagUp_2017':
    ("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/PIDCALIB.ROOT/00075641/0000/00075641_%8.8d_1.pidcalib.root",
     189),
    'MagDown_2016':
    ("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00056408/0000/00056408_%8.8d_1.pidcalib.root",
     182),
    'MagUp_2016':
    ("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00056409/0000/00056409_%8.8d_1.pidcalib.root",
     185),
    'MagDown_2015':
    ("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00057802/0000/00057802_%8.8d_1.pidcalib.root",
     87),
    'MagUp_2015':
    ("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00057800/0000/00057800_%8.8d_1.pidcalib.root",
     48),
}


def samples():
    from . import Samples
    d = {}
    for i in Samples.__all__:
        module = __import__("PIDGenExpert.Run2.Samples", fromlist=[i])
        s = getattr(getattr(module, i), "sample")
        d[s['name']] = s
    return d


def configs():
    from . import Configs
    d = {}
    for i in Configs.__all__:
        module = __import__("PIDGenExpert.Run2.Configs", fromlist=[i])
        c = getattr(getattr(module, i), "config")
        d[c['name']] = c
    return d
