###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import absolute_import
from . import Config
from . import ConfigMC

particles = []
variables = []

configs = Config.configs()

for n in list(configs.keys()):
    ns = n.split("_", 1)
    p = ns[0]
    v = ns[1]
    if p == 'gamma': continue
    if not p in particles: particles += [p]
    if not v in variables: variables += [v]

particles = ["pi", "K", "p", "e", "mu"]

print('Run2 configs:')
print('Key: G = real data; C8 = Sim08; C9 = Sim09')
s = "| *%32.32s* " % "Variable"
for p in particles:
    s += " | *%5.5s* " % p
s += " |"
print(s)
for v in sorted(variables):
    s = "| %40s " % ("!" + v)
    for p in particles:
        n = "%s_%s" % (p, v)
        f = ""
        if n in list(configs.keys()):
            f += "G"
            if n in list(ConfigMC.configs.keys()): f += "/C9"
        s += " | %7.7s " % f
    s += ' |'
    print(s)
