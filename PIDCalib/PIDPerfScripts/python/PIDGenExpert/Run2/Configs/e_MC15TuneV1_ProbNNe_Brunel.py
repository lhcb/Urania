###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
config = {
    'bins': 30,
    'controlstat': 300000,
    'gamma': -0.2,
    'name': 'e_MC15TuneV1_ProbNNe_Brunel',
    'nbootstrap': 5,
    'sample': 'e_BJpsi_Brunel',
    'scale_default': 1.5,
    'scale_pid': 1.5,
    'scale_syst': 2.0,
    'toystat': 300000,
    'var': 'probe_Brunel_MC15TuneV1_ProbNNe'
}
