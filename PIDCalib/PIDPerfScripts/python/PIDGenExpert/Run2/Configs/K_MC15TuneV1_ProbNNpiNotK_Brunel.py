###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
config = {
    'bins':
    100,
    'controlstat':
    1000000,
    'name':
    'K_MC15TuneV1_ProbNNpiNotK_Brunel',
    'nbootstrap':
    5,
    'sample':
    'K_DSt_Brunel',
    'scale_default':
    0.1,
    'scale_pid':
    1.0,
    'scale_syst':
    0.15,
    'toystat':
    20000000,
    'transform_backward':
    '(1.-(1.-x)**(1./0.2))**(1./0.2)',
    'transform_forward':
    '1.-(1.-x**0.2)**0.2',
    'var':
    'i.probe_Brunel_MC15TuneV1_ProbNNpi*(1.-i.probe_Brunel_MC15TuneV1_ProbNNk)'
}
