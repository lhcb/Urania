###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
config = {
    'bins':
    50,
    'controlstat':
    1000000,
    #    'gamma': -0.25,
    'name':
    'p_LbLcPi_MC15TuneV1_ProbNNpNotKpi_Brunel',
    'nbootstrap':
    5,
    'sample':
    'p_LbLcPi_Brunel',
    'scale_default':
    0.5,
    'scale_pid':
    1.2,
    'scale_syst':
    0.75,
    'toystat':
    2000000,
    'transform_backward':
    '(1.-(1.-x)**(1./0.2))**(1./0.2)',
    'transform_forward':
    '1.-(1.-x**0.2)**0.2',
    'var':
    'i.lp_MC15TuneV1_ProbNNp*(1.-i.lp_MC15TuneV1_ProbNNK)*(1.-i.lp_MC15TuneV1_ProbNNpi)'
}
