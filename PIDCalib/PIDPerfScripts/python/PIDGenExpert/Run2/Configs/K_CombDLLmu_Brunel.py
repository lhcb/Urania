###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
config = {
    'bins': 100,
    'controlstat': 5000000,
    'gamma': 1.0,
    'limits': (-15.0, 14.0),
    'name': 'K_CombDLLmu_Brunel',
    'nbootstrap': 10,
    'sample': 'K_DSt_Brunel',
    'scale_default': 0.1,
    'scale_pid': 1.0,
    'scale_syst': 0.15,
    'toystat': 10000000,
    'var': 'probe_Brunel_PIDmu'
}
