###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
config = {
    'bins': 50,
    'controlstat': 1000000,
    'cut': 'i.lp_MC12TuneV2_ProbNNp>0.05',
    'gamma': -0.25,
    'name': 'p_LbLcPi_MC12TuneV2gt0.05_MC15TuneV1_ProbNNp_Brunel',
    'nbootstrap': 5,
    'sample': 'p_LbLcPi_Brunel',
    'scale_default': 0.75,
    'scale_pid': 1.2,
    'scale_syst': 1.0,
    'toystat': 2000000,
    'var': 'lp_MC15TuneV1_ProbNNp'
}
