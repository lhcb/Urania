###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
config = {
    'bins': 30,
    'controlstat': 300000,
    'name': 'e_MC15TuneV1_ProbNNpi_Stripping',
    'nbootstrap': 5,
    'sample': 'e_BJpsi_Stripping',
    'scale_default': 1.5,
    'scale_pid': 1.5,
    'scale_syst': 2.0,
    'toystat': 300000,
    'transform_backward': '(1.-(1.-x)**(1./0.2))**(1./0.15)',
    'transform_forward': '1.-(1.-x**0.15)**0.2',
    'var': 'probe_Brunel_MC15TuneV1_ProbNNpi'
}
