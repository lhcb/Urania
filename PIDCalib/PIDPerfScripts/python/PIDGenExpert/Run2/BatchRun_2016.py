###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
conf = [
    "p_LbLcPi_PIDpgt0_MC15TuneV1_ProbNNp_Brunel",
    "pi_PIDKlt2_MC15TuneV1_ProbNNpi_Brunel",
    "K_PIDKgt4_MC15TuneV1_ProbNNK_Brunel",
]

ds = "MagUp_2016:MagDown_2016"

import os

for i in conf:
    #  os.system("python MakeTuples_numpy.py %s - %s" % (i, ds) )
    #  os.system("python CreatePIDPdf_condor.py %s - %s" % (i, ds) )
    os.system("python ComparePDFs.py %s %s" % (i, ds))
#  os.system("python CreatePIDPdf_condor.py %s continue %s" % (i, ds) )
