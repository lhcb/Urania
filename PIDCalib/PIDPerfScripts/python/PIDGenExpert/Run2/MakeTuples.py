###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import absolute_import

import os, sys
from ROOT import TFile, TNtuple
from math import sqrt, log

from .Config import *


def convert_single_file(infile,
                        f2,
                        nt2,
                        treenames,
                        pidvar,
                        ptvar,
                        etavar,
                        ntracksvar,
                        weightvar,
                        transform,
                        cut=None,
                        pol_cut=""):
    f1 = TFile.Open(infile)
    print("file ", infile)
    if not f1: return False
    print(pol_cut)

    for treename in treenames:
        print(treename)
        nt1 = f1.Get(treename)
        f2.cd()
        if not nt1:
            continue
        nentries = nt1.GetEntries()
        print("  tuple %s: %d entries" % (treename, nentries))
        n = 0
        if isinstance(transform, str):
            pid_code = transform
        else:
            gamma = transform
            if gamma < 0:
                pid_code = compile("(1.-(1.-i.%s)**%f)" % (pidvar, abs(gamma)),
                                   '<string>', 'eval')
            else:
                pid_code = compile("(i.%s)**%f" % (pidvar, abs(gamma)),
                                   '<string>', 'eval')
        pt_code = compile("log(i.%s)" % ptvar, '<string>', 'eval')
        eta_code = compile("i.%s" % etavar, '<string>', 'eval')

        if "i." in pidvar:
            x_code = compile(pidvar, '<string>', 'eval')
        else:
            x_code = compile("i.%s" % pidvar, '<string>', 'eval')

        if ntracksvar:
            ntracks_code = compile("log(i.%s)" % ntracksvar, '<string>',
                                   'eval')
        weight_code = compile("i.%s" % weightvar, '<string>', 'eval')
        if pol_cut:
            pol_code = compile("i.%s" % pol_cut, '<string>', 'eval')
        cut_code = None
        if cut:
            cut_code = compile(cut, "<string>", 'eval')
        for i in nt1:
            n += 1
            if (n % 10000 == 0):
                print("    event %d/%d" % (n, nentries))
            try:
                #        if i.probe_MINIPCHI2 < 400 : continue
                if cut:
                    if not eval(cut_code): continue
                if pol_cut:
                    if not eval(pol_code): continue
                x = eval(x_code)
                pid = eval(pid_code)
                pt = eval(pt_code)
                eta = eval(eta_code)
                ntracks = eval(ntracks_code)
                weight = eval(weight_code)
                nt2.Fill(pid, pt, eta, ntracks, weight)
            except:
                continue

    f1.Close()
    return True


if len(sys.argv) > 1:
    # config name, e.g. "p_ProbNNp"
    configname = sys.argv[1]
else:
    print(
        "Usage: MakeTuples [config] [option1:option2:...] [dataset1:dataset2:]"
    )
    print("  configs are: ")
    for i in sorted(configs.keys()):
        print("    ", i)
    print("  options are: ")
    print(
        "    test     - run for just a single PIDCalib file rather than whole dataset"
    )
    print(
        "    polarity - create separate datasets for positive and negative track polarities"
    )
    print(
        "    brem     - create separate datasets for HasBremAdded=0 and 1 electrons"
    )
    print("  datasets can be, e.g. ")
    print("    MagDown_2012:MagUp_2011")
    print("    or leave empty to process all available datasets")
    sys.exit(0)

config = configs[configname]
# sample name, e.g. "p" for proton
samplename = config['sample']
sample = samples[samplename]

dslist = list(datasets.keys())
options = ""
if len(sys.argv) > 2: options = sys.argv[2].split(":")
if len(sys.argv) > 3: dslist = sys.argv[3].split(":")

# resampled variable name
var = configname

# other variable names
pidvar = config['var']
ptvar = sample['pt']
etavar = sample['eta']
ntracksvar = sample['ntracks']
weightvar = sample['weight']
treename = sample['trees']
treedict = treename
transform = None
if 'gamma' in list(config.keys()):
    transform = config['gamma']
elif 'transform_forward' in list(config.keys()):
    transform = config['transform_forward']

cut = None
if "cut" in list(config.keys()): cut = config["cut"]

# Create dictionary with the lists of PIDCalib datasets for each year and magnet polarity
dsdict = {}

if "datasets" in list(sample.keys()):
    dss = sample['datasets']
else:
    dss = datasets
for ds in dslist:
    if not isinstance(dss[ds], tuple):
        dsdict[ds] = [dss[ds]]
    else:
        dsdict[ds] = []
        n = dss[ds][1]
        for i in range(0, n):
            dsdict[ds] += [dss[ds][0] % i]

os.system("eos mkdir -p %s/%s" % (eosdir, configname))

# Loop over PIDCalib datasets
for pol, dss in dsdict.items():
    if isinstance(treedict, tuple):
        treename = treedict[pol]
    if "polarity" in options:
        calibfile_p = pol + "_P.root"
        f2p = TFile.Open(calibfile_p, "RECREATE")
        nt2p = TNtuple("pid", "pid", "PID:Pt:Eta:Ntracks:w")
        calibfile_m = pol + "_M.root"
        f2m = TFile.Open(calibfile_m, "RECREATE")
        nt2m = TNtuple("pid", "pid", "PID:Pt:Eta:Ntracks:w")
    elif "brem" in options:
        calibfile_nb = pol + "_NoBrem.root"
        f2nb = TFile.Open(calibfile_nb, "RECREATE")
        nt2nb = TNtuple("pid", "pid", "PID:Pt:Eta:Ntracks:w")
        calibfile_b = pol + "_Brem.root"
        f2b = TFile.Open(calibfile_b, "RECREATE")
        nt2b = TNtuple("pid", "pid", "PID:Pt:Eta:Ntracks:w")
    else:
        calibfile = pol + ".root"
        f2 = TFile.Open(calibfile, "RECREATE")
        nt2 = TNtuple("pid", "pid", "PID:Pt:Eta:Ntracks:w")

    print("Polarity " + pol)

    ntracksvar2 = ntracksvar
    if isinstance(ntracksvar, dict):
        ntracksvar2 = ntracksvar[pol]

    nds = 0
    for ds in dss:
        if "polarity" in options:
            polarity_var = "probe_Brunel_charge"
            if "polarity" in sample: polarity_var = sample["polarity"]
            p_cut = "%s>0" % polarity_var
            m_cut = "%s<0" % polarity_var
            ok = convert_single_file(ds, f2p, nt2p, treename, pidvar, ptvar,
                                     etavar, ntracksvar2, weightvar, transform,
                                     cut, p_cut)
            ok = convert_single_file(ds, f2m, nt2m, treename, pidvar, ptvar,
                                     etavar, ntracksvar2, weightvar, transform,
                                     cut, m_cut)
        elif "brem" in options:
            brem_var = "probe_Brunel_HasBremAdded"
            nb_cut = "%s==0" % brem_var
            b_cut = "%s==1" % brem_var
            ok = convert_single_file(ds, f2nb, nt2nb, treename, pidvar, ptvar,
                                     etavar, ntracksvar2, weightvar, transform,
                                     cut, nb_cut)
            ok = convert_single_file(ds, f2b, nt2b, treename, pidvar, ptvar,
                                     etavar, ntracksvar2, weightvar, transform,
                                     cut, b_cut)
        else:
            ok = convert_single_file(ds, f2, nt2, treename, pidvar, ptvar,
                                     etavar, ntracksvar2, weightvar, transform,
                                     cut)
        if ok: nds += 1
        if nds >= 2 and "test" in options:
            break

    if "polarity" in options:
        f2p.cd()
        nt2p.Write()
        f2p.Close()
        os.system("eos cp %s %s/%s/" % (calibfile_p, eosdir, configname))
        os.remove(calibfile_p)
        f2m.cd()
        nt2m.Write()
        f2m.Close()
        os.system("eos cp %s %s/%s/" % (calibfile_m, eosdir, configname))
        os.remove(calibfile_m)
    elif "brem" in options:
        f2nb.cd()
        nt2nb.Write()
        f2nb.Close()
        os.system("eos cp %s %s/%s/" % (calibfile_nb, eosdir, configname))
        os.remove(calibfile_nb)
        f2b.cd()
        nt2b.Write()
        f2b.Close()
        os.system("eos cp %s %s/%s/" % (calibfile_b, eosdir, configname))
        os.remove(calibfile_b)
    else:
        f2.cd()
        nt2.Write()
        f2.Close()
        os.system("eos cp %s %s/%s/" % (calibfile, eosdir, configname))
        os.remove(calibfile)

    if "test" in options:
        print("Now run:")
        print("root -l %s/%s/%s" % (eosrootdir, configname, calibfile))
