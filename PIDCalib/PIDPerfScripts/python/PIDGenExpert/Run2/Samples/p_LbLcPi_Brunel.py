###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
sample = {
    'datasets': {
        'MagDown_2015':
        '/eos/lhcb/wg/PID/PIDGen/Calibration/Run2/Proton/lcpi_splot_exp15_md.root',
        'MagDown_2016':
        '/eos/lhcb/wg/PID/PIDGen/Calibration/Run2/Proton/lcpi_splot_exp16_md.root',
        'MagDown_2017':
        '/eos/lhcb/wg/PID/PIDGen/Calibration/Run2/Proton/lcpi_splot_exp17_md.root',
        'MagDown_2018':
        '/eos/lhcb/wg/PID/PIDGen/Calibration/Run2/Proton/lcpi_splot_exp18_md.root',
        'MagUp_2015':
        '/eos/lhcb/wg/PID/PIDGen/Calibration/Run2/Proton/lcpi_splot_exp15_mu.root',
        'MagUp_2016':
        '/eos/lhcb/wg/PID/PIDGen/Calibration/Run2/Proton/lcpi_splot_exp16_mu.root',
        'MagUp_2017':
        '/eos/lhcb/wg/PID/PIDGen/Calibration/Run2/Proton/lcpi_splot_exp17_mu.root',
        'MagUp_2018':
        '/eos/lhcb/wg/PID/PIDGen/Calibration/Run2/Proton/lcpi_splot_exp18_mu.root'
    },
    'eta': 'lp_eta',
    'name': 'p_LbLcPi_Brunel',
    'ntracks': 'nTracks',
    'polarity': 'lp_id',
    'pt': 'lp_pt',
    'trees': ('wdata', ),
    'weight': 'sum_lcpi_sig_norm_sw'
}
