###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
sample = {
    'eta': 'probe_Brunel_ETA',
    'name': 'p_Lam0_HPT_Brunel',
    'ntracks': 'nTracks',
    'pt': 'probe_Brunel_PT',
    'trees': ('Lam0_HPT_PTuple/DecayTree', 'Lam0_HPT_PbarTuple/DecayTree'),
    'weight': 'probe_sWeight'
}
