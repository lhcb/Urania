###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
sample = {
    'eta': 'probe_Brunel_ETA',
    'name': 'pi_DSt_Brunel',
    'ntracks': {
        'MagDown_2015': 'nTracks',
        'MagDown_2016': 'nTracks',
        'MagDown_2017': 'nTracks_Brunel',
        'MagDown_2018': 'nTracks_Brunel',
        'MagUp_2015': 'nTracks',
        'MagUp_2016': 'nTracks',
        'MagUp_2017': 'nTracks_Brunel',
        'MagUp_2018': 'nTracks_Brunel'
    },
    'pt': 'probe_Brunel_PT',
    'trees': ('DSt_PiPTuple/DecayTree', 'DSt_PiMTuple/DecayTree'),
    'weight': 'probe_sWeight'
}
