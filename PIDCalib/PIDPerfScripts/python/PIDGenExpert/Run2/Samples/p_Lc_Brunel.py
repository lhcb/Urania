###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
sample = {
    'datasets': {
        'MagDown_2015':
        ('/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00064785/0000/00064785_%8.8d_1.pidcalib.root',
         79),
        'MagDown_2016':
        ('/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064795/0000/00064795_%8.8d_1.pidcalib.root',
         156),
        'MagDown_2017':
        ('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/PIDCALIB.ROOT/00075643/0000/00075643_%8.8d_1.pidcalib.root',
         249),
        'MagDown_2018':
        ('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/PIDCALIB.ROOT/00082949/0000/00082949_%8.8d_1.pidcalib.root',
         436),
        'MagUp_2015':
        ('/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00064787/0000/00064787_%8.8d_1.pidcalib.root',
         45),
        'MagUp_2016':
        ('/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064793/0000/00064793_%8.8d_1.pidcalib.root',
         147),
        'MagUp_2017':
        ('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/PIDCALIB.ROOT/00075641/0000/00075641_%8.8d_1.pidcalib.root',
         189),
        'MagUp_2018':
        ('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/PIDCALIB.ROOT/00082947/0000/00082947_%8.8d_1.pidcalib.root',
         437)
    },
    'eta': 'probe_Brunel_ETA',
    'name': 'p_Lc_Brunel',
    'ntracks': 'nTracks_Brunel',
    'pt': 'probe_Brunel_PT',
    'trees': ('Lc_PTuple/DecayTree', 'Lc_PbarTuple/DecayTree'),
    'weight': 'probe_sWeight'
}
