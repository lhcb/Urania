###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
sample = {
    'eta': 'probe_Brunel_ETA',
    'name': 'p_Lam0_Brunel',
    'ntracks': {
        'MagDown_2015': 'nTracks',
        'MagDown_2016': 'nTracks',
        'MagDown_2017': 'nTracks_Brunel',
        'MagDown_2018': 'nTracks_Brunel',
        'MagUp_2015': 'nTracks',
        'MagUp_2016': 'nTracks',
        'MagUp_2017': 'nTracks_Brunel',
        'MagUp_2018': 'nTracks_Brunel'
    },
    'pt': 'probe_Brunel_PT',
    'trees': {
        'MagDown_2015':
        ('Lam0_PTuple/DecayTree', 'Lam0_PbarTuple/DecayTree',
         'Lam0_HPT_PTuple/DecayTree', 'Lam0_HPT_PbarTuple/DecayTree',
         'Lam0_VHPT_PTuple/DecayTree', 'Lam0_VHPT_PbarTuple/DecayTree'),
        'MagDown_2016':
        ('Lam0_PTuple/DecayTree', 'Lam0_PbarTuple/DecayTree',
         'Lam0_HPT_PTuple/DecayTree', 'Lam0_HPT_PbarTuple/DecayTree',
         'Lam0_VHPT_PTuple/DecayTree', 'Lam0_VHPT_PbarTuple/DecayTree'),
        'MagDown_2017':
        ('Lam0LL_PTuple/DecayTree', 'Lam0LL_PbarTuple/DecayTree',
         'Lam0LL_HPT_PTuple/DecayTree', 'Lam0LL_HPT_PbarTuple/DecayTree',
         'Lam0LL_VHPT_PTuple/DecayTree', 'Lam0LL_VHPT_PbarTuple/DecayTree'),
        'MagDown_2018':
        ('Lam0LL_PTuple/DecayTree', 'Lam0LL_PbarTuple/DecayTree',
         'Lam0LL_HPT_PTuple/DecayTree', 'Lam0LL_HPT_PbarTuple/DecayTree',
         'Lam0LL_VHPT_PTuple/DecayTree', 'Lam0LL_VHPT_PbarTuple/DecayTree'),
        'MagUp_2015':
        ('Lam0_PTuple/DecayTree', 'Lam0_PbarTuple/DecayTree',
         'Lam0_HPT_PTuple/DecayTree', 'Lam0_HPT_PbarTuple/DecayTree',
         'Lam0_VHPT_PTuple/DecayTree', 'Lam0_VHPT_PbarTuple/DecayTree'),
        'MagUp_2016':
        ('Lam0_PTuple/DecayTree', 'Lam0_PbarTuple/DecayTree',
         'Lam0_HPT_PTuple/DecayTree', 'Lam0_HPT_PbarTuple/DecayTree',
         'Lam0_VHPT_PTuple/DecayTree', 'Lam0_VHPT_PbarTuple/DecayTree'),
        'MagUp_2017':
        ('Lam0LL_PTuple/DecayTree', 'Lam0LL_PbarTuple/DecayTree',
         'Lam0LL_HPT_PTuple/DecayTree', 'Lam0LL_HPT_PbarTuple/DecayTree',
         'Lam0LL_VHPT_PTuple/DecayTree', 'Lam0LL_VHPT_PbarTuple/DecayTree'),
        'MagUp_2018': ('Lam0LL_PTuple/DecayTree', 'Lam0LL_PbarTuple/DecayTree',
                       'Lam0LL_HPT_PTuple/DecayTree',
                       'Lam0LL_HPT_PbarTuple/DecayTree',
                       'Lam0LL_VHPT_PTuple/DecayTree',
                       'Lam0LL_VHPT_PbarTuple/DecayTree')
    },
    'weight': 'probe_sWeight'
}
