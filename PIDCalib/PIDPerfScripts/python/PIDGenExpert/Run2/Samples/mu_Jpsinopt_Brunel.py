###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
sample = {
    'datasets': {
        'MagDown_2018':
        ("/eos/lhcb/grid/prod/lhcb/LHCb/Collision18/PIDCALIB.ROOT/00108870/0000/00108870_%8.8d_1.pidcalib.root",
         55),
        'MagUp_2018':
        ("/eos/lhcb/grid/prod/lhcb/LHCb/Collision18/PIDCALIB.ROOT/00108868/0000/00108868_%8.8d_1.pidcalib.root",
         64),
        'MagDown_2017':
        ("/eos/lhcb/grid/prod/lhcb/LHCb/Collision17/PIDCALIB.ROOT/00108864/0000/00108864_%8.8d_1.pidcalib.root",
         39),
        'MagUp_2017':
        ("/eos/lhcb/grid/prod/lhcb/LHCb/Collision17/PIDCALIB.ROOT/00108862/0000/00108862_%8.8d_1.pidcalib.root",
         33),
        'MagDown_2016':
        ("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00111667/0000/00111667_%8.8d_1.pidcalib.root",
         29),
        'MagUp_2016':
        ("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00111665/0000/00111665_%8.8d_1.pidcalib.root",
         25),
        'MagDown_2015':
        ("/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00064785/0000/00064785_%8.8d_1.pidcalib.root",
         79),
        'MagUp_2015':
        ("/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00064787/0000/00064787_%8.8d_1.pidcalib.root",
         45),
    },
    'eta': 'probe_Brunel_ETA',
    'name': 'mu_Jpsinopt_Brunel',
    'ntracks': 'nTracks_Brunel',
    'pt': 'probe_Brunel_PT',
    'trees': ('Jpsinopt_MuPTuple/DecayTree', 'Jpsinopt_MuMTuple/DecayTree'),
    'weight': 'probe_sWeight'
}
