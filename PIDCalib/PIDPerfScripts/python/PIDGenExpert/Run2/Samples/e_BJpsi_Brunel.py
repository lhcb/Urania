###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
sample = {
    'eta': 'probe_Brunel_ETA',
    'name': 'e_BJpsi_Brunel',
    'ntracks': 'nTracks',
    'pt': 'probe_Brunel_PT',
    'trees': {
        "MagDown_2015": ('B_Jpsi_EPTuple/DecayTree',
                         'B_Jpsi_EMTuple/DecayTree'),
        "MagUp_2015": ('B_Jpsi_EPTuple/DecayTree', 'B_Jpsi_EMTuple/DecayTree'),
        "MagDown_2016": ('B_Jpsi_EPTuple/DecayTree',
                         'B_Jpsi_EMTuple/DecayTree'),
        "MagUp_2016": ('B_Jpsi_EPTuple/DecayTree', 'B_Jpsi_EMTuple/DecayTree'),
    },
    'weight': 'probe_sWeight'
}
