###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
sample = {
    'datasets': {
        'MagDown_2015':
        '/eos/lhcb/wg/PID/PIDCalib_2015_electrons/pidcalib_BJpsiEE_MD.root',
        'MagDown_2016':
        '/eos/lhcb/wg/PID/PIDCalib_2016_electrons/pidcalib_BJpsiEE_MD_TAGCUT.root',
        'MagDown_2017':
        '/eos/lhcb/wg/PID/PIDCalib_2017_electrons/Turbo2017_B2KJpsiEE_MagDown.root',
        'MagDown_2018':
        '/eos/lhcb/wg/PID/PIDCalib_2018_electrons/Turbo2018_B2KJpsiEE_MagDown.root',
        'MagUp_2015':
        '/eos/lhcb/wg/PID/PIDCalib_2015_electrons/pidcalib_BJpsiEE_MU.root',
        'MagUp_2016':
        '/eos/lhcb/wg/PID/PIDCalib_2016_electrons/pidcalib_BJpsiEE_MU_TAGCUT.root',
        'MagUp_2017':
        '/eos/lhcb/wg/PID/PIDCalib_2017_electrons/Turbo2017_B2KJpsiEE_MagUp.root',
        'MagUp_2018':
        '/eos/lhcb/wg/PID/PIDCalib_2018_electrons/Turbo2018_B2KJpsiEE_MagUp.root',
    },
    'eta': 'probe_Brunel_ETA',
    'name': 'e_BJpsi_Stripping',
    'ntracks': {
        'MagDown_2015': 'nTracks_Brunel',
        'MagDown_2016': 'nTracks',
        'MagDown_2017': 'nTracks_Brunel',
        'MagDown_2018': 'nTracks_Brunel',
        'MagUp_2015': 'nTracks_Brunel',
        'MagUp_2016': 'nTracks',
        'MagUp_2017': 'nTracks_Brunel',
        'MagUp_2018': 'nTracks_Brunel',
    },
    'pt': 'probe_Brunel_PT',
    'trees': ['DecayTree'],
    'weight': 'probe_sWeight'
}
