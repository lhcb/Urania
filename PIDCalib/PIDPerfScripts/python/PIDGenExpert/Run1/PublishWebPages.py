###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import absolute_import
from . import Config
from . import ConfigMC
from . import ConfigMCSim09
import os, sys

particles = []
variables = []

controldir = "/eos/user/p/poluekt/pid/www/PIDGenControlPlots/Data/Run1/"
controlmc08dir = "/eos/user/p/poluekt/pid/www/PIDGenControlPlots/MC/Run1Sim08/"
controlmc09dir = "/eos/user/p/poluekt/pid/www/PIDGenControlPlots/MC/Run1Sim09/"

templatedir = "/eos/lhcb/wg/PID/PIDGen/Data/Run1/"
templatemc08dir = "/eos/lhcb/wg/PID/PIDGen/MC/Run1Sim08/"
templatemc09dir = "/eos/lhcb/wg/PID/PIDGen/MC/Run1Sim09/"

header = """
<html>
<style>
body { font-family: Arial, Helvetica, sans-serif; font-size:12px; }
table, td, th, tr {
border-collapse:collapse;
border:1px solid black;
padding:2px;
font-family: Arial, Helvetica, sans-serif; font-size:12px;
}
table.invisible { border:none; }
tr.invisible { border:none; }
td.invisible { border:none; }
table.side { width:auto; }
tr.alt1 { background-color:#E0E0E0; }
tr.alt2 { background-color:#FFFFFF; }
th { background-color:#707070; color:white; border:1px solid black; }
th.header { background-color:#707070; color:white; border:none; }
a { color:blue; }
a:link,a:visited {text-decoration:none}
a:hover {text-decoration:underline}
a.header { color:white; }
a.extern { color:darkred; }
.hidden { display: none; }
.unhidden { display: block; }
</style>
<body>
<h1>PIDGen/PIDCorr templates in Run1</h1>
<p>If <strong>Data</strong> template is present, PIDGen can be used</p>
<p>If both <strong>Data</strong> and <strong>Sim</strong> templates are present, PIDCorr can be used</p>
<p><strong>Boldface</strong>: template is present</p>
<p>Normal font: config is present, but template is missing</p>
"""

footer = """
</body></html>
"""

pagedir = "/eos/project/l/lhcbwebsites/www/lhcb-pid-wgp-plots/PIDGen/Run1/"
pagefile = pagedir + "/index.html"

particles = ["pi", "K", "p", "e", "mu"]
years = ["2011", "2012"]
mags = ["MagDown", "MagUp"]

categories = {
    "e": ("Brem", "NoBrem"),
}


def treat_template(n, sample, f):
    template = n + "_" + sample
    filename = pagedir + "/Data/" + template + "_hist.png"
    controlname1 = controldir + "/" + template + "_hist.pdf"
    controlname2 = controldir + "/" + template + "_slices.pdf"
    if not (os.path.isfile(controlname1) and os.path.isfile(controlname2)):
        if os.path.isfile(templatedir + "/" + n + "/" + sample +
                          "_distrib.root"):
            os.system("python ComparePDFs.py %s %s" % (n, sample))
            os.system("cp plots/%s_hist.pdf %s" % (template, controlname1))
            os.system("cp plots/%s_slices.pdf %s" % (template, controlname2))
    if os.path.isfile(controlname1) and os.path.isfile(controlname2):
        f += "<a href=%s>" % ("Data/" + template + "_hist.png")
        if not os.path.isfile(filename):
            print("convert -density 150 -trim -append %s %s %s" %
                  (controlname1, controlname2, filename))
            os.system("convert -density 150 -trim -append %s %s %s" %
                      (controlname1, controlname2, filename))
    if os.path.isfile(templatedir + "/" + n + "/" + sample + ".root"):
        f += "<strong>"
    f += "Data "
    if os.path.isfile(templatedir + "/" + n + "/" + sample + ".root"):
        f += "</strong>"
    if os.path.isfile(controlname1) and os.path.isfile(controlname2):
        f += "</a>"
    return f


def treat_mc08_template(n, sample, f):
    template = n + "_" + sample
    filename = pagedir + "/MC08/" + template + "_hist.png"
    controlname1 = controlmc08dir + "/" + template + "_hist.pdf"
    controlname2 = controlmc08dir + "/" + template + "_slices.pdf"
    if not (os.path.isfile(controlname1) and os.path.isfile(controlname2)):
        if os.path.isfile(templatemc08dir + "/" + n + "/" + sample +
                          "_distrib.root"):
            os.system("python ComparePDFs_MC.py sim08 %s %s" % (n, sample))
            os.system("cp plots_mc/%s_hist.pdf %s" % (template, controlname1))
            os.system(
                "cp plots_mc/%s_slices.pdf %s" % (template, controlname2))
    if os.path.isfile(controlname1) and os.path.isfile(controlname2):
        f += "<a href=%s>" % ("MC08/" + template + "_hist.png")
        if not os.path.isfile(filename):
            print("convert -density 150 -trim -append %s %s %s" %
                  (controlname1, controlname2, filename))
            os.system("convert -density 150 -trim -append %s %s %s" %
                      (controlname1, controlname2, filename))
    if os.path.isfile(templatemc08dir + "/" + n + "/" + sample + ".root"):
        f += "<strong>"
    f += "Sim08 "
    if os.path.isfile(templatemc08dir + "/" + n + "/" + sample + ".root"):
        f += "</strong>"
    if os.path.isfile(controlname1) and os.path.isfile(controlname2):
        f += "</a>"
    return f


def treat_mc09_template(n, sample, f):
    template = n + "_" + sample
    filename = pagedir + "/MC09/" + template + "_hist.png"
    controlname1 = controlmc09dir + "/" + template + "_hist.pdf"
    controlname2 = controlmc09dir + "/" + template + "_slices.pdf"
    if not (os.path.isfile(controlname1) and os.path.isfile(controlname2)):
        if os.path.isfile(templatemc09dir + "/" + n + "/" + sample +
                          "_distrib.root"):
            os.system("python ComparePDFs_MC.py sim09 %s %s" % (n, sample))
            os.system("cp plots_mc/%s_hist.pdf %s" % (template, controlname1))
            os.system(
                "cp plots_mc/%s_slices.pdf %s" % (template, controlname2))
    if os.path.isfile(controlname1) and os.path.isfile(controlname2):
        f += "<a href=%s>" % ("MC09/" + template + "_hist.png")
        if not os.path.isfile(filename):
            print("convert -density 150 -trim -append %s %s %s" %
                  (controlname1, controlname2, filename))
            os.system("convert -density 150 -trim -append %s %s %s" %
                      (controlname1, controlname2, filename))
    if os.path.isfile(templatemc09dir + "/" + n + "/" + sample + ".root"):
        f += "<strong>"
    f += "Sim09 "
    if os.path.isfile(templatemc09dir + "/" + n + "/" + sample + ".root"):
        f += "</strong>"
    if os.path.isfile(controlname1) and os.path.isfile(controlname2):
        f += "</a>"
    return f


configs = Config.configs
mc08configs = ConfigMC.configs
mc09configs = ConfigMCSim09.configs

for n in list(configs.keys()) + list(mc08configs.keys()) + list(
        mc09configs.keys()):
    ns = n.split("_", 1)
    p = ns[0]
    v = ns[1]
    if p == 'gamma': continue
    if not p in particles: particles += [p]
    if not v in variables: variables += [v]

s = '<table><tr><th> %40.40s </th>' % "Variable"
s += "<th> %13.13s </th>" % "Year / Polarity"
for p in particles:
    s += "<th> %6.6s </th>" % p
    if p in categories:
        for c in categories[p]:
            s += "<th> %6.6s (%s) </th>" % (p, c)
s += "</tr>\n"

for v in sorted(variables):
    for y in years:
        for m in mags:
            sample = m + "_" + y
            if y == years[0] and m == mags[0]:
                s += "<tr><td rowspan=%d> %50.50s </td>" % (
                    len(years) * len(mags), v)
                s += "<td> %4.4s / %8.8s </td>" % (y, m)
            else:
                s += "<tr>"
                s += "<td> %4.4s / %8.8s </td>" % (y, m)
            for p in particles:
                n = "%s_%s" % (p, v)
                f = ""
                #        print controldir + "/" + n + "_" + m + "_" + y + "_hist.pdf"
                if n in list(configs.keys()):
                    f = treat_template(n, sample, f)

                if n in list(mc08configs.keys()):
                    f = treat_mc08_template(n, sample, f)

                if n in list(mc09configs.keys()):
                    f = treat_mc09_template(n, sample, f)

                s += "<td> %s </td>" % f

                if p in categories:
                    for c in categories[p]:

                        f = ""

                        if n in list(configs.keys()):
                            f = treat_template(n, sample + "_" + c, f)

                        if n in list(mc08configs.keys()):
                            f = treat_mc08_template(n, sample + "_" + c, f)

                        if n in list(mc09configs.keys()):
                            f = treat_mc09_template(n, sample + "_" + c, f)

                        s += "<td> %s </td>" % f

            s += '</tr>\n'

print(s)
f = open(pagefile, "w")
f.write(header + s + footer)
f.close()
