{
  //TH1::Sumw2(1);  
//2015 MAGUP
TChain *pionm_MU2015 = new TChain("DSt_PiMTuple/DecayTree");
pionm_MU2015->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064787/0000/*.root");
 TChain *kaonp_MU2015 = new TChain("DSt_KPTuple/DecayTree");
kaonp_MU2015->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064787/0000/*.root");
TChain *pionp_MU2015 = new TChain("DSt_PiPTuple/DecayTree");
pionp_MU2015->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064787/0000/*.root");
 TChain *kaonm_MU2015 = new TChain("DSt_KMTuple/DecayTree");
kaonm_MU2015->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064787/0000/*.root");
//2015 MAGDOWN
 TChain *kaonm_MD2015 = new TChain("DSt_KMTuple/DecayTree");
kaonm_MD2015->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064785/0000/*.root");
 TChain *pionp_MD2015 = new TChain("DSt_PiPTuple/DecayTree");
pionp_MD2015->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064785/0000/*.root");
TChain *pionm_MD2015 = new TChain("DSt_PiMTuple/DecayTree");
pionm_MD2015->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064785/0000/*.root");
TChain *kaonp_MD2015 = new TChain("DSt_KPTuple/DecayTree");
kaonp_MD2015->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064785/0000/*.root");
//2016 MAGUP
TChain *pionm_MU2016 = new TChain("DSt_PiMTuple/DecayTree");
pionm_MU2016->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064793/0000/*.root");
 TChain *kaonp_MU2016 = new TChain("DSt_KPTuple/DecayTree");
kaonp_MU2016->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064793/0000/*.root");
TChain *pionp_MU2016 = new TChain("DSt_PiPTuple/DecayTree");
pionp_MU2016->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064793/0000/*.root");
 TChain *kaonm_MU2016 = new TChain("DSt_KMTuple/DecayTree");
kaonm_MU2016->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064793/0000/*.root");
//2016 MAGDOWN
 TChain *kaonm_MD2016 = new TChain("DSt_KMTuple/DecayTree");
kaonm_MD2016->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064795/0000/*.root");
 TChain *pionp_MD2016 = new TChain("DSt_PiPTuple/DecayTree");
pionp_MD2016->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064795/0000/*.root");
TChain *pionm_MD2016 = new TChain("DSt_PiMTuple/DecayTree");
pionm_MD2016->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064795/0000/*.root");
TChain *kaonp_MD2016 = new TChain("DSt_KPTuple/DecayTree");
kaonp_MD2016->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064795/0000/*.root");
//2017 MAGUP
TChain *pionm_MU2017 = new TChain("DSt_PiMTuple/DecayTree");
pionm_MU2017->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00090825/0000/*.root");
 TChain *kaonp_MU2017 = new TChain("DSt_KPTuple/DecayTree");
kaonp_MU2017->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00090825/0000/*.root");
TChain *pionp_MU2017 = new TChain("DSt_PiPTuple/DecayTree");
pionp_MU2017->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00090825/0000/*.root");
 TChain *kaonm_MU2017 = new TChain("DSt_KMTuple/DecayTree");
kaonm_MU2017->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00090825/0000/*.root");
//2017 MAGDOWN
 TChain *kaonm_MD2017 = new TChain("DSt_KMTuple/DecayTree");
kaonm_MD2017->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00090823/0000/*.root");
 TChain *pionp_MD2017 = new TChain("DSt_PiPTuple/DecayTree");
pionp_MD2017->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00090823/0000/*.root");
TChain *pionm_MD2017 = new TChain("DSt_PiMTuple/DecayTree");
pionm_MD2017->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00090823/0000/*.root");
TChain *kaonp_MD2017 = new TChain("DSt_KPTuple/DecayTree");
kaonp_MD2017->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00090823/0000/*.root");
//2018 MAGUP
TChain *pionm_MU2018 = new TChain("DSt_PiMTuple/DecayTree");
pionm_MU2018->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00082947/0000/*.root");
 TChain *kaonp_MU2018 = new TChain("DSt_KPTuple/DecayTree");
kaonp_MU2018->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00082947/0000/*.root");
TChain *pionp_MU2018 = new TChain("DSt_PiPTuple/DecayTree");
pionp_MU2018->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00082947/0000/*.root");
 TChain *kaonm_MU2018 = new TChain("DSt_KMTuple/DecayTree");
kaonm_MU2018->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00082947/0000/*.root");
//2018 MAGDOWN
 TChain *kaonm_MD2018 = new TChain("DSt_KMTuple/DecayTree");
kaonm_MD2018->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00082949/0000/*.root");
 TChain *pionp_MD2018 = new TChain("DSt_PiPTuple/DecayTree");
pionp_MD2018->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00082949/0000/*.root");
TChain *pionm_MD2018 = new TChain("DSt_PiMTuple/DecayTree");
pionm_MD2018->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00082949/0000/*.root");
TChain *kaonp_MD2018 = new TChain("DSt_KPTuple/DecayTree");
kaonp_MD2018->Add("/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00082949/0000/*.root");


//Friend Tree relationships
//2015
 pionp_MD2015->AddFriend(kaonm_MD2015,"FriendTree");
 kaonm_MD2015->AddFriend(pionp_MD2015,"FriendTree");
 pionm_MD2015->AddFriend(kaonp_MD2015,"FriendTree");
 kaonp_MD2015->AddFriend(pionm_MD2015,"FriendTree");

 pionp_MD2015->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
pionm_MD2015->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
 kaonm_MD2015->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonm_MD2015->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
kaonp_MD2015->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonp_MD2015->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 pionm_MD2015->SetAlias("logIP","log(_IP)");
 pionp_MD2015->SetAlias("logIP","log(_IP)");

pionp_MU2015->AddFriend(kaonm_MU2015,"FriendTree");
kaonm_MU2015->AddFriend(pionp_MU2015,"FriendTree");
pionm_MU2015->AddFriend(kaonp_MU2015,"FriendTree");
kaonp_MU2015->AddFriend(pionm_MU2015,"FriendTree");

 pionp_MU2015->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
pionm_MU2015->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
 kaonm_MU2015->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonm_MU2015->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
kaonp_MU2015->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonp_MU2015->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 pionm_MU2015->SetAlias("logIP","log(_IP)");
 pionp_MU2015->SetAlias("logIP","log(_IP)");
 //---------------------------------------------------------------
 // 2016
pionp_MD2016->AddFriend(kaonm_MD2016,"FriendTree");
 kaonm_MD2016->AddFriend(pionp_MD2016,"FriendTree");

 pionm_MD2016->AddFriend(kaonp_MD2016,"FriendTree");
 kaonp_MD2016->AddFriend(pionm_MD2016,"FriendTree");

 pionp_MD2016->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
pionm_MD2016->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
 kaonm_MD2016->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonm_MD2016->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
kaonp_MD2016->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonp_MD2016->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 pionm_MD2016->SetAlias("logIP","log(_IP)");
 pionp_MD2016->SetAlias("logIP","log(_IP)");

 
pionp_MU2016->AddFriend(kaonm_MU2016,"FriendTree");
kaonm_MU2016->AddFriend(pionp_MU2016,"FriendTree");
pionm_MU2016->AddFriend(kaonp_MU2016,"FriendTree");
kaonp_MU2016->AddFriend(pionm_MU2016,"FriendTree");

pionp_MU2016->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
pionm_MU2016->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
 kaonm_MU2016->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonm_MU2016->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
kaonp_MU2016->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonp_MU2016->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 pionm_MU2016->SetAlias("logIP","log(_IP)");
 pionp_MU2016->SetAlias("logIP","log(_IP)");
 //---------------------------------------------
 // 2017

pionp_MD2017->AddFriend(kaonm_MD2017,"FriendTree");
kaonm_MD2017->AddFriend(pionp_MD2017,"FriendTree");
pionm_MD2017->AddFriend(kaonp_MD2017,"FriendTree");
kaonp_MD2017->AddFriend(pionm_MD2017,"FriendTree");

 pionp_MD2017->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
pionm_MD2017->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
 kaonm_MD2017->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonm_MD2017->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
kaonp_MD2017->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonp_MD2017->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 pionm_MD2017->SetAlias("logIP","log(_IP)");
 pionp_MD2017->SetAlias("logIP","log(_IP)");

pionp_MU2017->AddFriend(kaonm_MU2017,"FriendTree");
kaonm_MU2017->AddFriend(pionp_MU2017,"FriendTree");
pionm_MU2017->AddFriend(kaonp_MU2017,"FriendTree");
kaonp_MU2017->AddFriend(pionm_MU2017,"FriendTree");
 
pionp_MU2017->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
pionm_MU2017->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
 kaonm_MU2017->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonm_MU2017->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
kaonp_MU2017->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonp_MU2017->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 pionm_MU2017->SetAlias("logIP","log(_IP)");
 pionp_MU2017->SetAlias("logIP","log(_IP)");
 //-----------------------------------------------------
 //2018

pionp_MD2018->AddFriend(kaonm_MD2018,"FriendTree");
kaonm_MD2018->AddFriend(pionp_MD2018,"FriendTree");
pionm_MD2018->AddFriend(kaonp_MD2018,"FriendTree");
kaonp_MD2018->AddFriend(pionm_MD2018,"FriendTree");

 pionp_MD2018->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
pionm_MD2018->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
 kaonm_MD2018->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonm_MD2018->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
kaonp_MD2018->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonp_MD2018->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 pionm_MD2018->SetAlias("logIP","log(_IP)");
 pionp_MD2018->SetAlias("logIP","log(_IP)");

pionp_MU2018->AddFriend(kaonm_MU2018,"FriendTree");
kaonm_MU2018->AddFriend(pionp_MU2018,"FriendTree");
pionm_MU2018->AddFriend(kaonp_MU2018,"FriendTree");
kaonp_MU2018->AddFriend(pionm_MU2018,"FriendTree");


 pionp_MU2018->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
pionm_MU2018->SetAlias("kmu_M","sqrt(Dz_M*Dz_M+105.7*105.7-139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+493.7*493.7)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)))");
 kaonm_MU2018->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonm_MU2018->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
kaonp_MU2018->SetAlias("pipi_M","sqrt(Dz_M*Dz_M-439.68*439.68+139.57*139.57+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+139.57*139.57)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 kaonp_MU2018->SetAlias("kmupi_M","sqrt(Dz_M*Dz_M-439.68*439.68+105.7*105.7+2*sqrt(FriendTree.probe_Brunel_P*FriendTree.probe_Brunel_P+139.57*139.57)*(sqrt(probe_Brunel_P*probe_Brunel_P+105.7*105.7)-sqrt(probe_Brunel_P*probe_Brunel_P+493.68*493.68)))");
 pionm_MU2018->SetAlias("logIP","log(_IP)");
 pionp_MU2018->SetAlias("logIP","log(_IP)");

 TCut popular="probe_P<13000&&probe_P>11000&&probe_Brunel_ETA>3.1&&probe_Brunel_ETA<3.3&&nSPDhits_Brunel>200&&nSPDhits_Brunel<400";
 TCut dmsig="Dst_M-Dz_M>143&&Dst_M-Dz_M<148";
 TH1F *hh;
 
}
