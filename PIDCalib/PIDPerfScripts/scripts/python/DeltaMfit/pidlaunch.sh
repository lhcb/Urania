#!/bin/bash  
echo "entree pidpro.sh ++++" $1
#parameters 1 : particle and period
#parameter 2 : histo file
#parameter 3 : histo name
#parameter 4 dimension of the histo
#paramter 5 output file location
#parameter 6 PIDcut
#parameter 7 general cut 
#parameter 8 companion cut flag 
#parameter 9 workdir
#parameter 10 outfile name
#parameter 11 output histoname
STD_IN=0
#default values
YEAR="2016"
MAGNET_POLARITY="MU"
PARTICLE="kaonm"
VARIABLE="probe_P:probe_ETA:nTracks"
PROBE_CUT="probe_Brunel_MC15TuneV1_ProbNNmu>.5"
COMPANIONFLAG="1"
DIMENSION="3"
WORKDIR="/afs/cern.ch/work/g/guwormse/public/work_pidcalib"
OUTPUTDIR="/eos/lhcb/user/g/guwormse/work_pidcal"
HISTONAME="TotalHist_K_DLLK > 12.0_All__K_P_K_Eta_nTracks"
HISTOFILE="/afs/cern.ch/user/d/dhill/public/forGuy/PerfHists_K_Turbo15_MagUp_P_ETA_nTracks.root"
OUTFILE="DeltaM_"$HISTONAME".root"
OUT_HISTONAME="DeltaM_results_"$HISTONAME
SWITCH="-1"
prefix=""
key=""
value=""
for keyValue in "$@"
do
  case "${prefix}${keyValue}" in
    -y=*|--year=*)  key="-y";     value="${keyValue#*=}";; 
    -m=*|--polarity=*)      key="-m";    value="${keyValue#*=}";;
    -p=*|--particle=*)    key="-p";     value="${keyValue#*=}";;
     -h=*|--histoname=*)    key="-h";     value="${keyValue#*=}";;
     -f=*|--histofile=*)    key="-f";     value="${keyValue#*=}";;
    -b=*|--probe_cut=*)    key="-b";     value="${keyValue#*=}";;
    -v=*|--variable=*)    key="-v";     value="${keyValue#*=}";;
    -g=*|--general_cut=*)    key="-g";     value="${keyValue#*=}";;
    -d=*|--dimension=*)    key="-d";     value="${keyValue#*=}";;
     -w=*|--workdir=*)    key="-w";     value="${keyValue#*=}";;
    -o=*|--outputdir=*)    key="-o";     value="${keyValue#*=}";;
     -c=*|--companionflag=*)    key="-c";     value="${keyValue#*=}";;
     -s=*|--switch=*)    key="-s";     value="${keyValue#*=}";;
     -a=*|--outfile=*)    key="-s";     value="${keyValue#*=}";;
     -e=*|--outhisto=*)    key="-s";     value="${keyValue#*=}";;
     -|--stdin)                key="-";      value=1;;
    *)                                      value=$keyValue;;
  esac
  case $key in
    -y) YEAR="${value}";                 prefix=""; key="";;
    -m) MAGNET_POLARITY="${value}";      prefix=""; key="";;
    -p)  PARTICLE="${value}";            prefix=""; key="";;
    -h)  HISTONAME="${value}";            prefix=""; key="";;
    -f)  HISTOFILE="${value}";            prefix=""; key="";;
    -b)  PROBE_CUT="${value}";           prefix=""; key="";;
    -v)  VARIABLE="${value}";            prefix=""; key="";;
    -g)  GENERAL_CUT="${value}";           prefix=""; key="";;
    -o)  OUTPUTDIR="${value}";              prefix=""; key="";;
    -w)  WORKDIR="${value}";              prefix=""; key="";;
    -d)  DIMENSION="${value}";              prefix=""; key="";;
    -c)  COMPANIONFLAG="${value}";              prefix=""; key="";;
    -s)  SWITCH="${value}";              prefix=""; key="";;
    -a)  OUTFILE="${value}";              prefix=""; key="";;
    -e)  OUT_HISTONAME="${value}";              prefix=""; key="";;
    -)   STD_IN=${value};                prefix=""; key="";; 
    *)   prefix="${keyValue}=";;
  esac
done
echo "Year="$YEAR
echo "Magnet polarity ="$MAGNET_POLARITY
echo "Particle ="$PARTICLE
echo "Probe_cut ="$PROBE_CUT
echo "Variable ="$VARIABLE
echo "General cut ="$GENERAL_CUT
echo "Companion Flag ="$COMPANIONFLAG
echo "Output dir = "$OUTPUTDIR
echo "Work dir = "$WORKDIR
echo "Histo file = "$HISTOFILE
echo "Histo name = "$HISTONAME
echo "Switch parameter ="$SWITCH

# build the pointers
pointer=$PARTICLE"_"$MAGNET_POLARITY$YEAR

echo $pointer
#if switch =0 leave it as is
#switch is -1 when the particle differs from the probe cut, 1 otherwise
if [ $SWITCH != "0" ]
then 
SWITCH="-1"
if expr match "$PROBE_CUT" "ProbNNpi" && expr match "$pointer" "pi"; then
SWITCH="1"
fi
if expr match "$PROBE_CUT" "PIDk" && expr match "$pointer" "ka"; then
SWITCH="1"
fi
if expr match "$PROBE_CUT" "PIDmu" && expr match "$pointer" "mu"; then
SWITCH="1"
fi
if expr match "$PROBE_CUT" "ProbNNk" && expr match "$pointer" "ka"; then
SWITCH="1"
fi
if expr match "$PROBE_CUT" "ProbNNmu" && expr match "$pointer" "mu"; then
SWITCH="1"
fi
fi
echo "switch "$SWITCH

if [ $DIMENSION = "3" ]
then
echo "launch 3D jobs"
echo "Pointer = "$pointer
echo "output file location " $OUTPUTDIR
echo "work file location " $WORKDIR
echo "PIDCut " $PROBE_CUT
echo "Companion cut OK" $COMPANIONFLAG
echo "General cut " $GENERAL_CUT
echo "Dimensio histo " $DIMENSION
VARIABLEX=$(echo $VARIABLE | cut -d':' -f1)
VARIABLEY=$(echo $VARIABLE | cut -d':' -f2)
VARIABLEZ=$(echo $VARIABLE | cut -d':' -f3)
echo "Variable x = "$VARIABLEX
echo "Variable y = "$VARIABLEY
echo "Variable z = "$VARIABLEZ
# copy softwre to workspace
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.cc $WORKDIR/.
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.sh $WORKDIR/.
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.C $WORKDIR/.
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.sub $WORKDIR/.
chmod +x *.sh
cd $WORKDIR
root -l -b <<EOF
.x pidcalib.cc
.L fit_dm_test.C
.L fit_deltam.C
.L eff_pidcal.cc
.x prod_binloop3D_launch.cc("$pointer","$HISTOFILE","$HISTONAME","$OUTPUTDIR","$PROBE_CUT","$GENERAL_CUT","$COMPANIONFLAG","$WORKDIR","$VARIABLEX","$VARIABLEY","$VARIABLEZ","$SWITCH")
.q
EOF
echo "fin travail root"
fi

if [ $DIMENSION = "2" ]
then
echo "launch 2D jobs"
echo "Pointer = "$pointer
echo "output file location " $OUTDIR
echo "work file location " $WORKDIR
echo "PIDCut " $PROBE_CUT
echo "Companion cut OK" $COMPANIONFLAG
echo "General cut " $GENERAL_CUT
echo "Dimensio histo " $DIMENSION
VARIABLEX=$(echo $VARIABLE | cut -d':' -f1)
VARIABLEY=$(echo $VARIABLE | cut -d':' -f2)
echo "Variable x = "$VARIABLEX
echo "Variable y = "$VARIABLEY
# copy softwre to workspace
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.cc $WORKDIR/.
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.sh $WORKDIR/.
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.C $WORKDIR/.
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.sub $WORKDIR/.
chmod +x *.sh
cd $WORKDIR
root -l -b <<EOF
.x pidcalib.cc
.L fit_dm_test.C
.L fit_deltam.C
.L eff_pidcal.cc
.x prod_binloop2D_launch.cc("$pointer","$HISTOFILE","$HISTONAME","$OUTPUTDIR","$PROBE_CUT","$GENERAL_CUT","$COMPANIONFLAG","$WORKDIR","$VARIABLEX","$VARIABLEY","$SWITCH")
.q
EOF
fi
if [ $DIMENSION = "1" ]
then
echo "launch 1D jobs"
echo "Pointer = "$pointer
echo "output file location " $OUTDIR
echo "work file location " $WORKDIR
echo "PIDCut " $PROBE_CUT
echo "Companion cut OK" $COMPANIONFLAG
echo "General cut " $GENERAL_CUT
echo "Dimensio histo " $DIMENSION
echo "Variable x = "$VARIABLE
# copy softwre to workspace
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.cc $WORKDIR/.
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.sh $WORKDIR/.
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.C $WORKDIR/.
cp /afs/cern.ch/work/g/guwormse/public/pidcalib_code/*.sub $WORKDIR/.
chmod +x *.sh
cd $WORKDIR
root -l -b <<EOF
.x pidcalib.cc
.L fit_dm_test.C
.L fit_deltam.C
.L eff_pidcal.cc
.x prod_binloop1Dlaunch.cc("$pointer","$HISTOFILE","$HISTONAME","$OUTPUTDIR","$PROBE_CUT","$GENERAL_CUT","$COMPANIONFLAG","$WORKDIR","$VARIABLE","$SWITCH")
.q
EOF
fi
sleep 2000
nrunning="1"
echo "interrogation condor"
while [ "$nrunning" != 0 ]; do   
condor_q > condor_status.txt
grep  query condor_status.txt | cut -d ',' -f4 >nr.txt
nrunning=$(cat nr.txt | cut -d' ' -f2)
echo $nrunning 
done
cd $OUTPUTDIR
root -l -b <<EOF
.x $WORKDIR/collect.cc("$OUTFILE","$OUT_HISTONAME","$HISTOFILE","$HISTONAME","$DIMENSION")
.q
EOF
echo "File " $OUTFILE " created successfully"
