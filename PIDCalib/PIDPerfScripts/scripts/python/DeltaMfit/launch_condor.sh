#
#parameter lists
#1 :ffd for the bin
#2 :sub for the bin
#3 :bin cut
#4 sample
#5 write_directory
#6 work directory
#7 switch parameter
echo "launch condor work directory" $6
echo "launch condor write directory" $5
sed s/zzzz/"out_"$2".txt"/ submit0.sub >t.sub
sed s/wwww/$1/ binloop0.cc >rr.cc
sed s/qqqq/$3/ rr.cc >$3.cc
sed s/yyyy/$3/ launch_binloop0.sh >t1.sh
#transmit paraameters to the binloop routine
sed s/z_sample/$4/ t1.sh >t3.sh
sed s/z_switch/$7/ t3.sh >t2.sh
#escape slashq
perl -pi -e  s{z_write_directory}{$5} t2.sh
perl -pi -e  s{z_workdirectory}{$6} t2.sh
cp t2.sh launch_$3.sh
sed s/wwwww/$1/ t.sub >tt.sub
sed s/ssss/$3/ tt.sub >$2.sub
condor_submit $2.sub
