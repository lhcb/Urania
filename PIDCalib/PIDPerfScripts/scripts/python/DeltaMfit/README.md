The DeltaM package is integrated into PIDCALIB to provide an estimate of the PID efficiencies  using the D*+ -> pi+ (D0->K-pi+) PIDCALIB rootuples uniquely based on the Fit of the DeltaM spectrum in each bin.
1) Presentation of the method
 The rationale for this is to profit from the unique feature of the DeltaM variable to be vrey largely insensitive to variations of the track parameters that can be caused by interaction with teh detctor material or by a decay in flight. This leads, in principle, to a more reliable estimate of the PID efficiencies and misID rates, since there are correlations between PID performance and track degradation. However, this method is more prone to physics background present in therootuples due to other D0 decays such pi+pi-pi0, Kmu nu, pimunu. To remove these backgrounds, cuts are applied to the other track, so called companion cuts. These cuts are selected automatically and a flag is there to remove them for systematic studies. The use of an uncorrelated companion cut (such as K positive ID when looking to the pion travk) P induces an overestimate of the track efficiency of teh order of 0,2%. This is due to the fact that these two tracks share the same global environment and the same same hardware status. Note however that this is good to have this correlation within PIDCALIB if your analysis requires PID criteria on two tracks since teh asme effect will be present in your analysis.
Detailed presentation of DeltaM method and comparison with the present default of sWeight method are available in presentations by G. Wormser to the Run1 run2 performances working group in Januray and February 2020. In a nutshell, the pi->pi PID efficiencies are lower by those computed by the sWeight method by 1% and pi->mu and K->mu midID rates are larger than those predicted by SWeight by factors to ~2 at low momentum decaying to +50% around 30 GeV and +20% at high momentum.

2) usage
It has been decied that the tool to transmit the binning information required by the user, and to store the deltaM method results, is an HISTOGRAM,. This histogram is constructed at the beginning of the PIDCALIB process. The deltaM package can accomodate 1D, 2D or 3D histograms. The package is launched by a single command line
./pidlaunch.sh 
where arguments can be given using options. All options have default values but the user shoud at least specify:
-f the file where is located the histogram defining the binning scheme
-h the name of that histogram
-d the dimension of the histogram ("1","2" or "3") (default 3)
-v "VariableX:VariableY:VariableZ" is the string containing the name of the variables to be scanned in each dimension. Examples are : -v "probe_P" for a 1D histo as function of the track momentum, 
                           -v "probe_P:probe_ETA" for 2 D histo function of momentum and eta
            (default)      -v "probe_P:probe_ETA:nTracks" for 3D histo fction of momentum, eta and nTracks
-b the PID cut which needs to be probed: example -b "probe_Brunel_MC15TuneV1_ProbNNmu>.5" (default)
-w the work directory where the code will be copied and ancillary files will be written
-o the OUTDIR where the histogram containing in each bin the result will be stored.
-m magnet polarity to be studied
-p particle to be studied ("pionm","pionp","kaonm","kaonp")
-y Year to be studied (only run2 data) (2015,2016,2017,2018)
Other optional commands are 
    -g)  GENERAL_CUT will aply a cut on all tracks.can be quality track cut such as "probe_Brunel_TRCHI2NDOF<4.0&&probe_Brunel_TRACK_GHOSTPROB<0.35" or a companion cut to replace the one by default when associated with the flag -c "0" 
    -c)  COMPANIONFLAG ( default 1) when 0 , suppresses the default comapnion cuts
    -s)  SWITCH         (computed by default) when 0 replaces the deltaM results by sWeight results. If not 0 
    -a)  OUTFILE : (default same name of input file) name of the outfile which will contain the histogram contains the results 
    -e)  OUT_HISTONAME (not yet implemented) (default "out_histo")

3) Workflow
The pidlaunch.sh command will result in launching as many condor jobs as there are bins in the histogram.
In each job, the PIDCALIB rootuples will be read twice : once with the cuts defining the bin, plus the companion cut if any, and plus the general cut if any. The DeltaM spectrum fit will be fitted and the result store both in *.root file and in an ascii file called out_sub_i_j_k.txt for bin (i,j,k) in the work directory. the second time the probe cut will be added for misID measurement or the anti-probe cut will be used for efficiency meazsurement (this is to reduce the statistical error on the efficiency). The results will be store in the same ascii file. Each job will store in the OUTPUTDIRECTORY 
a) an ascii file called resol_i_j_k.txt containg the deltaM result for that bin, 
b)two *.jpg files showing the deltaM spectrum before and after the PID cut,
c)   the *.root file containing the results of the two fits.
The program will then wait for 2000 s (36 minutes) , will check whether condor jobs are still running, and as soon as the last condor job is finished, will construct the result histogram by reading back the resol_i_j_k.txt ascii files.
