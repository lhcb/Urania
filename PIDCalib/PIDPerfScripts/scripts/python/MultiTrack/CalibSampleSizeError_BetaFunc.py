#!/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import division

from past.utils import old_div
from PIDPerfScripts.StartScreen import *
from PIDPerfScripts.Definitions import *

import ROOT
import sys
import argparse
import warnings
import time
import math
import os.path
from array import array

import numpy as np


class ShowArgumentsParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n\n' % message)
        parser.print_usage(sys.stderr)
        sys.stderr.write('\n' + self.description)
        sys.exit(2)


if '__main__' == __name__:

    start()
    print("")

    parser = ShowArgumentsParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        prog=os.path.basename(sys.argv[0]),
        description=
        """Estimate the statistical uncertainty due to the size of the calibration sample for a given:
        a) Stripping version, <stripVersion> (e.g. \"20\")
        b) Magnet polarity <magPol> (\"MagUp\" or \"MagDown\")
        c) Output file from PerformMultiTrackCalib.py <multiTrackFileName>
        d) Number of smeared histograms to create <nHists>

The results will be stored in the ROOT file <resultsFileName>.

The remaining positional argument, <track>, must be specified in the same format
as in PerformMultiTrackCalib.py

For a full list of arguments, do: 'python {0} -h'

e.g. python {0} \"Turbo16\" \"MagDown\" \"/output/from/PerformMultiTrackCalib.root\" \"nHists\" \"outputFileName.root\" \\
        \"[Kaon,K,DLLK>4.0]\" \"[Pion,Pi,DLLK<4.0]\"


""".format(os.path.basename(sys.argv[0])))

    ## add the positional arguments
    parser.add_argument(
        'stripVersion',
        metavar='<stripVersion>',
        help="Sets the stripping version")
    parser.add_argument(
        'magPol', metavar='<magPol>', help="Sets the magnet polarity")
    parser.add_argument(
        'multiTrackFileName',
        metavar='<multiTrackFileName>',
        help='File containing the output of running PerformMultiTrackCalib.py')
    parser.add_argument(
        'nHists',
        metavar='<nHists>',
        type=int,
        default=100,
        help='Number of smeared histograms to loop through')
    parser.add_argument(
        'resultsFileName',
        metavar='<resultsFileName>',
        help='Name of the output file')
    parser.add_argument(
        'tracks',
        metavar='<track>',
        nargs='+',
        help="Sets the name of one of the reference tracks, "
        "its track type and the PID cut to apply")

    ## add the optional arguments
    parser.add_argument(
        '-t',
        '--inputTree',
        dest='inputTreeName',
        metavar='<multiTrackTreeName>',
        default='CalibTool_PIDCalibTree',
        help='The name of the TTree in <multiTrackFileName>')
    parser.add_argument(
        "-q",
        "--quiet",
        dest="verbose",
        action="store_false",
        default=True,
        help="suppresses the printing of verbose information")
    parser.add_argument(
        "-d",
        "--debug",
        dest="debug",
        action="store_true",
        default=False,
        help="print out some extra info")

    parser.add_argument(
        "-i",
        "--inputDir",
        dest="inputDir",
        metavar="DIR",
        help=("directory containing the performance histograms "
              "(default: current directory)"))

    binGroup = parser.add_argument_group("binning options")
    binGroup.add_argument(
        "-X",
        "--xVarName",
        dest="xVarName",
        metavar="NAME",
        default="P",
        help=("Sets the NAME of the 1st (x) bin variable "
              "in the calibration sample "
              "(default: %(default)s)"))
    binGroup.add_argument(
        "-Y",
        "--yVarName",
        dest="yVarName",
        metavar="NAME",
        default="ETA",
        help=("Sets the NAME of the 2nd (y) bin variable "
              "in the calibration sample "
              "(default: %(default)s). "
              "If 1D binning is required, then this option should "
              "be set to an empty string"))
    binGroup.add_argument(
        "-Z",
        "--zVarName",
        dest="zVarName",
        metavar="NAME",
        default="nTracks",
        help=("Sets the NAME of the 3rd (z) bin variable "
              "in the calibration sample "
              "(default: %(default)s). "
              "If 2D binning is required, then this option should "
              "be set to an empty string"))
    binGroup.add_argument(
        "-s",
        "--schemeName",
        dest="schemeNames",
        nargs=2,
        action='append',
        metavar=("TYPE", "NAME"),
        help=("Sets the NAME of the binning scheme for particles "
              "of type TYPE, as defined in the module "
              "'PIDPerfScripts.binning'. This option can be used "
              "multiple times to specify the binning scheme for "
              "each particle type in the reference sample"
              "If this option is not set for a particular particle "
              "type, the default binning scheme is used."))

    opts = parser.parse_args()

    StripVersion = opts.stripVersion
    CheckStripVer(StripVersion)

    MagPolarity = opts.magPol
    CheckMagPol(MagPolarity)

    trackArgs = opts.tracks

    trackList = []
    trackTypes = []
    trackNames = []
    samecutTracks = {}
    for arg in trackArgs:
        trackName = None
        trackType = None
        pidCut = None
        if not arg.startswith("[") or not arg.endswith("]"):
            parser.error("Invalid track variable string '{0}'".format(arg))
        trackInfo = arg[1:-1].split(",")
        if len(trackInfo) != 3:
            parser.error("Invalid track variable string '{0}'".format(arg))
        trackName = trackInfo[0].strip()
        trackType = trackInfo[1].strip()
        pidCut = trackInfo[2].strip().replace("/", "_div_")

        CheckPartType(trackType)

        if trackType not in trackTypes:
            trackTypes.append(trackType)
        trackNames.append(trackName)
        trackList.append((trackName, trackType, pidCut))

        #Keep a list of track names that should share the same set of smeared values
        trackTypeAndCut = (trackType, pidCut)
        if trackTypeAndCut in samecutTracks:
            samecutTracks[trackTypeAndCut].append(trackName)
        else:
            samecutTracks[trackTypeAndCut] = [trackName]

    if opts.verbose:
        print('\n====================================')
        print('These tracks share the same type and cut value:')
        for key in samecutTracks:
            print('{} : {}'.format(key, samecutTracks[key]))
        print('====================================\n')

    XVarName = opts.xVarName
    if XVarName == '':
        parser.error("Argument to --xBinVarName is an empty string.")

    YVarName = opts.yVarName
    ZVarName = opts.zVarName

    if ZVarName != '' and YVarName == '':
        msg = ("Argument to --yVarName is an empty string,"
               "but argument to --zVarName. If you planned to"
               "include only two binning variables. Did you"
               "mean to do --yVarName='{0}' --zVarName='{1}'?").format(
                   ZVarName, YVarName)
        parser.error(msg)

    for vname_calib in (XVarName, YVarName, ZVarName):
        if vname_calib == '': continue
        CheckVarName(vname_calib)

    SchemeNames = {} if opts.schemeNames is None else dict(opts.schemeNames)

    if opts.verbose:
        print('====================================')
        print('Retrieving output of MultiTrack tool')
        print('from file: ', opts.multiTrackFileName)
        print('from TTree: ', opts.inputTreeName)
        print('====================================')
        print("Stripping version: %s" % StripVersion)
        print("Magnet polarity: %s" % MagPolarity)
        for i, v in enumerate(trackList):
            print(
                "Track name, type and PID cut (track {num}): {trInfo}".format(
                    num=i, trInfo=str(v)))
        print('====================================\n')

    #===========================================================================
    # Start the timer
    #===========================================================================
    start = time.time()
    print(time.asctime(time.localtime()))

    #===========================================================================
    # Retrieve the output from PerformMultiTrackCalib.py
    #===========================================================================
    inputFile = ROOT.TFile.Open(opts.multiTrackFileName)
    if not inputFile:
        raise IOError('Failed to open the file {}'.format(
            opts.multiTrackFileName))

    inputTree = inputFile.Get(opts.inputTreeName)
    if not inputTree:
        raise ValueError('Failed to get the TTree {} from file {}'.format(
            opts.inputTreeName, opts.multiTrackFileName))
    if opts.debug:
        print('The tree has {} entries\n'.format(inputTree.GetEntries()))

    #===========================================================================
    # Retrieve the total and passed hists from the performance TFile
    #===========================================================================
    perfFiles = {}
    for trackType in trackTypes:
        fnameSuffix = ''
        if trackType in SchemeNames:
            fnameSuffix += '_{0}'.format(SchemeNames[trackType])
        for vname in (XVarName, YVarName, ZVarName):
            if vname == '': continue
            fnameSuffix += '_{0}'.format(vname)
        if 'Turbo' not in StripVersion:
            fname = "PerfHists_{part}_Strip{strp}_{pol}{suf}.root".format(
                part=trackType,
                strp=StripVersion,
                pol=MagPolarity,
                suf=fnameSuffix)
        elif 'Turbo' in StripVersion:
            fname = "PerfHists_{part}_{strp}_{pol}{suf}.root".format(
                part=trackType,
                strp=StripVersion,
                pol=MagPolarity,
                suf=fnameSuffix)
        if opts.inputDir is not None:
            fname = "%s/%s" % (opts.inputDir, fname)

        f_Perf = ROOT.TFile.Open(fname)
        if not f_Perf:
            raise IOError("Failed to open file {0} for reading".format(fname))
        perfFiles[trackType] = f_Perf

    perfArrays = {}
    for key in samecutTracks:
        trackType, pidCut = key
        f_Perf = perfFiles[trackType]
        totalHistName = 'TotalHist_{}_{}_All_'.format(trackType, pidCut)
        for vname in (XVarName, YVarName, ZVarName):
            if vname == '':
                continue
            if 'nTracks' in vname or 'nSPDHits' in vname:
                totalHistName += '_{}'.format(vname)
            else:
                totalHistName += '_{}_{}'.format(trackType, vname)
            if 'ETA' in totalHistName and 'Turbo' not in StripVersion:
                totalHistName = totalHistName.replace('ETA', 'Eta')
        passedHistName = totalHistName.replace('TotalHist', 'PassedHist')
        totalPerfHist = f_Perf.Get(totalHistName)
        passedPerfHist = f_Perf.Get(passedHistName)

        totalPerfHist.ClearUnderflowAndOverflow()
        passedPerfHist.ClearUnderflowAndOverflow()

        if not totalPerfHist or not passedPerfHist:
            msg = ('Failed to retrieve the histograms\n'
                   '{}\n'
                   '{}\n'
                   'from file {}').format(totalHistName, passedHistName,
                                          f_Perf.GetName())
            raise ValueError(msg)

        perfArrays[key] = {}
        perfArrays[key]['Total'] = np.array(totalPerfHist, dtype=np.float64)
        perfArrays[key]['Passed'] = np.array(passedPerfHist, dtype=np.float64)

        if opts.verbose:
            msg = ('Track key {}: Got total/passed histograms\n'
                   '{}\n'
                   '{}\n'
                   'from file {}').format(key, totalHistName, passedHistName,
                                          f_Perf.GetName())
        if opts.debug:
            print(perfArrays[key]['Total'].size,
                  perfArrays[key]['Total'].dtype)
            print(perfArrays[key]['Passed'].size,
                  perfArrays[key]['Total'].dtype)

        del totalPerfHist, passedPerfHist

    #===========================================================================
    # Create the smeared values by sampling from a Beta distribution, B(a,b)
    # The two histograms give for each bin (Npass, Ntotal), from which:
    #       a == Npass + 1
    #       b == Ntotal - Npass +1
    # see: http://home.fnal.gov/~paterno/images/effic.pdf
    #===========================================================================
    smearedValues = {}
    for key in samecutTracks:
        totArray = perfArrays[key]['Total']  #This does not copy the array!
        passArray = perfArrays[key]['Passed']
        arraySize = totArray.size

        #Need to deal with bins with negative nEvents (due to sWeights)
        invalidBins_totArray = totArray < 0.0  #Bool array where negative==True
        invalidBins_passArray = passArray < 0.0
        invalidBins_other = passArray > totArray
        invalidBins_all = invalidBins_totArray + invalidBins_passArray + invalidBins_other
        posInvalidBins = np.where(invalidBins_all == True)[0]
        nInvalidBins = invalidBins_all.sum()
        if nInvalidBins:
            print('\n====================================')
            print(('There are {} bin(s) with negative values '
                   'in the performance histograms!\n'
                   'You may want to check the binning for {}.').format(
                       nInvalidBins, key))
            print('This tool will continue running and ignore events '
                  'falling in those bins\n')
            print('PassedHist invalid indices: {}'.format(
                np.where(passArray < 0.0)[0]))
            print('TotalHist invalid indices: {}'.format(
                np.where(totArray < 0.0)[0]))
            print('In these bins nPass > nTot: {}'.format(
                np.where(passArray > totArray)[0]))
            print('====================================\n')

        #Use the invalid bool array to mask the original arrays
        totArray[invalidBins_all] = 0.0
        passArray[invalidBins_all] = 0.0

        smearedValues[key] = np.random.beta(
            passArray + np.ones(arraySize),
            totArray - passArray + np.ones(arraySize),
            (opts.nHists, arraySize))

        print(key, smearedValues[key].shape)

    #===========================================================================
    # Set up the necessary branch addresses from the MultiTrack output
    #===========================================================================
    print('Loading the required branches from the TTree')
    trackBinNumbers = {}
    for trk in trackNames:
        trackBinNumbers[trk] = array('i', [0])
        inputTree.SetBranchAddress(trk + '_PIDCalibBinNumber',
                                   trackBinNumbers[trk])
    if opts.debug:
        print('Check if values using addresses and GetLeaf().GetValue() match')
        trk = trackNames[0]
        for i in (5, 17, 51):
            inputTree.GetEntry(i)
            leafVal = inputTree.GetLeaf(trk + '_PIDCalibBinNumber').GetValue()
            print('Entry number', i)
            print('Value from GetLeaf(): ', leafVal)
            print('Value from address: ', trackBinNumbers[trk][0])
        print('')

    #===========================================================================
    # Loop through the events in inputTree
    # For each track, check if it is in an invalid bin and skip the event if so
    #===========================================================================
    nEntries = inputTree.GetEntries()
    avgEffs = np.zeros(
        opts.nHists, dtype=np.float64)  #Store the smeared average efficiencies
    nInvalidEntries = 0
    for i in range(nEntries):
        if i % (old_div(nEntries, 20)) == 0:
            print('Looped through {} out of {} events'.format(i, nEntries))

        inputTree.GetEntry(i)
        tempVals = np.ones(opts.nHists, dtype=np.float64)

        badValue = False
        for trk in trackList:
            trkName = trk[0]
            key = (trk[1], trk[2])

            binNumber = trackBinNumbers[trkName][0]
            if binNumber in posInvalidBins:
                badValue = True
                break

            tempVals *= smearedValues[key][:, binNumber]

        if badValue:
            if opts.verbose:
                print(
                    'One or more tracks in event {} fall into an invalid bin!'.
                    format(i))
                print(
                    'The tool will ignore and skip this event, please check your binning.'
                )
            nInvalidEntries += 1
        else:
            avgEffs += tempVals

    nValidEntries = nEntries - nInvalidEntries
    if nInvalidEntries:
        print('\n====================================')
        print('There were {} invalid events that have been ignored.'.format(
            nInvalidEntries))
        print('====================================\n')

    avgEffs = old_div(avgEffs, nValidEntries)
    if opts.debug:
        print(avgEffs.shape, type(avgEffs))
        print("The first ten smeared average event efficiencies are:")
        print(avgEffs[:10])
    print("The mean of all histograms is {:0.6}".format(avgEffs.mean()))

    #===========================================================================
    # Save things to the output file
    #===========================================================================
    print("Storing results in file: {0}".format(opts.resultsFileName))

    resultsFile = ROOT.TFile(opts.resultsFileName, "recreate")
    resultsTree = ROOT.TTree("Tree_CalibSampleError", "Tree_CalibSampleError")
    smearedEff = array("d", [0])
    resultsTree.Branch("smearedEff", smearedEff, "smearedEff/D")

    # Loop over all smeared average efficiencies
    for eff in avgEffs:
        smearedEff[0] = eff
        resultsTree.Fill()
    resultsTree.Write()
    print("TTree filled!")

    # Draw a histogram and fit a Gaussian to it
    print("Fitting a Gaussian to the results")
    c1 = ROOT.TCanvas('c1', 'c1', 800, 600)
    resultsTree.Draw("smearedEff>>hist")
    hist = ROOT.gDirectory.Get("hist")
    hist.Fit("gaus")
    plotName = opts.resultsFileName.replace('.root', '.png')
    c1.SaveAs(plotName)

    print("File saved!")

    end = time.time()
    minutes, seconds = divmod(end - start, 60)
    print(time.asctime(time.localtime()))
    print("Time taken: {:0.0f} minutes and {:0.2f} seconds".format(
        int(minutes), seconds))
