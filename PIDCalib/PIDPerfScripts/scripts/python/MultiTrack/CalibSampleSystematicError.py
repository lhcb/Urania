#!/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from PIDPerfScripts.StartScreen import *

import ROOT
import sys
import argparse
import warnings
import time
import math
import os.path
from array import array

import numpy as np
import root_numpy


class ShowArgumentsParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n\n' % message)
        parser.print_usage(sys.stderr)
        sys.stderr.write('\n' + self.description)
        sys.exit(2)


if '__main__' == __name__:

    start()
    print("")

    parser = ShowArgumentsParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        prog=os.path.basename(sys.argv[0]),
        description=
        """Estimate the systematic uncertainty from the choice of calibration sample used.
        This is done by comparing the per-event efficiencies obtained using the nominal/alternative calibration samples.
        Please not that the alternative calibration samples are only available for Run 2.

        The inputs required are:
        a) MultiTrack output using the nominal calibration sample <multiTrackFileName_nom>
        b) MultiTrack output using the alternative calibration sample <multiTrackFileName_alt>

The results will be stored in the ROOT file <resultsFileName>.

For a full list of arguments, do: 'python {0} -h'

e.g. python {0} \"/output/from/PerformMultiTrackCalib_1.root\" \"/output/from/PerformMultiTrackCalib_2.root\" \"outputFileName.root\"


""".format(os.path.basename(sys.argv[0])))

    ## add the positional arguments
    parser.add_argument(
        'multiTrackFileName_nom',
        metavar='<multiTrackFileName_nom>',
        help=(
            'File containing the output of running PerformMultiTrackCalib.py '
            'with the nominal calibration sample'))
    parser.add_argument(
        'multiTrackFileName_alt',
        metavar='<multiTrackFileName_alt>',
        help=(
            'File containing the output of running PerformMultiTrackCalib.py '
            'with the alternative calibration sample'))
    parser.add_argument(
        'resultsFileName',
        metavar='<resultsFileName>',
        help='Name of the output file')

    ## add the optional arguments
    parser.add_argument(
        '-t1',
        '--inputTree1',
        dest='inputTreeName_nom',
        metavar='<multiTrackTreeName_nom>',
        default='CalibTool_PIDCalibTree',
        help='The name of the TTree in <multiTrackFileName>')
    parser.add_argument(
        '-t2',
        '--inputTree2',
        dest='inputTreeName_alt',
        metavar='<multiTrackTreeName_alt>',
        default='CalibTool_PIDCalibTree',
        help='The name of the TTree in <multiTrackFileName>')
    parser.add_argument(
        "-q",
        "--quiet",
        dest="verbose",
        action="store_false",
        default=True,
        help="suppresses the printing of verbose information")
    parser.add_argument(
        "-d",
        "--debug",
        dest="debug",
        action="store_true",
        default=False,
        help="print out some extra info")

    opts = parser.parse_args()

    if opts.verbose:
        print('====================================')
        print('Retrieving output of MultiTrack tool')
        print('from file: ', opts.multiTrackFileName_nom)
        print('from TTree: ', opts.inputTreeName_nom)
        print('\n and\n')
        print('from file: ', opts.multiTrackFileName_alt)
        print('from TTree: ', opts.inputTreeName_alt)
        print('====================================\n')

    #===========================================================================
    # Start the timer
    #===========================================================================
    start = time.time()
    print(time.asctime(time.localtime()))

    #===========================================================================
    # Retrieve both outputs from PerformMultiTrackCalib.py
    #===========================================================================
    inputFile_nom = ROOT.TFile.Open(opts.multiTrackFileName_nom)
    if not inputFile_nom:
        raise IOError('Failed to open the file {}'.format(
            opts.multiTrackFileName_nom))

    inputTree_nom = inputFile_nom.Get(opts.inputTreeName_nom)
    if not inputTree_nom:
        raise ValueError('Failed to get the TTree {} from file {}'.format(
            opts.inputTreeName_nom, opts.multiTrackFileName_nom))

    nEntries_nom = inputTree_nom.GetEntries()
    print('The tree {} has {} entries\n'.format(opts.inputTreeName_nom,
                                                nEntries_nom))

    inputFile_alt = ROOT.TFile.Open(opts.multiTrackFileName_alt)
    if not inputFile_alt:
        raise IOError('Failed to open the file {}'.format(
            opts.multiTrackFileName_alt))

    inputTree_alt = inputFile_alt.Get(opts.inputTreeName_alt)
    if not inputTree_alt:
        raise ValueError('Failed to get the TTree {} from file {}'.format(
            opts.inputTreeName_alt, opts.multiTrackFileName_alt))

    nEntries_alt = inputTree_alt.GetEntries()
    print('The tree {} has {} entries\n'.format(opts.inputTreeName_alt,
                                                nEntries_alt))

    if nEntries_nom != nEntries_alt:
        raise ValueError(
            'The two TTrees do not have the same number of entries!')

    #===========================================================================
    # Extract the per-event efficiencies from the both TTrees into numpy arrays
    # and calculate the difference in these.
    #===========================================================================
    eventEffs_nom = root_numpy.tree2array(
        inputTree_nom, branches='Event_PIDCalibEff')
    eventEffs_alt = root_numpy.tree2array(
        inputTree_alt, branches='Event_PIDCalibEff')

    invalidEvents_nom = np.logical_or(eventEffs_nom < 0.0, eventEffs_nom > 1.0)
    nInvalidEvents_nom = invalidEvents_nom.sum()
    invalidEvents_alt = np.logical_or(eventEffs_alt < 0.0, eventEffs_alt > 1.0)
    nInvalidEvents_alt = invalidEvents_alt.sum()

    invalidEvents_total = invalidEvents_nom + invalidEvents_alt
    nInvalidEvents_total = invalidEvents_total.sum()
    if nInvalidEvents_total:
        print('There are {} events with unphysical efficiencies!'.format(
            nInvalidEvents_total))
        print('In file {} there are {} invalid events'.format(
            opts.multiTrackFileName_nom, nInvalidEvents_nom))
        print('In file {} there are {} invalid events'.format(
            opts.multiTrackFileName_alt, nInvalidEvents_alt))
        print(
            'This tool will ignore these events; You may want to check the binning used'
        )

    eventEffs_nom = eventEffs_nom[np.logical_not(invalidEvents_total)]
    eventEffs_alt = eventEffs_alt[np.logical_not(invalidEvents_total)]

    deltaEventEffs = eventEffs_nom - eventEffs_alt

    #===========================================================================
    # Close the input file
    #===========================================================================
    del inputTree_nom, inputTree_alt
    inputFile_nom.Close()
    inputFile_alt.Close()

    #===========================================================================
    # Save things to the output file
    #===========================================================================
    print("Storing results in file: {0}".format(opts.resultsFileName))

    resultsFile = ROOT.TFile(opts.resultsFileName, "recreate")
    resultsTree = ROOT.TTree("Tree_CalibSystError", "Tree_CalibSystError")
    deltaEff = array("d", [0])
    resultsTree.Branch("deltaEff", deltaEff, "deltaEff/D")

    # Loop over all bootstrapped average efficiencies
    for err in deltaEventEffs:
        deltaEff[0] = err
        resultsTree.Fill()
    resultsTree.Write()
    print("TTree filled!")

    # Draw a histogram and fit a Gaussian to it
    print("Fitting a Gaussian to the results")
    c1 = ROOT.TCanvas('c1', 'c1', 800, 600)
    resultsTree.Draw("deltaEff>>hist")
    hist = ROOT.gDirectory.Get("hist")
    hist.Fit("gaus")
    plotName = opts.resultsFileName.replace('.root', '.png')
    c1.SaveAs(plotName)

    print("File saved!")

    end = time.time()
    minutes, seconds = divmod(end - start, 60)
    print(time.asctime(time.localtime()))
    print("Time taken: {:0.0f} minutes and {:0.2f} seconds".format(
        int(minutes), seconds))
