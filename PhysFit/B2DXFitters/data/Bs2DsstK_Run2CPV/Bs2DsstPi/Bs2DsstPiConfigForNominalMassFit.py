def getconfig() :

    configdict = {}
    
    from math import pi
    from math import log

    # considered decay mode                                                       
    configdict["Decay"] = "Bs2DsstPi"
    configdict["Backgrounds"] = ["Bs2DsRho","Bs2DsstRho","Bd2DsstPi"]
    # configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK", "KPiPi", "PiPiPi"}
    configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK"}
    # year of data taking                                                                                 
    configdict["YearOfDataTaking"] = {"2015", "2016"}
    # stripping (necessary in case of PIDK shapes)                               
    configdict["Stripping"] = {"2012":"21", "2011":"21r1"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {"2011": {"Down": 0.5600, "Up": 0.4200},
                                          "2012": {"Down": 0.9912, "Up": 0.9988},
                                          "2015": {"Down": 0.1540, "Up": 0.1740}, 
                                          "2016": {"Down": 0.8325, "Up": 0.8325}  
                                          }
    # file name with paths to MC/data samples                                     
    configdict["dataName"] = "../data/Bs2DsstK_Run2CPV/Bs2DsstPi/config_Bs2DsstPi.txt"

    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = { "Directory": "PlotBs2DsstPi", "Extension":"pdf"}

    # basic variables                                                                                        
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5100,    5900    ], "InputName" : "FBs_DsstMC_M"}
    configdict["BasicVariables"]["CharmMass"]     = { "Range" : [2080,    2150    ], "InputName" : "FDsst_DeltaM_M"}
    configdict["BasicVariables"]["BeautyTime"]    = { "Range" : [0.4,     12.0    ], "InputName" : "FBs_LF_ctau"}
    configdict["BasicVariables"]["BacP"]          = { "Range" : [1000.,   650000.0], "InputName" : "FBac_P"}
    configdict["BasicVariables"]["BacPT"]         = { "Range" : [500.,    45000.0 ], "InputName" : "FBac_Ptr"}
    configdict["BasicVariables"]["BacPIDK"]       = { "Range" : [-150.0,  150.0   ], "InputName" : "FBac_PIDK"}
    configdict["BasicVariables"]["nTracks"]       = { "Range" : [15.,     1000.0  ], "InputName" : "FnTracks"}
    configdict["BasicVariables"]["BeautyTimeErr"] = { "Range" : [0.01,    0.1     ], "InputName" : "FBs_LF_ctauErr"}
    configdict["BasicVariables"]["BacCharge"]     = { "Range" : [-1000.0, 1000.0  ], "InputName" : "FBac_ID"}
    configdict["BasicVariables"]["BDTG"]          = { "Range" : [0.01,    1.0     ], "InputName" : "FBDT_Var"}
    configdict["BasicVariables"]["TagDecOS"]      = { "Range" : [-1.0,    1.0     ], "InputName" : "FBs_TAGDECISION_OS"}
    configdict["BasicVariables"]["TagDecSS"]      = { "Range" : [-1.0,    1.0     ], "InputName" : "FBs_SS_nnetKaon_DEC"}
    configdict["BasicVariables"]["MistagOS"]      = { "Range" : [0.0,     0.5     ], "InputName" : "FBs_TAGOMEGA_OS"}
    configdict["BasicVariables"]["MistagSS"]      = { "Range" : [0.0,     0.5     ], "InputName" : "FBs_SS_nnetKaon_PROB"}
    '''
    # additional variables in data sets
    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["BacEta"]   = { "Range" : [-9.0,    9.0     ], "InputName" : "FBac_Eta"}
    configdict["AdditionalVariables"]["PhEta"]    = { "Range" : [-9.0,    9.0     ], "InputName" : "FPh_Eta"}
    configdict["AdditionalVariables"]["PhPT"]     = { "Range" : [ 0.0,    1.e+9   ], "InputName" : "FPh_Ptr"}
    configdict["AdditionalVariables"]["PhCL"]     = { "Range" : [ 0.0,    1.0     ], "InputName" : "FPh_CL"}
    configdict["AdditionalVariables"]["PhisNotE"] = { "Range" : [ 0.0,    1.0     ], "InputName" : "FPh_isNotE"}
    configdict["AdditionalVariables"]["BsEta"]    = { "Range" : [-9.0,    9.0     ], "InputName" : "FBs_Eta"}
    configdict["AdditionalVariables"]["BsPhi"]    = { "Range" : [ 0.0,    9.0     ], "InputName" : "FBs_Phi"}
    configdict["AdditionalVariables"]["BsPT"]     = { "Range" : [ 0.0,    1.e+9   ], "InputName" : "FBs_Ptr"}
    configdict["AdditionalVariables"]["DsDec"]    = { "Range" : [ 0.0,    6.0     ], "InputName" : "FDs_Dec"}
    configdict["AdditionalVariables"]["KpPT"]     = { "Range" : [100.,45000.0     ], "InputName" : "FKp_Ptr"}
    configdict["AdditionalVariables"]["KmPT"]     = { "Range" : [100.,45000.0     ], "InputName" : "FKm_Ptr"}
    configdict["AdditionalVariables"]["PiPT"]     = { "Range" : [100.,45000.0     ], "InputName" : "FPi_Ptr"}
    configdict["AdditionalVariables"]["PtrRel"]   = { "Range" : [ 0.0,  200.      ], "InputName" : "FPtr_Rel"}
    configdict["AdditionalVariables"]["CosTheS"]  = { "Range" : [-1.0,     1.0    ], "InputName" : "FCTS_Ds"}
    configdict["AdditionalVariables"]["BsIpChi2Own"] = { "Range" : [ 0.0,  100.    ], "InputName" : "FBs_IpChi2Own"}
    configdict["AdditionalVariables"]["BsDIRAOwn"]= { "Range" : [-1.0,     1.0    ], "InputName" : "FBs_DIRAOwn"}
    configdict["AdditionalVariables"]["BsRFD"]    = { "Range" : [ 0.0,   100.0    ], "InputName" : "FBs_RFD"}
    configdict["AdditionalVariables"]["DsDIRAOri"]= { "Range" : [-1.0,     1.0    ], "InputName" : "FDs_DIRAOri"}
    configdict["AdditionalVariables"]["nSPDHits"] = { "Range" : [ 0.0,  1000.0    ], "InputName" : "FnSPDHits"}
    '''
    
    # tagging calibration                                                                                               
    configdict["TaggingCalibration"] = {}
    configdict["TaggingCalibration"]["OS"] = {"p0": 0.3834, "p1": 0.9720, "average": 0.3813}
    configdict["TaggingCalibration"]["SS"] = {"p0": 0.4244, "p1": 1.2180, "average": 0.4097}

    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"]    = {"Data": "FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>111.5&&(FDsst_M-FDs_M)<181.5&&FDelta_R<1.&&FBDT_Var>0.01&&FBac_PIDK<0&&(FDsBac_M-FDs_M<3350||FDsBac_M-FDs_M>3500)",
                                              "MC":   "FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>111.5&&(FDsst_M-FDs_M)<181.5&&FDelta_R<1.&&FBDT_Var>0.01&&((FDsBac_M-FDs_M<3350||FDsBac_M-FDs_M>3500))"}
    configdict["AdditionalCuts"]["NonRes"] = {"Data":"FDs_Dec==3.", "MC":"FDs_Dec==3."}
    configdict["AdditionalCuts"]["PhiPi"]  = {"Data":"FDs_Dec==1.", "MC":"FDs_Dec==1."}
    configdict["AdditionalCuts"]["KstK"]   = {"Data":"FDs_Dec==2.", "MC":"FDs_Dec==2."}
    configdict["AdditionalCuts"]["KPiPi"]  = {"Data":"FDs_Dec==5.&&FDs_FDCHI2_ORIVX>9.&&FPi_PIDK>10.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FKst_M<1750.",
                                              "MC":  "FDs_Dec==5.&&FDs_FDCHI2_ORIVX>9.&&FKst_M<1750."}
    configdict["AdditionalCuts"]["PiPiPi"] = {"Data":"FDs_Dec==4.&&FDs_FDCHI2_ORIVX>9.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FPi_PIDK<0.&&FPhi_M<1700.&&FKst_M<1700.", 
                                              "MC":  "FDs_Dec==4.&&FDs_FDCHI2_ORIVX>9.&&FPhi_M<1700.&&FKst_M<1700."}
 
    configdict["CreateCombinatorial"] = {}
    configdict["CreateCombinatorial"]["BeautyMass"] = {} 
    configdict["CreateCombinatorial"]["BeautyMass"]["All"]    = {"Cut":"FBs_DsstMC_M>5100.&&FBs_DsstMC_M<6000.&&FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>195.&&(FDsst_M-FDs_M)<205.&&FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.01&&FBac_P>1000.&&FBac_P<650000.&&FBac_Ptr>500.&&FBac_Ptr<45000.&&FBac_PIDK<0",
                                                                 "Rho":3.5, "Mirror":"Left"}
    configdict["CreateCombinatorial"]["BeautyMass"]["NonRes"] = {"Cut":"FDs_Dec==3."}
    configdict["CreateCombinatorial"]["BeautyMass"]["PhiPi"]  = {"Cut":"FDs_Dec==1."}
    configdict["CreateCombinatorial"]["BeautyMass"]["KstK"]   = {"Cut":"FDs_Dec==2."}
    configdict["CreateCombinatorial"]["BeautyMass"]["KPiPi"]  = {"Cut":"FDs_Dec==5.&&FDs_FDCHI2_ORIVX>9.&&FPi_PIDK>10.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FKst_M<1750."}
    configdict["CreateCombinatorial"]["BeautyMass"]["PiPiPi"] = {"Cut":"FDs_Dec==4.&&FDs_FDCHI2_ORIVX>9.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FPi_PIDK<0.&&FPhi_M<1700.&&FKst_M<1700."}
    
    configdict["CreateCombinatorial"]["BacPIDK"] = {}
    configdict["CreateCombinatorial"]["BacPIDK"]["All"]       = {"Cut":"FBs_DsstMC_M>5100.&&FBs_DsstMC_M<6000.&&FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>195.&&(FDsst_M-FDs_M)<205.&&FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.01&&FBac_P>1000.&&FBac_P<650000.&&FBac_Ptr>500.&&FBac_Ptr<45000.",
                                                              "Rho":1.25, "Mirror":"Left"}

    # configdict["WeightingMassTemplates"] = { "RatioDataMC":{"FileLabel":{"2011":"#RatioDataMC 2011 PNTr","2012":"#RatioDataMC 2012 PNTr"},
    #                                                         "Var":["FBac_P","FnTracks"],"HistName":"histRatio"},
    #                                          "Shift":{ "BeautyMass": 3.3, "CharmMass": 2.8} }
    # configdict["WeightingMassTemplates"] = { "Shift":{ "BeautyMass": 3.3, "CharmMass": 2.8} }
    #configdict["WeightingMassTemplates"]={"PIDBachEff":      {"FileLabel":{"2011":"#PIDK Pion 2011","2012":"#PIDK Pion 2012"},
    #                                                          "Var":["nTracks","lab1_P"],"HistName":"MyPionEff_0_mu2"},
    #                                      "PIDBachMisID":    {"FileLabel":{"2011":"#PIDK Kaon 2011","2012":"#PIDK Kaon 2012"},
    #                                                          "Var":["nTracks","lab1_P"],"HistName":"MyKaonMisID_0_mu2"},
    #                                      "PIDChildKaonPionMisID":{"FileLabel":{"2011":"#PIDK Pion 2011","2012":"#PIDK Pion 2012"},
    #                                                               "Var":["nTracks","lab3_P"],"HistName":"MyPionMisID_0"},
    #                                      "PIDChildProtonMisID": {"FileLabel":{"2011":"#PIDK Proton 2011","2012":"#PIDK Proton 2012"},
    #                                                              "Var":["nTracks","lab4_P"],"HistName":"MyProtonMisID_pKm5_KPi5"},
    #                                      "RatioDataMC":{"FileLabel":{"2011":"#RatioDataMC 2011 PNTr","2012":"#RatioDataMC 2012 PNTr"},
    #                                                     "Var":["lab1_P","nTracks"],"HistName":"histRatio"}, 
    #                                      "Shift":{"BeautyMass":-2.0,"CharmMass":0.0}}
    configdict["WeightingMassTemplates"] = { }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    
    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"]    = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"]    = {"Run2": {"All": 5.3679e+03}, "Fixed":False}
    configdict["SignalShape"]["BeautyMass"]["sigma1"]  = {"Run2": {"All": 1.3350e+01}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["sigma2"]  = {"Run2": {"All": 2.1248e+01}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["alpha1"]  = {"Run2": {"All":-2.1055e+00}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["alpha2"]  = {"Run2": {"All": 1.6940e+00}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All": 1.7665e+00}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All": 1.3685e+00}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["frac"]    = {"Run2": {"All": 5.2800e-01}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["R"]    =    {"Run2": {"All": 1.},         "Fixed":False}
    
    # Ds signal shapes
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"]["type"]    = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["CharmMass"]["mean"]    = {"Run2": {"All": 2.1123e+03}, "Fixed":False}
    configdict["SignalShape"]["CharmMass"]["sigma1"]  = {"Run2": {"All": 9.7264e+00}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["sigma2"]  = {"Run2": {"All": 9.0858e+00}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["alpha1"]  = {"Run2": {"All": 1.2374e+00}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["alpha2"]  = {"Run2": {"All":-2.9204e-01}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["n1"]      = {"Run2": {"All": 1.1724e+02}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["n2"]      = {"Run2": {"All": 4.9987e+01}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["frac"]    = {"Run2": {"All": 4.7483e-01}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["R"]    =    {"Run2": {"All": 1.},         "Fixed":False}


    # combinatorial background              
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    
    #configdict["CombBkgShape"]["BeautyMass"]["type"] = "RooKeysPdf"
    #configdict["CombBkgShape"]["BeautyMass"]["name"] = {"2015": {"NonRes": "PhysBkgCombPi_BeautyMassPdf_m_both_nonres_2015",
    #                                                             "PhiPi":  "PhysBkgCombPi_BeautyMassPdf_m_both_phipi_2015",
    #                                                             "KstK":   "PhysBkgCombPi_BeautyMassPdf_m_both_kstk_2015",
    #                                                             "KPiPi":  "PhysBkgCombPi_BeautyMassPdf_m_both_kpipi_2015",
    #                                                             "PiPiPi": "PhysBkgCombPi_BeautyMassPdf_m_both_pipipi_2015"},
    #                                                    "2016": {"NonRes": "PhysBkgCombPi_BeautyMassPdf_m_both_nonres_2016",
    #                                                             "PhiPi":  "PhysBkgCombPi_BeautyMassPdf_m_both_phipi_2016",
    #                                                             "KstK":   "PhysBkgCombPi_BeautyMassPdf_m_both_kstk_2016",
    #                                                             "KPiPi":  "PhysBkgCombPi_BeautyMassPdf_m_both_kpipi_2016",
    #                                                             "PiPiPi": "PhysBkgCombPi_BeautyMassPdf_m_both_pipipi_2016"}}


    configdict["CombBkgShape"]["BeautyMass"]["type"]    = "RooHILLidni"
    configdict["CombBkgShape"]["BeautyMass"]["a"]       = {"Run2": {"All": 4.56e+03}, "Fixed":True}
    configdict["CombBkgShape"]["BeautyMass"]["b"]       = {"Run2": {"All": 5.52e+03}, "Fixed":True}
    configdict["CombBkgShape"]["BeautyMass"]["csi"]     = {"Run2": {"NonRes": 6.5470e-01,  "PhiPi": 1.5286e-01, "KstK":2.7711e-01, "KPiPi": 1.7093e+00, "PiPiPi": 1.7093e+00}, "Fixed":True}
    configdict["CombBkgShape"]["BeautyMass"]["shift"]   = {"Run2": {"All": 0.0},         "Fixed":True}
    configdict["CombBkgShape"]["BeautyMass"]["sigma"]   = {"Run2": {"NonRes": 2.3374e+01,  "PhiPi": 6.4875e+01, "KstK": 7.0380e+01, "KPiPi":3.13119e+01, "PiPiPi":3.13119e+01}, "Fixed":True}
    configdict["CombBkgShape"]["BeautyMass"]["R"]       = {"Run2": {"NonRes": 2.4807e+01,  "PhiPi": 8.1272e+00, "KstK": 5.1103e+00, "KPiPi":6.84048e+00, "PiPiPi":6.84048e+00},"Fixed":False}
    configdict["CombBkgShape"]["BeautyMass"]["frac"]    = {"Run2": {"NonRes": 5.7905e-01,  "PhiPi": 9.4966e-01, "KstK": 9.2764e-01, "KPiPi": 3.9837e-01, "PiPiPi": 3.9837e-01}, "Fixed":True}

    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"]  = "ExponentialPlusSignal" 
    configdict["CombBkgShape"]["CharmMass"]["cD"]    = {"Run2": {"NonRes":0.001, "PhiPi":0.001, "KstK":0.001, "KPiPi":0.001, "PiPiPi":0.001}, "Fixed":False}
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {"Run2": {"NonRes":0.5,   "PhiPi":0.5,   "KstK":0.5,   "KPiPi":0.5,   "PiPiPi":0.5},   "Fixed":False}

    configdict["CombBkgShape"]["BacPIDK"] = {}
    configdict["CombBkgShape"]["BacPIDK"]["type"] = "Fixed"
    configdict["CombBkgShape"]["BacPIDK"]["components"] = { "Kaon":True, "Pion":True, "Proton":False }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK1"] = { "Run1":{"PhiPi":0.5, "NonRes":0.5, "KstK":0.5, "KPiPi":0.5, "PiPiPi":0.5}, "Fixed":False }


    #configdict["Lb2DspShape"] = {}
    #configdict["Lb2DspShape"]["BeautyMass"] = {}
    #configdict["Lb2DspShape"]["BeautyMass"]["type"]    = "Gaussian"
    #configdict["Lb2DspShape"]["BeautyMass"]["sigma"]   = {"Run2": {"All": 20.0}, "Fixed":False}
    #configdict["Lb2DspShape"]["BeautyMass"]["mean"]    = {"Run2": {"All": 5650.0}, "Fixed":False}

    configdict["Bd2DsstPiShape"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"]["type"]    = "ShiftedSignal"
    configdict["Bd2DsstPiShape"]["BeautyMass"]["shift"]   = {"Run2": {"All": 86.8}, "Fixed":True}
    configdict["Bd2DsstPiShape"]["BeautyMass"]["scale1"]  = {"Run2": {"All": 1.00}, "Fixed":True}
    configdict["Bd2DsstPiShape"]["BeautyMass"]["scale2"]  = {"Run2": {"All": 1.00}, "Fixed":True}

    #expected yields
    configdict["Yields"] = {} 
    configdict["Yields"]["Bs2DsstRho"] = {"2016": { "NonRes":4000.0,  "PhiPi":4000.0,  "KstK":4000.0,  "KPiPi":4000.0,  "PiPiPi":4000.0},  
                                          "2015": { "NonRes":2000.0,  "PhiPi":2000.0,  "KstK":2000.0,  "KPiPi":2000.0,  "PiPiPi":2000.0}, "Fixed":False}
    configdict["Yields"]["Bd2DsstPi"]  = {"2016": { "NonRes":1000.0,  "PhiPi":1000.0,  "KstK":1000.0,  "KPiPi":1000.0,  "PiPiPi":1000.0},
                                          "2015": { "NonRes":1000.0,  "PhiPi":1000.0,  "KstK":1000.0,  "KPiPi":1000.0,  "PiPiPi":1000.0}, "Fixed":False}
    configdict["Yields"]["Bs2DsRho"]   = {"2016": { "NonRes":4000.0,  "PhiPi":4000.0,  "KstK":4000.0,  "KPiPi":4000.0,  "PiPiPi":4000.0},
                                          "2015": { "NonRes":2000.0,  "PhiPi":2000.0,  "KstK":2000.0,  "KPiPi":2000.0,  "PiPiPi":2000.0}, "Fixed":False}
    configdict["Yields"]["CombBkg"]    = {"2016": { "NonRes":20000.0, "PhiPi":20000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
                                          "2015": { "NonRes":10000.0, "PhiPi":10000.0, "KstK":10000.0, "KPiPi":10000.0, "PiPiPi":10000.0}, "Fixed":False}
    configdict["Yields"]["Signal"]     = {"2016": { "NonRes":20000.0, "PhiPi":50000.0, "KstK":20000.0, "KPiPi":5000.0,  "PiPiPi":5000.0},
                                          "2015": { "NonRes":20000.0, "PhiPi":50000.0, "KstK":20000.0, "KPiPi":2500.0,  "PiPiPi":2500.0}, "Fixed":False}
    #configdict["Yields"]["Lb2Dsp"]  = {"2016": { "NonRes":1000.0,  "PhiPi":1000.0,  "KstK":1000.0,  "KPiPi":1000.0,  "PiPiPi":1000.0},
    #                                   "2015": { "NonRes":1000.0,  "PhiPi":1000.0,  "KstK":1000.0,  "KPiPi":1000.0,  "PiPiPi":1000.0}, "Fixed":False}
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#    
    ###                                                               MDfit plotting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Bd2DsstPi", "Bs2DsRho", "Bs2DsstRho","Lb2Dsp"]
    configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kOrange, kBlue-10, kGreen+3, kRed]
    #configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Bs2DsRho", "Bs2DsstRho"]
    #configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kBlue-10, kGreen+3]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {"Position":[0.53, 0.45, 0.90, 0.91], "TextSize": 0.05, "LHCbText":[0.35,0.9]}
    configdict["LegendSettings"]["CharmMass"]  = {"Position":[0.20, 0.69, 0.93, 0.93], "TextSize": 0.05, "LHCbText":[0.8,0.66],
                                                  "ScaleYSize":1.75, "SetLegendColumns":2, "LHCbTextSize":0.075 }
    configdict["LegendSettings"]["BacPIDK"]    = {"Position":[0.20, 0.69, 0.93, 0.93], "TextSize": 0.05, "LHCbText":[0.2,0.9], 
                                                  "ScaleYSize":1.5, "SetLegendColumns":2, "LHCbTextSize":0.075}

    return configdict
