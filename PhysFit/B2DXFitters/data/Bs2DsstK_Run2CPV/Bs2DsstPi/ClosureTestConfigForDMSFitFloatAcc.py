def getconfig() :

    from Bs2DsstPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    from math import pi

    # PHYSICAL PARAMETERS
    configdict["Gammas"]        = 6.568954e-01
    configdict["DeltaGammas"]   = -1.096968e-01
    configdict["DeltaMs"]       = 17.81
    configdict["TagEffSig"]     = 0.403
    configdict["TagOmegaSig"]   = 0.391
    configdict["StrongPhase"]   = 20. / 180. * pi
    configdict["WeakPhase"]     = 70./180.*pi
    configdict["ModLf"]         = 0.372
    configdict["CPlimit"]       = {"upper":4.0, "lower":-4.0} 


    configdict["TaggingCalibration"] = {}

    configdict["FixAcceptance"] = False
    #configdict["ConstrainsForTaggingCalib"] = True
    configdict["ConstrainsForTaggingCalib"] = False

    configdict["UsedResolution"] = "Nominal"

    # Nominal Resolution and corresponding tagging parameters
    configdict["Resolution"] = { "scaleFactor":{"p0":0.0, "p1":1.046, "p2":0.0}, # {"p0":0.010262, "p1":1.280, "p2":0.0},
                                 "meanBias":0.0,
                                 "shape": { "sigma1":2.14946e-02, "sigma2":3.67643e-02, "sigma3":6.32869e-02,
                                            "frac1":3.72147e-01, "frac2":5.65150e-01},
                                 "templates": { "fileName":"work_templates_dspi.root",
                                                "workName":"work",
                                                "templateName":"TimeErrorPdf_Bs2DsPi"} }
    '''
    configdict["TaggingCalibration"]["SS"] = {"p0": 0.44119, "dp0": 0.0, "p1": 1.0868, "dp1": 0.0,
                                              "cov": [ [2.903e-05, 0.0, 0.0001613, 0.0],
                                                       [0.0, 1.0, 0.0, 0.0],
                                                       [0.0001613, 0.0, 0.006101, 0.0],
                                                       [0.0, 0.0, 0.0, 1.0]],
                                              "average": 0.43744, "tagEff":0.63926, "aTagEff":0.0, "use":True}
    configdict["TaggingCalibration"]["OS"] = {"p0": 0.37718, "dp0": 0.0, "p1": 1.1244, "dp1": 0.0,
                                              "cov": [ [5.212e-05, 0.0, 0.0002286, 0.0],
                                                       [0.0, 1.0, 0.0, 0.0],
                                                       [0.0002286, 0.0, 0.006685, 0.0],
                                                       [0.0, 0.0, 0.0, 1.0]],
                                              "average": 0.369798, "tagEff":0.37151, "aTagEff":0.0, "use":True}
    '''
    configdict["TaggingCalibration"]["SS"] = {"p0": 4.26292e-01, "dp0": 0.0, "p1": 1.17119e+00, "dp1": 0.0,
                                              "cov": [ [1.0, 0.0, 0.0, 0.0],
                                                       [0.0, 1.0, 0.0, 0.0],
                                                       [0.0, 0.0, 1.0, 0.0],
                                                       [0.0, 0.0, 0.0, 1.0]],
#                                              "average": 0.4311, "tagEff": 0.6801705055, "aTagEff":0.0, "use":True}
                                              "average": 4.14836e-01, "tagEff":0.707351, "aTagEff":0.0, "use":True}
#                                              "average": 0.4311, "tagEff":0.70123, "aTagEff":0.0, "use":True}
    configdict["TaggingCalibration"]["OS"] = {"p0": 3.45826e-01, "dp0": 0.0, "p1": 8.74013e-01, "dp1": 0.0,
                                              "cov": [ [1.0, 0.0, 0.0, 0.0],
                                                       [0.0, 1.0, 0.0, 0.0],
                                                       [0.0, 0.0, 1.0, 0.0],
                                                       [0.0, 0.0, 0.0, 1.0]],
#                                              "average": 0.3597, "tagEff":0.3766009593, "aTagEff":0.0, "use":True}
                                              "average": 3.62469e-01, "tagEff":0.371004, "aTagEff":0.0, "use":True}

    # up to 12 ps
    configdict["Acceptance"] = { "knots":  [0.5,  1.,  2.,  3., 7.0],
                                 "values": [0.5, 0.5, 0.5, 0.5, 1.0] }
    # up to 15 ps trial
    # configdict["Acceptance"] = { "knots":  [0.5,  1.,  2.,  3.,  6., 12.0],
    #                             "values": [0.5, 0.5, 0.5, 0.5, 0.5,  1.0] }

    configdict["constParams"] = []
    configdict["constParams"].append('Gammas_Bs2DsstPi')
    configdict["constParams"].append('deltaGammas_Bs2DsstPi')
    configdict["constParams"].append('C_Bs2DsstPi')
    configdict["constParams"].append('Cbar_Bs2DsstPi')
    configdict["constParams"].append('S_Bs2DsstPi')
    configdict["constParams"].append('Sbar_Bs2DsstPi')
    configdict["constParams"].append('D_Bs2DsstPi')
    configdict["constParams"].append('Dbar_Bs2DsstPi')
    configdict["constParams"].append('tagEff_OS')
    configdict["constParams"].append('tagEff_SS')
    configdict["constParams"].append('tagEff_Both')
    configdict["constParams"].append('aTagEff_OS')
    configdict["constParams"].append('aTagEff_SS')
    configdict["constParams"].append('aTagEff_Both')
    if configdict["FixAcceptance"] == True:
        configdict["constParams"].append('var1')
        configdict["constParams"].append('var2')
        configdict["constParams"].append('var3')
        configdict["constParams"].append('var4')
        configdict["constParams"].append('var5')
        configdict["constParams"].append('var6')
        configdict["constParams"].append('var7')
    if configdict["ConstrainsForTaggingCalib"] == False:
        configdict["constParams"].append('p0_OS')
        configdict["constParams"].append('p0_SS')
        configdict["constParams"].append('p1_OS')
        configdict["constParams"].append('p1_SS')
    configdict["constParams"].append('dp0_OS')
    configdict["constParams"].append('dp0_SS')
    configdict["constParams"].append('dp1_OS')
    configdict["constParams"].append('dp1_SS')
    configdict["constParams"].append('p0_mean_OS')
    configdict["constParams"].append('p0_mean_SS')
    configdict["constParams"].append('p1_mean_OS')
    configdict["constParams"].append('p1_mean_SS')
    configdict["constParams"].append('dp0_mean_OS')
    configdict["constParams"].append('dp0_mean_SS')
    configdict["constParams"].append('dp1_mean_OS')
    configdict["constParams"].append('dp1_mean_SS')
    configdict["constParams"].append('average_OS')
    configdict["constParams"].append('average_SS')
#    configdict["constParams"].append('DeltaMs_Bs2DsPi')

    return configdict
