###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsKConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    configdict["pdfList"] = {}
    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsK"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsK"]["BeautyMass"][
        "IpatiaPlusJohnsonSU"] = {
            "Title": "B_{s}#rightarrowD_{s}K",
            "Bins": 200,
            "Min": 5000.0,
            "Max": 6000.0,
            "Unit": "MeV/c^{2}",
            "mean": [5367.9, 5300, 5400],
            "sigmaI": [21.7, 10.0, 50.0],
            "sigmaJ": [13.2, 10.0, 50.0],
            "zeta": [0.0],
            "fb": [0.0],
            "a1": [0.94, 0.2, 5.0],
            "a2": [1.91, 0.2, 5.0],
            "n1": [1.4, 0.01, 6.0],
            "n2": [2.1, 0.01, 3.0],
            "l": [-1.7],  # for stability
            "tau": [0.34, 0.0, 2.0],
            "nu": [-0.2, -2.0, 0.5],
            "fracI": [0.19, 0.01, 0.99]
        }

    configdict["pdfList"]["Bs2DsstK"] = {}
    configdict["pdfList"]["Bs2DsstK"]["Bs2DsK"] = {}
    configdict["pdfList"]["Bs2DsstK"]["Bs2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsstK"]["Bs2DsK"]["BeautyMass"][
        "HILLdiniPlusHORNSdini"] = {
            "Title": "B_{s}#rightarrowD_{s}#rho",
            "Bins": 100,
            "Min": 5000.0,
            "Max": 5400.0,
            "Unit": "MeV/c^{2}",
            "ahill": [4550, 4300, 5300],
            "bhill": [5340, 4300, 5500],
            "csihill": [-1.4, -3.0, 3.0],
            "sigma": [40.0, 2.0, 80.0],
            "ahorns": [5100.0, 5000, 5150],
            "bhorns": [5200.0, 5150, 5300],
            "csihorns": [1.4, 0.1, 2.8],
            "sigmahorns": [14.0, 3.0, 30.0],
            "shift": [0.0],
            "R": [2.0, 0.5, 40.0],
            "frac": [0.8, 0.0, 1.0],
            "frachorns": [0.5, 0.0, 0.7]
        }

    configdict["pdfList"]["Bs2DsKst"] = {}
    configdict["pdfList"]["Bs2DsKst"]["Bs2DsK"] = {}
    configdict["pdfList"]["Bs2DsKst"]["Bs2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsKst"]["Bs2DsK"]["BeautyMass"]["HORNSdini"] = {
        "Title": "B_{s}#rightarrowD_{s}#rho",
        "Bins": 100,
        "Min": 5000.0,
        "Max": 5400.0,
        "Unit": "MeV/c^{2}",
        "a": [4100, 3500, 4500],
        "b": [5200, 4300, 5500],
        "csi": [1.1, -2.0, 2.0],
        "sigma": [40.0, 2.0, 80.0],
        "shift": [0.0],
        "R": [5.8, 0.5, 40.0],
        "frac": [0.98, 0.0, 1.0],
    }

    configdict["pdfList"]["Bs2DsstKst"] = {}
    configdict["pdfList"]["Bs2DsstKst"]["Bs2DsK"] = {}
    configdict["pdfList"]["Bs2DsstKst"]["Bs2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsstKst"]["Bs2DsK"]["BeautyMass"][
        "CrystalBall"] = {
            "Title": "B_{s}#rightarrowD_{s}#rho",
            "Bins": 80,
            "Min": 5000.0,
            "Max": 5250.0,
            "Unit": "MeV/c^{2}",
            "mean": [4900.0, 4500.0, 5200.0],
            "sigma": [50.0, 0., 100.0],
            "n": [2.0],
            "alpha": [1.5, -4.02, 4.0],
        }

    configdict["pdfList"]["Lb2Dsstp"] = {}
    configdict["pdfList"]["Lb2Dsstp"]["Bs2DsK"] = {}
    configdict["pdfList"]["Lb2Dsstp"]["Bs2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Lb2Dsstp"]["Bs2DsK"]["BeautyMass"][
        "CrystalBall"] = {
            "Title": "B_{s}#rightarrowD_{s}#rho",
            "Bins": 80,
            "Min": 5000.0,
            "Max": 5350.0,
            "Unit": "MeV/c^{2}",
            "mean": [4900.0, 4500.0, 5200.0],
            "sigma": [50.0, 0., 100.0],
            "n": [2.0],
            "alpha": [1.5, -4.02, 4.0],
        }

    #Dataset prefix
    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Bs2DsK_",
        "Bs2DsstK": "dataSetMC_Bs2DsstK_",
        "Bs2DsKst": "dataSetMC_Bs2DsKst_",
        "Bs2DsstKst": "dataSetMC_Bs2DsstKst_",
        "Lb2Dsstp": "dataSetMC_Lb2Dsstp_",
    }

    #Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {
        "Bs2DsK": "m(D_{s}^{\mp}K^{\pm}) [MeV/c^{2}]"
    }

    return configdict
