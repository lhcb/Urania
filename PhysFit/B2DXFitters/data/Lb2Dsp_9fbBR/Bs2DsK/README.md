# Contact:
in case of troubles contact: [Agnieszka Dziurda](agnieszka.dziurda@cern.ch)

# Files:
- Bs2DsKConfigForNominalMassFit.py
  - nominal config file
- config_Bs2DsK.txt
  - paths to files 

# Saving logfile

Please use one of two options:
- >& logfile.txt &
- |& tee logfile.txt

# How to run code

You need to be in:
cd UraniaDev_v7r0/PhysFit/B2DXFitters/scripts/

# Obtaining data sample:
```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Bs2DsK/Bs2DsKConfigForNominalMassFit.py --debug --Data -s work_data_bs2dsk_for_lb2dsp.root > & log_work_data_bs2dsk_for_lb2dsp.txt &
```

# Obtaining MC templates:
```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Bs2DsK/Bs2DsKConfigForNominalMassFit.py --debug --MC -s work_mc_bs2dsk_for_lb2dsp.root >& log_work_mc_bs2dsk_for_lb2dsp.txt
```
Due to memory limits probably you won't be able to do it in one go.
The best is to split by year: 
```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Bs2DsK/Bs2DsKConfigForNominalMassFit.py --debug --MC -s work_mc_bs2dsk_for_lb2dsp_2015.root >& log_work_mc_bs2dsk_for_lb2dsp_2015.txt &
```
and then load this workspace and continute with next year, for example:
```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Bs2DsK/Bs2DsKConfigForNominalMassFit.py --debug --MC -i work_mc_bs2dsk_for_lb2dsp_2015.root -s work_mc_bs2dsk_for_lb2dsp_20152016.root >& log_work_mc_bs2dsk_for_lb2dsp_20152016.txt &
```


# Performing MDFit
```
../../../run python runMDFitter.py --configName ../data/Lb2Dsp_5fbBR/Bs2DsK/Bs2DsKConfigForNominalMassFit.py --fileName /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_mc_bs2dsk_for_lb2dsp_2015201620172018.root  --fileData /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_data_bs2dsk_for_lb2dsp.root --mode kkpi --year 2015 2016 2017 2018 --pol both --merge pol -s WS_MDFit_Bs2DsK_for_Lb2Dsp.root --dim 1 --debug >& log_mdfit_bs2dsk_for_lb2dsp.txt &
```

# Plotting results
```
../../../run python plotMDFitter.py WS_MDFit_Bs2DsK_for_Lb2Dsp.root --configName ../data/Lb2Dsp_5fbBR/Bs2DsK/Bs2DsKConfigForNominalMassFit.py --mode kkpi --year 2015 2016 2017 2018 --pol both --merge pol --bin 200 --log
```
