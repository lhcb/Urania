###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsKConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #restricting to Run1 data
    configdict["YearOfDataTaking"] = {"2011", "2012"}

    # additional Run1 trigger requirements
    HLT_L0 = "(lab0_L0HadronDecision_TOS == 1 || lab0_L0Global_TIS ==1)"
    HLT_1 = "(lab0_Hlt1TrackAllL0Decision_TOS ==1) "
    HLT_2 = "(lab0_Hlt2Topo2BodyBBDTDecision_TOS ==1 || lab0_Hlt2Topo3BodyBBDTDecision_TOS == 1 || lab0_Hlt2Topo4BodyBBDTDecision_TOS ==1)"
    app = "&&"

    bkg_cat = "lab0_BKGCAT<60 && lab2_BKGCAT<60"

    configdict["AdditionalCuts"]["All"][
        "MC"] = HLT_L0 + app + HLT_1 + app + HLT_2 + app + bkg_cat

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    ### Weighting MC samples ###
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>10.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>10.0_All;"
            }
        },
        "PIDBachMisID": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>10.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>10.0_All;"
            }
        },
        "PIDBachProtonMisID": {
            "2011": {
                "FileLabel": "#PIDK Proton 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_DLLK>10.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Proton 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_DLLK>10.0_All;"
            }
        },
        "PIDChildKaonPionMisID": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_DLLK>10.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_DLLK>10.0_All;"
            }
        },
        "PIDChildProtonMisID": {
            "2011": {
                "FileLabel": "#PIDK Proton 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_DLLK>5.0&&(DLLK-DLLp>5.0)_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Proton 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_DLLK>5.0&&(DLLK-DLLp>5.0)_All;"
            }
        },
        "RatioDataMC": {
            "2011": {
                "FileLabel": "#DataMC 2011",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2012": {
                "FileLabel": "#DataMC 2012",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            }
        },
        "Shift": {
            "2011": {
                "BeautyMass": -2.0,
                "CharmMass": 0.0
            },
            "2012": {
                "BeautyMass": -2.0,
                "CharmMass": 0.0
            }
        }
    }

    return configdict
