###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsPiConfigForMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    # year of data taking
    configdict["YearOfDataTaking"] = {"2015", "2016", "2017", "2018"}

    # additional Run2 trigger requirements
    HLT_L0 = "(lab0_L0HadronDecision_TOS == 1 || lab0_L0Global_TIS ==1)"
    HLT_1 = "(lab0_Hlt1TrackMVADecision_TOS == 1 || lab0_Hlt1TwoTrackMVADecision_TOS == 1)"
    HLT_2 = "(lab0_Hlt2Topo2BodyDecision_TOS == 1 || lab0_Hlt2Topo3BodyDecision_TOS == 1 || lab0_Hlt2Topo4BodyDecision_TOS==1)"
    app = "&&"

    configdict["AdditionalCuts"]["All"][
        "Data"] += app + HLT_L0 + app + HLT_1 + app + HLT_2

    return configdict
