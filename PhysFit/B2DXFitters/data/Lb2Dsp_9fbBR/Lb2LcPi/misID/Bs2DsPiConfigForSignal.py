###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsPiConfigForMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    configdict["pdfList"] = {}

    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["BeautyMass"][
        "DoubleCrystalBall"] = {
            "Title": "B_{s}#rightarrowD_{s}#pi",
            "Bins": 160,
            "Min": 5400.0,
            "Max": 6200.0,
            "Unit": "MeV/c^{2}",
            "sameMean": True,
            "mean": [5623.0, 5500, 5700],
            "sigma1": [18.74, 1.0, 100.0],
            "sigma2": [12.23, 1.0, 100.0],
            "alpha1": [1.657, 0.01, 6.0],
            "alpha2": [-2.063, -6.0, -0.01],
            "n1": [1.518, 0.01, 6.0],
            "n2": [2.30, 0.01, 6.0],
            "frac": [0.5]
        }

    configdict["pdfList"]["Signal"]["Bs2DsPi"]["BeautyMass"]["Ipatia"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 160,
        "Min": 5300.0,
        "Max": 5600.0,
        "Unit": "MeV/c^{2}",
        "mean": [5368.2, 5300, 5400],
        "sigma": [17.3, 1.0, 100.0],
        "zeta": [0.0],
        "fb": [0.0],
        "l": [-4.5, -10.0, -0.01],
        "a1": [1.5, 0.01, 10.0],
        "a2": [2.8, 0.01, 10.0],
        "n1": [4.3, 0.01, 10.0],
        "n2": [2.2, 0.01, 10.0]
    }

    configdict["pdfList"]["Signal"]["Bs2DsPi"]["BeautyMass"][
        "IpatiaPlusJohnsonSU"] = {
            "Title": "B_{s}#rightarrowD_{s}#pi",
            "Bins": 160,
            "Min": 5400.0,
            "Max": 6200.0,
            "Unit": "MeV/c^{2}",
            "mean": [5620.6, 5500, 5700],
            "sigmaI": [14.5, 10.0, 50.0],
            "sigmaJ": [14.2, 10.0, 50.0],
            "zeta": [0.0],
            "fb": [0.0],
            "a1": [1.4, 0.2, 5.0],
            "a2": [1.7, 0.2, 5.0],
            "n1": [1.6, 0.01, 6.0],
            "n2": [2.1, 0.01, 3.0],
            "l": [-1.7],  # for stability
            "tau": [0.3, 0.0, 2.0],
            "nu": [-0.1, -2.0, 0.5],
            "fracI": [0.25]  #for stability
        }

    #Dataset prefix
    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Bs2DsPi_",
    }

    #Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {
        "Bs2DsPi": "m(D_{s}^{-}#pi^{+}) [MeV/c^{2}]"
    }

    return configdict
