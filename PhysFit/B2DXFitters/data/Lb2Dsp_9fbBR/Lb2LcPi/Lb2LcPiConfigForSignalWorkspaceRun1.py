###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2LcPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #restricting to Run1 data
    configdict["YearOfDataTaking"] = {"2011", "2012"}

    # additional Run1 trigger requirements
    HLT_L0 = "(lab0_L0HadronDecision_TOS == 1 || lab0_L0Global_TIS ==1 )"
    HLT_1 = "(lab0_Hlt1TrackAllL0Decision_TOS ==1) "
    HLT_2 = "(lab0_Hlt2Topo2BodyBBDTDecision_TOS ==1 || lab0_Hlt2Topo3BodyBBDTDecision_TOS == 1 || lab0_Hlt2Topo4BodyBBDTDecision_TOS ==1)"
    app = "&&"

    configdict["AdditionalCuts"]["All"][
        "MC"] = HLT_L0 + app + HLT_1 + app + HLT_2

    ### Weighting MC samples ###
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0_All;"
            },
        },
        "PIDBachMisID": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0_All;;"
            },
        },
        "PIDChild1Eff": {
            "2011": {
                "FileLabel": "#PIDK Proton 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_MC12TuneV2_ProbNNp>0.6_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Proton 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_MC12TuneV2_ProbNNp>0.6_All;"
            },
        },
        "PIDChild2Eff": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab4_ETA", "lab4_P"],
                "HistName": "K_DLLK>0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab4_ETA", "lab4_P"],
                "HistName": "K_DLLK>0.0_All;"
            },
        },
        "PIDChild3Eff": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab5_ETA", "lab5_P"],
                "HistName": "Pi_DLLK<5.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab5_ETA", "lab5_P"],
                "HistName": "Pi_DLLK<5.0_All;"
            },
        },
        "PIDChildKaonProton": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "K_MC12TuneV2_ProbNNp>0.6_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "K_MC12TuneV2_ProbNNp>0.6_All;"
            },
        },
        "PIDChildPionProton": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_MC12TuneV2_ProbNNp>0.6_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_MC12TuneV2_ProbNNp>0.6_All;"
            },
        },
        "RatioDataMC": {
            "2011": {
                "FileLabel": "#DataMC 2011",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2012": {
                "FileLabel": "#DataMC 2012",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            }
        },
    }

    return configdict
