###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2LcPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    # a first order TOS fraction
    TISTOS_frac = 0.563

    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {
        "Run2": {
            "pKPi": 121.1 * TISTOS_frac
        },
        "Fixed": True
    }
    configdict["Yields"]["Bs2DsPi"] = {
        "Run2": {
            "pKPi": 941.0 * TISTOS_frac
        },
        "Fixed": True
    }
    configdict["Yields"]["Lb2LcK"] = {
        "Run2": {
            "pKPi": [2300.0 * TISTOS_frac, 71.0 * TISTOS_frac]
        },
        "Fixed": False,
        "Constrained": True
    }
    configdict["Yields"]["Lb2LcRho"] = {
        "2015": {
            "pKPi": 8000.0 * TISTOS_frac
        },
        "2016": {
            "pKPi": 8000.0 * TISTOS_frac
        },
        "2017": {
            "pKPi": 8000.0 * TISTOS_frac
        },
        "2018": {
            "pKPi": 8000.0 * TISTOS_frac
        },
        "Fixed": False
    }
    configdict["Yields"]["Lb2ScPi"] = {
        "2015": {
            "pKPi": 20000.0 * TISTOS_frac
        },
        "2016": {
            "pKPi": 20000.0 * TISTOS_frac
        },
        "2017": {
            "pKPi": 20000.0 * TISTOS_frac
        },
        "2018": {
            "pKPi": 20000.0 * TISTOS_frac
        },
        "Fixed": False
    }
    configdict["Yields"]["Signal"] = {
        "2015": {
            "pKPi": 1.1e5 * TISTOS_frac
        },
        "2016": {
            "pKPi": 1.1e5 * TISTOS_frac
        },
        "2017": {
            "pKPi": 1.1e5 * TISTOS_frac
        },
        "2018": {
            "pKPi": 1.1e5 * TISTOS_frac
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2015": {
            "pKPi": 28125.0 * TISTOS_frac
        },
        "2016": {
            "pKPi": 28125.0 * TISTOS_frac
        },
        "2017": {
            "pKPi": 28125.0 * TISTOS_frac
        },
        "2018": {
            "pKPi": 28125.0 * TISTOS_frac
        },
        "Fixed": False
    }

    return configdict
