###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Lb2LcPi"
    configdict["CharmModes"] = {"pKPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi", "Bs2DsPi", "Lb2LcK", "Lb2LcRho", "Lb2ScPi"
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2011", "2012", "2015", "2016", "2017", "2018"
    }

    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {
            "Down": 0.5600,
            "Up": 0.4200
        },
        "2012": {
            "Down": 0.9912,
            "Up": 0.9988
        },
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        },
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2LcPi/config_Lb2LcPi.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotLb2LcPi",
        "Extension": "pdf"
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5400, 6200],  #6200->6600, 5400->5325
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [2266, 2306],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3"
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR"
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR"
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2"
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2"
    }
    configdict["AdditionalVariables"]["lab0_L0HadronDecision_TOS"] = {
        "Range": [-1.0, 2.0],
        "InputName": "lab0_L0HadronDecision_TOS"
    }
    configdict["AdditionalVariables"]["lab0_L0Global_TIS"] = {
        "Range": [-1.0, 2.0],
        "InputName": "lab0_L0Global_TIS"
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data":
        "lab3_ProbNNp>0.6 && lab4_PIDK>0 && lab5_PIDK<5 && lab1_PIDK<0",
        "MC": " ",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": False
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: pKPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"][
        "type"] = "IpatiaPlusJohnsonSU"  #IpatiaPlusJohnsonSUWithWidthRatio
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "pkpi": 5619.36218
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {
            "pkpi": 25.57416
        },
        "Fixed": False  # True
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {
            "pkpi": 13.92415
        },
        "Fixed": False  # True
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {
            "pkpi": 0.84606
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {
            "pkpi": 2.23937
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {
            "pkpi": 1.50506
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {
            "pkpi": 2.00046
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {
            "pkpi": -1.7
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {
            "pkpi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {
            "pkpi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {
            "pkpi": -0.34167
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {
            "pkpi": 0.31789
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {
            "pkpi": 0.25
        },
        "Fixed": True
    }
    # configdict["SignalShape"]["BeautyMass"]["R"] = {
    #     "Run2": {
    #         "pkpi": 1.0
    #     },
    #     "Fixed": False
    # }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    # configdict["CombBkgShape"] = {}
    # configdict["CombBkgShape"]["BeautyMass"] = {}
    # configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    # configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
    #     "Run2": {
    #         "All": -5.0279e-03
    #     },
    #     "Fixed": False
    # }
    # configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
    #     "Run2": {
    #         "All": 0.0 #-5.0279e-04
    #     },
    #     "Fixed": True
    # }
    # configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
    #     "Run2": {
    #         "All": 6.2858e-01 #8.9591e-01
    #     },
    #     "Fixed": True#False
    # }

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "Run1": {
            "All": -3.0e-03
        },
        "Run2": {
            "All": -4.4672e-3
        },
        "Fixed": False
    }

    #   ------------------------------------   #
    #   -----    Lb2LcRho background   -----   #
    #   ------------------------------------   #

    configdict["Lb2LcRhoShape"] = {}
    configdict["Lb2LcRhoShape"]["BeautyMass"] = {}
    configdict["Lb2LcRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Lb2LcRhoShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 5.3962
        },
        "Fixed": True
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "All": 4.1087e+03
        },
        "Fixed": True
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["b"] = {
        "Run2": {
            "All": 5.4781e+03
        },
        "Fixed": True
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["csi"] = {
        "Run2": {
            "All": 5.9207
        },
        "Fixed": True
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 9.6556e-01
        },
        "Fixed": True
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 1.6685e+01
        },
        "Fixed": True
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----    Lb2ScPi background    -----   #
    #   ------------------------------------   #

    configdict["Lb2ScPiShape"] = {}
    configdict["Lb2ScPiShape"]["BeautyMass"] = {}
    configdict["Lb2ScPiShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Lb2ScPiShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 3.7152
        },
        "Fixed": True
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "All": 4.2100e+03
        },
        "Fixed": True
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["b"] = {
        "Run2": {
            "All": 5.4804e+03
        },
        "Fixed": True
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["csi"] = {
        "Run2": {
            "All": 4.9332
        },
        "Fixed": True
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 9.5526e-01
        },
        "Fixed": True
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 1.3919e+01
        },
        "Fixed": True
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {
        "Run1": {
            "pKPi": 89.0
        },
        "Run2": {
            "pKPi": 121.1
        },
        "Fixed": True
    }
    configdict["Yields"]["Bs2DsPi"] = {
        "Run1": {
            "pKPi": 28.21
        },
        "Run2": {
            "pKPi": 941.0
        },
        "Fixed": True
    }
    configdict["Yields"]["Lb2LcK"] = {
        "Run1": {
            "pKPi": [411.0, 13, .0]
        },
        "Run2": {
            "pKPi": [2300.0, 71.0]
        },
        "Fixed": False,
        "Constrained": True
    }
    configdict["Yields"]["Lb2LcRho"] = {
        "2011": {
            "pKPi": 1000.0
        },
        "2012": {
            "pKPi": 1000.0
        },
        "2015": {
            "pKPi": 8000.0
        },
        "2016": {
            "pKPi": 8000.0
        },
        "2017": {
            "pKPi": 8000.0
        },
        "2018": {
            "pKPi": 8000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Lb2ScPi"] = {
        "2011": {
            "pKPi": 12000.0
        },
        "2012": {
            "pKPi": 12000.0
        },
        "2015": {
            "pKPi": 20000.0
        },
        "2016": {
            "pKPi": 20000.0
        },
        "2017": {
            "pKPi": 20000.0
        },
        "2018": {
            "pKPi": 20000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Signal"] = {
        "2011": {
            "pKPi": 5e4
        },
        "2012": {
            "pKPi": 5e4
        },
        "2015": {
            "pKPi": 1.1e5
        },
        "2016": {
            "pKPi": 1.1e5
        },
        "2017": {
            "pKPi": 1.1e5
        },
        "2018": {
            "pKPi": 1.1e5
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {
            "pKPi": 20000.0
        },
        "2012": {
            "pKPi": 20000.0
        },
        "2015": {
            "pKPi": 28125.0
        },
        "2016": {
            "pKPi": 28125.0
        },
        "2017": {
            "pKPi": 28125.0
        },
        "2018": {
            "pKPi": 28125.0
        },
        "Fixed": False
    }

    #----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    #----------------------------------------------------------------------#

    from ROOT import (kRed, kBlue, kOrange, kMagenta, kYellow)

    configdict["PlotSettings"] = {}

    configdict["PlotSettings"]["components"] = [
        "Sig", "CombBkg", "Bd2DPi", "Bs2DsPi", "Lb2LcK", "Lb2LcRho", "Lb2ScPi"
    ]
    configdict["PlotSettings"]["colors"] = [
        "#006837", "#cccccc", "#fbb4b9", "#d7301f", "#31a354", "#78c679",
        "#c2e699"
    ]
    # configdict["PlotSettings"]["components"] = [
    #     "Lb2LcRho", "Lb2ScPi"
    # ]
    # configdict["PlotSettings"]["colors"] = [
    #     "#78c679", "#c2e699","#78c679", "#c2e699","#78c679", "#c2e699"
    # ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict
