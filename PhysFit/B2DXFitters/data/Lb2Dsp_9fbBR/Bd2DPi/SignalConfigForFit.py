def getconfig() :

    from Bd2DPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5100,    5450    ], "InputName" : "lab0_MassFitConsD_M"}

    return configdict
