###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Lb2Dsp"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = [
        "Bs2DsPi", "Bs2DsstPi", "Bs2DsRho", "Bs2DsstRho", "Bd2DsK", "Bs2DsK",
        "Bs2DsstK", "Bs2DsKst", "Bs2DsstKst", "Bd2DsstK", "Lb2Dsstp"
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2011", "2012", "2015", "2016", "2017", "2018"
    }

    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {
            "Down": 0.5600,
            "Up": 0.4200
        },
        "2012": {
            "Down": 0.9912,
            "Up": 0.9988
        },
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        },
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2Dsp/config_Lb2Dsp.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotLb2Dsp",
        "Extension": "pdf"
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5200, 6200],
        "InputName": "lab0_MassHypo_Dsp"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1948, 1988],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 150.0],
        "InputName": "lab1_ProbNNp"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 150000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [1000.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 500.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3"
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR"
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR"
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2"
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2"
    }
    configdict["AdditionalVariables"]["lab0_L0HadronDecision_TOS"] = {
        "Range": [-1.0, 2.0],
        "InputName": "lab0_L0HadronDecision_TOS"
    }
    configdict["AdditionalVariables"]["lab0_L0Global_TIS"] = {
        "Range": [-1.0, 2.0],
        "InputName": "lab0_L0Global_TIS"
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "",
        "MC": "",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": True
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5620.78865
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {
            "All": 20.70233
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {
            "All": 13.82495
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {
            "All": 1.04495
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {
            "All": 1.5666
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {
            "All": 1.5891
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {
            "All": 2.16415
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {
            "All": -1.7
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {
            "All": -0.17526
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {
            "All": 0.34947
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {
            "All": 0.25
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    single = False
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    if single:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
            "Run1": {
                "All": -1.4315e-03
            },
            "Run2": {
                "All": -9.0296e-04
            },
            "Fixed": False
        }
    else:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "MassDiff"
        configdict["CombBkgShape"]["BeautyMass"]["c"] = {
            "Run1": {
                "All": -3.5211e-04
            },
            "Run2": {
                "All": 1.2225e+02
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["m0"] = {
            "Run1": {
                "All": -3.5211e-02
            },
            "Run2": {
                "All": 5.1665e+03
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["a"] = {
            "Run1": {
                "All": 4.3067e-01
            },
            "Run2": {
                "All": -10.0
            },
            "Fixed": False
        }
        configdict["CombBkgShape"]["BeautyMass"]["b"] = {
            "Run1": {
                "All": 4.3067e-01
            },
            "Run2": {
                "All": 0.0
            },
            "Fixed": True
        }

    #   ------------------------------------   #
    #   -----   Lb2Dsstp background    -----   #
    #   ------------------------------------   #

    configdict["Lb2DsstpShape"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Lb2DsstpShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 4.95615
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahill"] = {
        "Run2": {
            "All": 5248.47329
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {
            "All": 5253.23423
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhill"] = {
        "Run2": {
            "All": 5532.30986
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {
            "All": 5562.55112
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihill"] = {
        "Run2": {
            "All": 0.32533
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihorns"] = {
        "Run2": {
            "All": 0.59192
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 0.993
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frachorns"] = {
        "Run2": {
            "All": 0.31183
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 38.23656
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigmahorns"] = {
        "Run2": {
            "All": 13.53707
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Bs2DsstK background    -----   #
    #   ------------------------------------   #

    configdict["Bs2DsstKShape"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bs2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5354.07669
        },
        "Fixed": True
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 124.82455
        },
        "Fixed": True
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["alpha"] = {
        "Run2": {
            "All": -0.79732
        },
        "Fixed": True
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["n"] = {
        "Run2": {
            "All": 49.76984
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Bd2DsstK background    -----   #
    #   ------------------------------------   #

    configdict["Bd2DsstKShape"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bd2DsstKShape"]["BeautyMass"]["alpha"] = configdict[
        "Bs2DsstKShape"]["BeautyMass"]["alpha"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["n"] = configdict[
        "Bs2DsstKShape"]["BeautyMass"]["n"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["sigma"] = configdict[
        "Bs2DsstKShape"]["BeautyMass"]["sigma"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstKShape"]["BeautyMass"]["mean"]["Run2"]["All"] -
            87.24
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   ----   Additional parameters   -----   #
    #   ------------------------------------   #

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g2_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g2_f2_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g3_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 1.0,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}

    configdict["Yields"]["Bs2DsPi"] = {
        "Run2": {
            "KKPi": [241.57, 11.0 * 2.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstPi"] = {
        "Run2": {
            "KKPi": [199.393, 9.0 * 2.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsRho"] = {
        "Run2": {
            "KKPi": [139.127, 7.0 * 2.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstRho"] = {
        "Run2": {
            "KKPi": [34.9765, 2.0 * 2.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsK"] = {
        "Run2": {
            "KKPi": [104.553, 3.0 * 2.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bd2DsK"] = {
        "Run2": {
            "KKPi": [40.9951, 2.0 * 2.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstK"] = {
        "Run2": {
            "KKPi": [115.2, 8.0 * 2.0]
        },
        "Fixed": False,
        "Constrained": True,
    }

    configdict["Yields"]["Bd2DsstK"] = {
        "Run2": {
            "KKPi": [41.0068, 4.0 * 2.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsKst"] = {
        "Run2": {
            "KKPi": [16.3455, 4.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstKst"] = {
        "Run2": {
            "KKPi": [18.1501, 4.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Lb2Dsstp"] = {
        "2011": {
            "KKPi": 500.0
        },
        "2012": {
            "KKPi": 500.0
        },
        "2015": {
            "KKPi": 500.0
        },
        "2016": {
            "KKPi": 500.0
        },
        "2017": {
            "KKPi": 500.0
        },
        "2018": {
            "KKPi": 500.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Signal"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {
            "KKPi": 5000.0
        },
        "2012": {
            "KKPi": 5000.0
        },
        "2015": {
            "KKPi": 5000.0
        },
        "2016": {
            "KKPi": 5000.0
        },
        "2017": {
            "KKPi": 5000.0
        },
        "2018": {
            "KKPi": 5000.0
        },
        "Fixed": False
    }

    #----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    #----------------------------------------------------------------------#

    from ROOT import (kRed, kBlue, kOrange, kMagenta, kYellow, kSolid)

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig", "CombBkg", "Bs2DsPi", "Bs2DsstPi", "Bs2DsRho", "Bs2DsstRho",
        "Bs2DsK", "Bs2DsstK", "Bs2DsKst", "Bs2DsstKst", "Bd2DsK", "Bd2DsstK",
        "Lb2Dsstp"
    ]
    configdict["PlotSettings"]["colors"] = [
        "#54278f",
        "#cccccc",
        "#d7301f",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
        "#08519c",
        "#6baed6",
        "#c6dbef",
        "#eff3ff",
        "#3182bd",
        "#9ecae1",
        "#9e9ac8",
    ]

    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3004,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        3005,
        3005,
        kSolid,
    ]

    configdict["PlotSettings"]["patterncolor"] = [
        "#54278f", "#969696", "#d7301f", "#fc8d59", "#fdcc8a", "#fef0d9",
        "#08519c", "#6baed6", "#bdd7e7", "#eff3ff", "#eff3ff", "#eff3ff",
        "#9e9ac8"
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.35, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict
