###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2DspConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5620.78939
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {
            "All": 20.64271
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {
            "All": 13.87385
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {
            "All": 1.059
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {
            "All": 1.57623
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {
            "All": 1.58456
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {
            "All": 2.16377
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {
            "All": -1.7
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {
            "All": -0.16346
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {
            "All": 0.3543
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {
            "All": 0.25
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Lb2Dsstp background    -----   #
    #   ------------------------------------   #

    configdict["Lb2DsstpShape"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Lb2DsstpShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 4.94871
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahill"] = {
        "Run2": {
            "All": 5248.51723
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {
            "All": 5253.26593
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhill"] = {
        "Run2": {
            "All": 5531.91092
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {
            "All": 5562.52577
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihill"] = {
        "Run2": {
            "All": 0.32351
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihorns"] = {
        "Run2": {
            "All": 0.59197
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 0.99328
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frachorns"] = {
        "Run2": {
            "All": 0.3123
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 38.52472
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigmahorns"] = {
        "Run2": {
            "All": 13.55752
        },
        "Fixed": True
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Bs2DsstK background    -----   #
    #   ------------------------------------   #

    configdict["Bs2DsstKShape"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bs2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5352.02451
        },
        "Fixed": True
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 119.57015
        },
        "Fixed": True
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["alpha"] = {
        "Run2": {
            "All": -0.78056
        },
        "Fixed": True
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["n"] = {
        "Run2": {
            "All": 49.99908
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Bd2DsstK background    -----   #
    #   ------------------------------------   #

    configdict["Bd2DsstKShape"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bd2DsstKShape"]["BeautyMass"]["alpha"] = configdict[
        "Bs2DsstKShape"]["BeautyMass"]["alpha"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["n"] = configdict[
        "Bs2DsstKShape"]["BeautyMass"]["n"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["sigma"] = configdict[
        "Bs2DsstKShape"]["BeautyMass"]["sigma"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstKShape"]["BeautyMass"]["mean"]["Run2"]["All"] -
            87.24
        },
        "Fixed": True
    }

    return configdict
