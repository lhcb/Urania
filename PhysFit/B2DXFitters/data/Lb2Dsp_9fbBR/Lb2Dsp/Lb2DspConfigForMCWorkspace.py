###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2DspConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    ### Weighting MC samples ###
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2011": {
                "FileLabel": "#PIDK Proton 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_MC12TuneV2_ProbNNp>0.7&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Proton 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_MC12TuneV2_ProbNNp>0.7&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Proton 2015",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "P_TotLc_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
        },
        "PIDBachPion": {
            "2011": {
                "FileLabel": "#PIDK Pion 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_MC12TuneV2_ProbNNp>0.7&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Pion 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_MC12TuneV2_ProbNNp>0.7&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
        },
        "PIDBachKaon": {
            "2011": {
                "FileLabel": "#PIDK Kaon 2011",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_MC12TuneV2_ProbNNp>0.7&&IsMuon==0.0_All;"
            },
            "2012": {
                "FileLabel": "#PIDK Kaon 2012",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_MC12TuneV2_ProbNNp>0.7&&IsMuon==0.0_All;"
            },
            "2015": {
                "FileLabel": "#PIDK Kaon 2015",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.9&&IsMuon==0.0_All;"
            },
        },
        "RatioDataMC": {
            "2011": {
                "FileLabel": "#DataMC 2011",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2012": {
                "FileLabel": "#DataMC 2012",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2015": {
                "FileLabel": "#DataMC 2015",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2016": {
                "FileLabel": "#DataMC 2016",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2017": {
                "FileLabel": "#DataMC 2017",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2018": {
                "FileLabel": "#DataMC 2018",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
        },
        "Shift": {
            "2011": {
                "BeautyMass": -2.0,
                "CharmMass": 0.0
            },
            "2012": {
                "BeautyMass": -2.0,
                "CharmMass": 0.0
            },
            "2015": {
                "BeautyMass": -1.0,
                "CharmMass": 0.0
            },
            "2016": {
                "BeautyMass": -1.1,
                "CharmMass": 0.0
            },
            "2017": {
                "BeautyMass": -0.95,
                "CharmMass": 0.0
            },
            "2018": {
                "BeautyMass": -1.65,
                "CharmMass": 0.0
            },
        }
    }

    return configdict
