###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2DspConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    # Take from nominal, and overwrite the contrained ones, making them fixed

    configdict["Yields"]["Bs2DsPi"] = {
        "Run2": {
            "KKPi": 241.57
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bs2DsstPi"] = {
        "Run2": {
            "KKPi": 199.393
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bs2DsRho"] = {
        "Run2": {
            "KKPi": 139.127
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bs2DsstRho"] = {
        "Run2": {
            "KKPi": 34.9765
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bs2DsK"] = {
        "Run2": {
            "KKPi": 104.553
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bd2DsK"] = {
        "Run2": {
            "KKPi": 40.9951
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bs2DsstK"] = {
        "Run2": {
            "KKPi": 115.2
        },
        "Fixed": True,
    }

    configdict["Yields"]["Bd2DsstK"] = {
        "Run2": {
            "KKPi": 41.0068
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bs2DsKst"] = {
        "Run2": {
            "KKPi": 16.3455
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bs2DsstKst"] = {
        "Run2": {
            "KKPi": 18.1501
        },
        "Fixed": True,
    }

    return configdict
