###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2DspConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"][
        "type"] = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5620.71192
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigma1"] = {
        "Run2": {
            "All": 16.35979
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["sigma2"] = {
        "Run2": {
            "All": 11.3908
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["alpha1"] = {
        "Run2": {
            "All": 1.81423
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["alpha2"] = {
        "Run2": {
            "All": -2.08439
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {
            "All": 1.62383
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {
            "All": 1.88089
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 0.5
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 1.0
        },
        "Fixed": False
    }

    return configdict
