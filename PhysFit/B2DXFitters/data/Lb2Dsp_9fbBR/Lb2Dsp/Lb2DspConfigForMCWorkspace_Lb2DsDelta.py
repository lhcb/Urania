###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Lb2DspConfigForMCWorkspaceRun2 import getconfig as getconfig_MC
    configdict = getconfig_MC()

    configdict["YearOfDataTaking"] = {"2015", "2016"}

    configdict["Backgrounds"] = ["Lb2DsDelta"]

    configdict[
        "dataName"] = "../data/Lb2Dsp_9fbBR/Lb2Dsp/config_Lb2DsDelta.txt"

    return configdict
