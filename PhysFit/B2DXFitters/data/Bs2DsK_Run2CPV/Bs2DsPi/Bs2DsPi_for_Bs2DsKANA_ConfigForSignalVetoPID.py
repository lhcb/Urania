###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["YearOfDataTaking"] = {"2015", "2016", "2017", "2018"}
    configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK", "PiPiPi"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1"
    }

    configdict[
        "dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi_SignalVetoPID.txt"

    configdict["AdditionalVariables"] = {}
    app = "&&"
    isMuon = "lab1_isMuon==0&&lab3_isMuon==0&&lab4_isMuon==0&&lab5_isMuon==0"
    p = "lab1_P > 3000.00&&lab1_P < 650000.00"
    time = "(lab0_LifetimeFit_ctau[0] > 0.119917)&&(lab0_LifetimeFit_ctau[0] < 4.496887)"
    bdtg = "(BDTGResponse_XGB_1 > 0.475000)&&(BDTGResponse_XGB_1 < 1.000000)"
    hasRich = "lab1_hasRich==1&&lab3_hasRich==1&&lab4_hasRich==1&&lab5_hasRich==1"
    vetoD = "(fabs(lab2_MassHypo_KKPi_D - 1869.6) > 30. || lab3_PIDK_corr > 10.)"
    vetoLc = "(fabs(lab2_MassHypo_KKPi_Lambda - 2286.4) > 30. || (lab3_PIDK_corr - lab3_PIDp_corr > 5.))"
    vetoD0 = "lab34_MM < 1800 && abs(lab13_MM - 1864.83) > 30"
    #veto = vetoD+app+vetoLc+app+vetoD0
    pid_phipi = "lab3_PIDK_corr > -2. && lab4_PIDK_corr > -2."
    pid_kstk = "lab3_PIDK_corr >  5. && lab4_PIDK_corr > -2."
    pid_nonres = "lab3_PIDK_corr >  5. && lab4_PIDK_corr >  5. && lab5_PIDK_corr < 10."

    pide = "lab1_PIDe < 5 && lab5_PIDe < 5"
    pidp = "lab5_PIDp_corr < 10"
    pid_bac = "lab1_PIDK_corr<0"
    pide_pipipi = "lab1_PIDe < 5 && lab5_PIDe < 5 && lab3_PIDe<5"
    pid_pipipi = "lab3_PIDK_corr<2.0 &&lab4_PIDK_corr < 2. && lab5_PIDK_corr < 2.&& lab3_PIDp_corr < 5. && lab4_PIDp_corr < 5. && lab5_PIDp_corr < 5."
    lab13D0 = "fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))+sqrt(pow(lab3_M,2)+pow(lab3_P,2)),2)-pow(lab1_PX+lab3_PX,2)-pow(lab1_PY+lab3_PY,2)-pow(lab1_PZ+lab3_PZ,2))-1864)>30.0"
    lab15D0 = "fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))+sqrt(pow(lab5_M,2)+pow(lab5_P,2)),2)-pow(lab1_PX+lab5_PX,2)-pow(lab1_PY+lab5_PY,2)-pow(lab1_PZ+lab5_PZ,2))-1864)>30.0"
    veto_pipipi = "(lab34_MM < 1700. && lab45_MM < 1700.)"

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data":
        isMuon + app + hasRich + app + time + app + p + bdtg,
        "MC":
        "lab1_M<200&&lab1_PIDK !=-1000.0&&" + isMuon + app + hasRich + app +
        time + app + p + app + bdtg + app + pide + app + pid_bac,
        "MCID":
        True,
        "MCTRUEID":
        True,
        "BKGCAT":
        True,
        "DsHypo":
        True
    }
    configdict["AdditionalCuts"]["KKPi"] = {
        "MC": pidp + app + vetoD + app + vetoLc + app + vetoD0
    }
    configdict["AdditionalCuts"]["PhiPi"] = {"MC": pid_phipi}
    configdict["AdditionalCuts"]["NonRes"] = {"MC": pid_nonres}
    configdict["AdditionalCuts"]["KstK"] = {"MC": pid_kstk}
    configdict["AdditionalCuts"]["PiPiPi"] = {
        "MC":
        lab13D0 + app + lab15D0 + app + pid_pipipi + app + pide_pipipi + app +
        veto_pipipi
    }

    configdict["GlobalWeight"] = {
        "2015": {
            "NonRes": {
                "Down": 0.106642,
                "Up": 0.080459
            },
            "PhiPi": {
                "Down": 0.232224,
                "Up": 0.175209
            },
            "KstK": {
                "Down": 0.167985,
                "Up": 0.126742
            },
            "PiPiPi": {
                "Down": 0.108659,
                "Up": 0.081981
            }
        },
        "2016": {
            "NonRes": {
                "Down": 0.490548,
                "Up": 0.459220
            },
            "PhiPi": {
                "Down": 1.068220,
                "Up": 1.000000
            },
            "KstK": {
                "Down": 0.771615,
                "Up": 0.723375
            },
            "PiPiPi": {
                "Down": 0.499827,
                "Up": 0.467906
            }
        },
        "2017": {
            "NonRes": {
                "Down": 0.482980,
                "Up": 0.458867
            },
            "PhiPi": {
                "Down": 1.052550,
                "Up": 1.000000
            },
            "KstK": {
                "Down": 0.760970,
                "Up": 0.722978
            },
            "PiPiPi": {
                "Down": 0.488805,
                "Up": 0.464401
            }
        },
        "2018": {
            "NonRes": {
                "Down": 0.417872,
                "Up": 0.454970
            },
            "PhiPi": {
                "Down": 0.918461,
                "Up": 1.000000
            },
            "KstK": {
                "Down": 0.672380,
                "Up": 0.732072
            },
            "PiPiPi": {
                "Down": 0.420699,
                "Up": 0.459048
            }
        }
    }

    #weighting templates by PID eff/misID
    configdict[
        "WeightingMassTemplates"] = {  # "PIDBachEff":      { "2015":{"FileLabel":"#PIDK Pion 2015",   "Var":["nTracks","lab1_P"],
            #                              "HistName":"Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"},
            #                      "2016":{"FileLabel":"#PIDK Pion 2016",   "Var":["nTracks","lab1_P"],
            #                              "HistName":"Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"},
            #                      "2017":{"FileLabel":"#PIDK Pion 2017",   "Var":["nTracks","lab1_P"],
            #                              "HistName":"Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"},
            #                      "2018":{"FileLabel":"#PIDK Pion 2018",   "Var":["nTracks","lab1_P"],
            #                              "HistName":"Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"},
            #                  },
            "RatioDataMC": {
                "2015": {
                    "FileLabel": "#DataMC 2015",
                    "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                    "HistName": "histRatio"
                },
                "2016": {
                    "FileLabel": "#DataMC 2016",
                    "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                    "HistName": "histRatio"
                },
                "2017": {
                    "FileLabel": "#DataMC 2017",
                    "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                    "HistName": "histRatio"
                },
                "2018": {
                    "FileLabel": "#DataMC 2018",
                    "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                    "HistName": "histRatio"
                },
            },
            "Shift": {
                "2011": {
                    "BeautyMass": 0.0,
                    "CharmMass": 0.0
                },
                "2012": {
                    "BeautyMass": 0.0,
                    "CharmMass": 0.0
                },
                "2015": {
                    "BeautyMass": 0.0,
                    "CharmMass": 0.0
                },
                "2016": {
                    "BeautyMass": 0.0,
                    "CharmMass": 0.0
                },
                "2017": {
                    "BeautyMass": 0.0,
                    "CharmMass": 0.0
                },
                "2018": {
                    "BeautyMass": 0.0,
                    "CharmMass": 0.0
                },
            }
        }

    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Bs2DsPi_",
        "Bd2DK": "dataSetMC_Bd2DK_",
        "Bd2DRho": "dataSetMC_Bd2DRho_",
        "Bd2DstPi": "dataSetMC_Bd2DstPi_",
        "Bd2DKst": "dataSetMC_Bd2DKst_",
        "Bs2DsPi": "dataSetMC_Bs2DsPi_",
        "Comb": "dataSetCombPi_"
    }
    #Axes titles
    configdict["AxisTitle"] = {
        "BeautyMass": {
            "Bs2DsPi": "m(D_{s}^{-}#pi^{+}) [MeV/c^{2}]",
            "Bd2DK": "DK mass (MeV/c^{2})"
        },
        #"CharmMass":{"Bs2DsPi":"m(#pi^{-}#pi^{+}#pi^{\pm}) [MeV/c^{2}]",
        "CharmMass": {
            "Bs2DsPi": "m(K^{-}K^{+}#pi^{\pm}) [MeV/c^{2}]",
        }
    }

    configdict["pdfList"] = {}
    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["BeautyMass"][
        "IpatiaPlusJohnsonSU"] = {
            "Title": "B_{d}#rightarrowD#pi",
            "Bins": 200,
            "Min": 5300.0,
            "Max": 5550.0,
            "Unit": "MeV/c^{2}",
            "mean": [5367.51, 5300, 5500],
            "sigmaI": [30.0, 1.0, 50.0],
            "sigmaJ": [15.0, 1.0, 50.0],
            "zeta": [0.0],  #1,0.0,5.0],
            "fb": [0.0],
            "l": [-2.5, -7.0, 0.0],
            "a1": [0.40, 0.01, 7.0],
            "a2": [3.24, 0.01, 7.0],
            "n1": [4.93, 0.01, 50.0],
            "n2": [1.96, 0.01, 50.0],
            "nu": [-0.3, -5.0, 5.0],
            "tau": [0.422, -5.0, 5.0],
            "fracI": [0.5, 0.001, 0.999]
        }

    configdict["pdfList"]["Signal"]["Bs2DsPi"]["BeautyMass"][
        "DoubleCrystalBall"] = {
            "Title": "B_{d}#rightarrowD#pi",
            "Bins": 200,
            "Min": 5300.0,
            "Max": 5550.0,
            "Unit": "MeV/c^{2}",
            "mean": [5367.51, 5300, 5400],
            "sameMean": True,
            "sigma1": [8.0, 1.0, 30.0],
            "sigma2": [15.0, 1.0, 30.0],
            "alpha1": [-2.0, -3.00, -0.01],
            "alpha2": [2.0, 0.01, 3.0],
            "n1": [20.0, 0.01, 50.0],
            "n2": [3.0, 0.01, 50.0],
            "frac": [0.26, 0.001, 0.999]
        }
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["CharmMass"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["CharmMass"][
        "IpatiaPlusJohnsonSU"] = {
            "Title": "m(hhh)",
            "Bins": 200,
            "Min": 1930.0,
            "Max": 2015.0,
            "Unit": "MeV/c^{2}",
            "mean": [1968.49, 1950, 1990],
            "sigmaI": [15.0, 1.0, 30.0],
            "sigmaJ": [15.0, 1.0, 30.0],
            "zeta": [0.0],
            "fb": [0.0],
            "l": [-1.5, -5.0, 0.0],
            "a1": [3.0, 0.01, 5.0],
            "a2": [0.5, 0.001, 4.0],
            "n1": [5.8182, 0.01, 8.0],
            "n2": [10.3802, 0.01, 50.0],
            "nu": [1.0, -5.0, 5.0],
            "tau": [0.5, -5.0, 5.0],
            "fracI": [0.26, 0.001, 0.999]
            #   "sigma"      : [15.0, 1.0, 30.0],
            #   "zeta"       : [0.0],
            #   "fb"         : [0.0],
            #   "l"          : [-2.7748,-5.0,0.0],
            #   "a1"         : [1.9876,0.01,3.0],
            #   "a2"         : [2.4416,0.01,3.0],
            #   "n1"         : [2.5849,0.01,6.0],
            #   "n2"         : [3.6042,0.01,6.0],
        }

    configdict["pdfList"]["Signal"]["Bs2DsPi"]["CharmMass"]["Ipatia"] = {
        "Title": "m(hhh)",
        "Bins": 200,
        "Min": 1930.0,
        "Max": 2015.0,
        "Unit": "MeV/c^{2}",
        "mean": [1968.49, 1950, 1990],
        "sigma": [10.0, 1.0, 30.0],
        "zeta": [0.0],
        "fb": [0.0],
        "l": [-3.5, -5.0, 0.0],
        "a1": [2.0, 0.01, 5.0],
        "a2": [2.0, 0.01, 5.0],
        "n1": [5.5, 0.01, 15.0],
        "n2": [5.5, 0.01, 15.0],
    }

    configdict["pdfList"]["Signal"]["Bs2DsPi"]["CharmMass"][
        "IpatiaPlusGaussian"] = {
            "Title": "m(hhh)",
            "Bins": 200,
            "Min": 1930.0,
            "Max": 2015.0,
            "Unit": "MeV/c^{2}",
            "mean": [1968.49, 1950, 1990],
            "sigmaI": [8.0, 1.0, 30.0],
            "zeta": [0.0],
            "fb": [0.0],
            "l": [-1.5, -10.0, 0.0],  # phipi [-2.5,-8.0,0.0],
            "a1": [2.0, 0.01, 5.0],
            "a2": [2.5, 0.01, 5.0],
            "n1": [2.0, 0.01, 50.0],
            "n2": [2.0, 0.01, 50.0],
            "sigmaG": [8.0, 1.0, 50.0],
            "fracI": [0.50, 0.001, 0.999]
        }

    configdict["pdfList"]["Signal"]["Bs2DsPi"]["CharmMass"][
        "IpatiaPlus2Gaussian"] = {
            "Title": "m(hhh)",
            "Bins": 200,
            "Min": 1930.0,
            "Max": 2015.0,
            "Unit": "MeV/c^{2}",
            "mean": [1968.49, 1950, 1990],
            "sigmaI": [5.0, 1.0, 30.0],
            "zeta": [0.0],
            "fb": [0.0],
            "l": [-2.5, -10.0, 0.0],
            "a1": [1.5, 0.01, 5.0],
            "a2": [1.5, 0.01, 5.0],
            "n1": [50.0],  #3.5,0.01,50.0],
            "n2": [50.0],  #3.5,0.01,50.0],
            "sigmaG": [10.0, 1.0, 50.0],
            "sigma2G": [12.0, 1.0, 50.0],
            "frac1": [0.60, 0.001, 0.999],
            "frac2": [0.30, 0.001, 0.999],
        }
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["CharmMass"][
        "IpatiaGaussConv"] = {
            "Title": "m(hhh)",
            "Bins": 200,
            "Min": 1930.0,
            "Max": 2015.0,
            "Unit": "MeV/c^{2}",
            "mean": [1968.49, 1950, 1990],
            "sigmaI": [10.0, 1.0, 30.0],
            "zeta": [0.0],
            "fb": [0.0],
            "l": [-4.3, -5.0, 0.0],
            "a1": [0.8, 0.01, 5.0],
            "a2": [1.3, 0.01, 5.0],
            "n1": [5.5, 0.01, 15.0],
            "n2": [5.5, 0.01, 15.0],
            "sigmaG": [0.03],  # 0.001, 0.1], #, 1.0, 30.0],
            "fracI": [0.9, 0.001, 0.999]
        }
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["CharmMass"][
        "DoubleCrystalBall"] = {
            "Title": "B_{d}#rightarrowD#pi",
            "Bins": 200,
            "Min": 1930.0,
            "Max": 2015.0,
            "Unit": "MeV/c^{2}",
            "mean": [1968.49, 1950, 1990],
            "sameMean": True,
            "sigma1": [8.0, 1.0, 30.0],
            "sigma2": [15.0, 1.0, 30.0],
            "alpha1": [-1.0, -8.00, -0.01],
            "alpha2": [1.0, 0.01, 5.0],
            "n1": [3.5, 0.01, 50.0],
            "n2": [3.5, 0.01, 50.0],
            "frac": [0.6, 0.001, 0.999]
        }

    return configdict
