###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import itertools


def getconfig():
    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    from math import pi

    # PHYSICAL PARAMETERS
    configdict["Gammas"] = 0.6600  #0.0020   # in ps^{-1}
    configdict["DeltaGammas"] = 0.085  #
    configdict["DeltaMs"] = 17.757  # in ps^{-1}
    configdict["TagEffSig"] = 0.403
    configdict["TagOmegaSig"] = 0.391
    configdict["StrongPhase"] = 20. / 180. * pi
    configdict["WeakPhase"] = 70. / 180. * pi
    configdict["ModLf"] = 0.372
    configdict["CPlimit"] = {"upper": 4.0, "lower": -4.0}
    configdict["Labels"] = ["20152016", "2017", "2018"]

    configdict["Asymmetries"] = {"Detection": 0.0, "Production": 0.0}

    configdict["ConstrainsForTaggingCalib"] = False

    configdict["SWeightCorrection"] = "Read"

    configdict["Resolution"] = {
        "20152016": {
            "scaleFactor": {
                "p0": 0.008377,
                "p1": 1.002,
                "p2": 0.0
            },
            "meanBias": -0.002254
        },
        "2017": {
            "scaleFactor": {
                "p0": 0.006092,
                "p1": 1.048,
                "p2": 0.0
            },
            "meanBias": -0.003047
        },
        "2018": {
            "scaleFactor": {
                "p0": 0.0055521,
                "p1": 1.052,
                "p2": 0.0
            },
            "meanBias": -0.002394
        }
    }

    configdict["TaggingCalibration"] = {
        "Type": "GLM",
        "OS": {
            "20152016": {
                "xml":
                os.path.abspath(
                    os.path.join(
                        os.path.dirname(__file__),
                        'EspressoCalibrationModel.xml')),
                "tagEff":
                0.4126,
                "aTagEff":
                0.0,
                "use":
                True,
            },
            "2017": {
                "xml":
                os.path.abspath(
                    os.path.join(
                        os.path.dirname(__file__),
                        'EspressoCalibrationModel.xml')),
                "tagEff":
                0.4084,
                "aTagEff":
                0.0,
                "use":
                True,
            },
            "2018": {
                "xml":
                os.path.abspath(
                    os.path.join(
                        os.path.dirname(__file__),
                        'EspressoCalibrationModel.xml')),
                "tagEff":
                0.4123,
                "aTagEff":
                0.0,
                "use":
                True,
            },
        },
        "SS": {
            "20152016": {
                "xml":
                os.path.abspath(
                    os.path.join(
                        os.path.dirname(__file__), 'EspressoSSModel.xml')),
                "tagEff":
                0.6918,
                "aTagEff":
                0.0,
                "use":
                True,
            },
            "2017": {
                "xml":
                os.path.abspath(
                    os.path.join(
                        os.path.dirname(__file__), 'EspressoSSModel.xml')),
                "tagEff":
                0.6992,
                "aTagEff":
                0.0,
                "use":
                True,
            },
            "2018": {
                "xml":
                os.path.abspath(
                    os.path.join(
                        os.path.dirname(__file__), 'EspressoSSModel.xml')),
                "tagEff":
                0.6973,
                "aTagEff":
                0.0,
                "use":
                True,
            },
        },
    }

    configdict["Acceptance"] = {
        "knots": [0.50, 1.0, 1.5, 2.0, 3.0, 12.0],
        "20152016": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
        "2017": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
        "2018": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
    }

    configdict["constParams"] = [
        'Gammas_Bs2DsPi',
        'deltaGammas_Bs2DsPi',
        'C_Bs2DsPi',
        'Cbar_Bs2DsPi',
        'S_Bs2DsPi',
        'Sbar_Bs2DsPi',
        'D_Bs2DsPi',
        'Dbar_Bs2DsPi',
        'tagEff_OS',
        'tagEff_SS',
        'aTagEff_OS',
        'aTagEff_SS',
        'average_OS',
        'average_SS',
        'adet',
    ]
    # [
    #     # dp_0_GLMCalib_SS_20152016
    #     'dp_{}_GLMCalib_{}'.format(i, t) for i, t in itertools.product(
    #         range(3),
    #         ['OS', 'SS'],
    #     )
    # ]

    # Add some name variants
    configdict["constParams"] += [
        c + "_" + l for c, l in itertools.product(configdict["constParams"],
                                                  configdict["Labels"])
    ]
    # float all asymmetries and tagging efficiency
    configdict["constParams"] = [
        c for c in configdict["constParams"]
        if not ("aprod" in c or "atageff" in c.lower())
    ]

    return configdict


if __name__ == '__main__':
    sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'Toys'))
    from configutils import main
    main(getconfig())
