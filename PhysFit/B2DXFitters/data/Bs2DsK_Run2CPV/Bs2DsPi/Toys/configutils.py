###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
import sys
from pprint import pprint


def main(config):
    if len(sys.argv) >= 2:
        for arg in sys.argv[1:]:
            keys = arg.split('.')
            subconfig = config
            for k in keys:
                subconfig = subconfig.get(k)
                if subconfig is None:
                    print('Key "{}" not found'.format(k))
                    break

            print(arg + ':')
            pprint(subconfig)
    else:
        pprint(config)
