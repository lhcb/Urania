###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import
import ROOT
import sys
import os

sys.path.insert(0,
                os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import nominal.MassFit
from . import Generator


def getconfig():
    configdict = nominal.MassFit.getconfig()

    configdict["BasicVariables"]["BeautyTime"]["Range"] = (
        Generator.getconfig()["Observables"]["BeautyTime"]["Range"])

    return configdict


if __name__ == '__main__':
    from configutils import main
    main(getconfig())
