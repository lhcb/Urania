###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import itertools
import os
import sys
from math import pi

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))  # noqa
import nominal.DMSFit


def getconfig():
    configdict = nominal.DMSFit.getconfig()

    configdict['constParams'] += [
        'adet',
        'adet_20152016',
        'adet_2017',
        'adet_2018',
        'aprod',
        'aprod_20152016',
        'aprod_2017',
        'aprod_2018',
    ]

    return configdict


# just print the full config if run directly
if __name__ == '__main__':
    from configutils import main
    main(getconfig())
