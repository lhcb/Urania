###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import

import itertools
import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))  # noqa
import nominal_signalonly.DMSFit
from . import Generator


def getconfig():
    configdict = nominal_signalonly.DMSFit.getconfig()

    for y in configdict['Labels']:
        configdict['Acceptance'][y]['values'] = Generator.getconfig(
        )['ResolutionAcceptance']['Signal']['Acceptance']['KnotCoefficients']

    configdict['constParams'] += [
        'var{}_{}'.format(i, y)
        for i, y in itertools.product(list(range(1, 7)), configdict['Labels'])
    ]

    return configdict


# just print the full config if run directly
if __name__ == '__main__':
    from configutils import main
    main(getconfig())
