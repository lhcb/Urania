###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import

import itertools
import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))  # noqa
import nominal_signalonly.DMSFit
from . import Generator


def getconfig():
    configdict = nominal_signalonly.DMSFit.getconfig()
    generator_config = Generator.getconfig()

    for y in configdict['Labels']:
        for tagger in ['OS', 'SS']:
            generated = generator_config['Taggers']['Signal'][tagger][
                'Calibration']
            configdict['TaggingCalibration'][tagger][y].update(
                p0=generated['p0'][0],
                p1=generated['p1'][0],
                dp0=generated['deltap0'][0],
                dp1=generated['deltap1'][0],
                tagEff=generated['tageff'][0],
                aTagEff=generated['tagasymm'][0],
                average=generated['avgeta'][0],
            )

    configdict['constParams'] += [
        '{}p{}_{}_{}'.format(d, i, t, y) for d, i, t, y in itertools.product(
            ['d', ''], list(range(2)), ['OS', 'SS'], configdict['Labels'])
    ]
    configdict['constParams'] += [
        '{}agEff_{}_{}'.format(d, t, y) for d, t, y in itertools.product(
            ['aT', 't'], ['OS', 'SS'], configdict['Labels'])
    ]

    return configdict


# just print the full config if run directly
if __name__ == '__main__':
    from configutils import main
    main(getconfig())
