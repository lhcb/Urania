###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import copy
import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))  # noqa

WORKSPACEFILE = os.path.join(
    os.environ['WORKDIR'],
    'mdfit/Bs2DsPiConfigForNominalMassFitShapeSplit9Tuned/workspace_Minuit_sim.root'
)
WORKSPACENAME = "FitMeToolWS"
G1_F1_FRAC = 0.5


def getconfig():
    configdict = {}

    ############################################################
    # List of observables for all the PDFs.
    # The content of this dictionary determines the observables
    # to generate for and how may taggers are present.
    ############################################################
    configdict["Observables"] = {
        "BeautyMass": {
            "Type": "RooRealVar",
            "Title": "B mass (MeV/c^2)",
            "Range": [5300, 5800],
        },
        "CharmMass": {
            "Type": "RooRealVar",
            "Title": "D mass (MeV/c^2)",
            "Range": [1930, 2015],
        },
        "BeautyTime": {
            "Type": "RooRealVar",
            "Title": "B decay time (ps)",
            "Range": [0.4, 15.0],
        },
        "BeautyTimeErr": {
            "Type": "RooRealVar",
            "Title": "B decay time error (ps)",
            "Range": [0.01, 0.1],
        },
        "MistagOS": {
            "Type": "RooRealVar",
            "Title": "#eta_{OS}",
            "Range": [0.0, 0.5],
        },
        "MistagSS": {
            "Type": "RooRealVar",
            "Title": "#eta_{SS}",
            "Range": [0.0, 0.5],
        },
        "TagDecOS": {
            "Type": "RooCategory",
            "Title": "q_{t}^{OS}",
            "Categories": {
                "B+": +1,
                "Untagged": 0,
                "B-": -1,
            },
        },
        "TagDecSS": {
            "Type": "RooCategory",
            "Title": "q_{t}^{SS}",
            "Categories": {
                "B+": +1,
                "Untagged": 0,
                "B-": -1,
            },
        },
        "BacCharge": {
            "Type": "RooCategory",
            "Title": "BacCharge",
            "Categories": {
                "h+": +1,
                "h-": -1,
            },
        },
    }

    ############################################################
    # List of mass hypotheses for bachelor
    # The content of this dictionary determines how many
    # bachelor PID bins the final dataset is splitted into
    ############################################################
    configdict["Hypothesys"] = ["Bs2DsPi"]

    ############################################################
    # Signal decay, Charm decay mode and year of data taking
    # Splitting per magnet polarity not implemented, at the moment
    ############################################################
    configdict["Decay"] = "Bs2DsPi"
    configdict["CharmModes"] = [
        "PhiPi",
        "KstK",
        "NonRes",
        "PiPiPi",
    ]
    configdict["Years"] = [
        "2015",
        "2016",
        "2017",
        "2018",
    ]

    ############################################################
    # For PIDK shapes we need also polarities
    # This is also used to split the yields between years properly
    ############################################################
    configdict["Polarity"] = ["Up", "Down"]
    configdict["IntegratedLuminosity"] = {
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        },
    }

    configdict["FractionsLuminosity"] = {
        k: v['Up'] / (v['Up'] + v['Down'])
        for k, v in configdict['IntegratedLuminosity'].items()
    }

    configdict["WorkspaceToRead"] = {
        "File": WORKSPACEFILE,
        "Workspace": WORKSPACENAME
    }
    ############################################################
    # List of components with yields to generate.
    # The content of this dictionary determines, for each
    # PID bin and year, how many PDF components are generated.
    # If there is only signal, a TTree ready for sFit is
    # generated directly, without need for doing a (useless)
    # mass fit.
    ############################################################
    configdict["Components"] = {
        "Signal": {
            "Bs2DsPi": {
                '20152016': {
                    'KstK': 36220.0,
                    'NonRes': 23090.0,
                    'PhiPi': 49990.0,
                    'PiPiPi': 23320.0
                },
                '2017': {
                    'KstK': 31580.0,
                    'NonRes': 20100.0,
                    'PhiPi': 43240.0,
                    'PiPiPi': 20270.0
                },
                '2018': {
                    'KstK': 36590.0,
                    'NonRes': 22700.0,
                    'PhiPi': 49690.0,
                    'PiPiPi': 22850.0
                }
            }
        },
        "Combinatorial": {
            'Bs2DsPi': {
                '20152016': {
                    'KstK': 3273.0,
                    'NonRes': 6767.0,
                    'PhiPi': 3482.0,
                    'PiPiPi': 11810.0
                },
                '2017': {
                    'KstK': 2635.0,
                    'NonRes': 5423.0,
                    'PhiPi': 2934.0,
                    'PiPiPi': 9128.0
                },
                '2018': {
                    'KstK': 3069.0,
                    'NonRes': 6758.0,
                    'PhiPi': 3560.0,
                    'PiPiPi': 10830.0
                }
            }
        },
        "Bd2DPi": {
            "Bs2DsPi": {
                '20152016': {
                    'KstK': 293.5,
                    'NonRes': 1010.0,
                    'PhiPi': 19.18,
                    'PiPiPi': 0.0,
                },
                '2017': {
                    'KstK': 265.2,
                    'NonRes': 931.5,
                    'PhiPi': 22.11,
                    'PiPiPi': 0.0,
                },
                '2018': {
                    'KstK': 319.4,
                    'NonRes': 1123.0,
                    'PhiPi': 21.59,
                    'PiPiPi': 0.0,
                }
            },
        },
        "Lb2LcPi": {
            "Bs2DsPi": {
                '20152016': {
                    'KstK': 168.8,
                    'NonRes': 781.3,
                    'PhiPi': 68.49,
                    'PiPiPi': 0.0,
                },
                '2017': {
                    'KstK': 133.1,
                    'NonRes': 650.2,
                    'PhiPi': 52.03,
                    'PiPiPi': 0.0,
                },
                '2018': {
                    'KstK': 159.3,
                    'NonRes': 765.8,
                    'PhiPi': 74.43,
                    'PiPiPi': 0.0,
                }
            }
        },
        "Bs2DsK": {
            "Bs2DsPi": {
                '20152016': {
                    'KstK': 281.3,
                    'NonRes': 181.0,
                    'PhiPi': 405.3,
                    'PiPiPi': 185.2
                },
                '2017': {
                    'KstK': 227.0,
                    'NonRes': 145.1,
                    'PhiPi': 323.1,
                    'PiPiPi': 148.9
                },
                '2018': {
                    'KstK': 277.3,
                    'NonRes': 173.5,
                    'PhiPi': 391.4,
                    'PiPiPi': 176.8
                }
            }
        },
        # As defined in Bs2DshModels, the Bs2DsDsstPiRho packground is
        # g1_f1 * Bs2DsstPi + (1 - g1_f1) * Bs2DsPi
        # (second case of RooAddPdf)
        "Bd2DsPi": {
            'Bs2DsPi': {
                '20152016': {
                    'KstK': 317.3,
                    'NonRes': 165.6,
                    'PhiPi': 449.7,
                    'PiPiPi': 227.1
                },
                '2017': {
                    'KstK': 196.1,
                    'NonRes': 110.0,
                    'PhiPi': 353.7,
                    'PiPiPi': 201.8
                },
                '2018': {
                    'KstK': 320.9,
                    'NonRes': 170.0,
                    'PhiPi': 302.2,
                    'PiPiPi': 160.4
                }
            },
        },
        "Bs2DsstPi": {
            'Bs2DsPi': {
                '20152016': {
                    'KstK': 317.3,
                    'NonRes': 165.6,
                    'PhiPi': 449.7,
                    'PiPiPi': 227.1
                },
                '2017': {
                    'KstK': 196.1,
                    'NonRes': 110.0,
                    'PhiPi': 353.7,
                    'PiPiPi': 201.8
                },
                '2018': {
                    'KstK': 320.9,
                    'NonRes': 170.0,
                    'PhiPi': 302.2,
                    'PiPiPi': 160.4
                }
            },
        }
    }

    # now split 20152016 to 2015 + 2016 individually, and adjust yields
    # according to polarity ratios
    total_lumi = (configdict["IntegratedLuminosity"]["2015"]["Down"] +
                  configdict["IntegratedLuminosity"]["2015"]["Up"] +
                  configdict["IntegratedLuminosity"]["2016"]["Down"] +
                  configdict["IntegratedLuminosity"]["2016"]["Up"])
    frac_2015 = (
        (configdict["IntegratedLuminosity"]["2015"]["Up"] +
         configdict["IntegratedLuminosity"]["2015"]["Down"]) / total_lumi)
    frac_2016 = (
        (configdict["IntegratedLuminosity"]["2016"]["Up"] +
         configdict["IntegratedLuminosity"]["2016"]["Down"]) / total_lumi)
    for comp in configdict["Components"].keys():
        for decay in configdict["Components"][comp].keys():
            if "20152016" in configdict["Components"][comp][decay]:
                yields = configdict["Components"][comp][decay]["20152016"]
                configdict["Components"][comp][decay]["2015"] = {
                    dmode: y * frac_2015
                    for dmode, y in yields.items()
                }
                configdict["Components"][comp][decay]["2016"] = {
                    dmode: y * frac_2016
                    for dmode, y in yields.items()
                }
                del configdict["Components"][comp][decay]["20152016"]
            for year in configdict["Components"][comp][decay]:
                # also split Bs2DsDsstPiRho yields according to g1_f1_frac
                if comp == "Bs2DsstPi":
                    configdict["Components"][comp][decay][year] = {
                        dmode: G1_F1_FRAC * yield_
                        for dmode, yield_ in configdict["Components"][comp]
                        [decay][year].items()
                    }
                elif comp == "Bd2DsPi":
                    configdict["Components"][comp][decay][year] = {
                        dmode: (1 - G1_F1_FRAC) * yield_
                        for dmode, yield_ in configdict["Components"][comp]
                        [decay][year].items()
                    }

                    ############################################################
    # List of PDFs for "time-independent" observables
    # Dictionary structure: observable->component->bachelor hypo->year->D mode
    ############################################################

    ############################################################
    #                      Signal
    ############################################################

    ############################################################

    def getPdfTemplateConfigFromWorkspace(
            name="Signal_BeautyMass_IpatiaPlusJohnsonSU_both_{mode}_{year}",
            modes=["All"],
            years=["2015", "2016", "2017", "2018"],
            lowercasemode=True,
            **kwargs):
        """
        Generate a dictionary of the form
        {
            year1: {
                mode1: {
                    'Type': 'FromWorkspace',
                    'Name': name.format(mode=m1),
                    'WorkspaceName': WorkspaceName,
                    'WorkspaceFile': WorkspaceFile,
                    **kwargs
                },
                mode2: { ... }
            },
            year2: { ... }
        }
        for all modes m1... given in the `modes` and all years in the `years`
        argument.
        """
        return {
            y: {
                m: dict(
                    Type="FromWorkspace",
                    Name=name.format(
                        mode=m.lower() if lowercasemode else m, year=y),
                    **kwargs)
                for m in modes
            }
            for y in years
        }

    configdict["PDFList"] = {
        "BeautyMass": {
            "Signal": {
                "Bs2DsPi": {
                    '20152016': {
                        'KstK': {
                            'a1': 0.2328,
                            'a2': 2.772,
                            'fracI': 0.1213,
                            'l': -1.404,
                            'n1': 50.0,
                            'n2': 0.9744,
                            'nu': -0.1409,
                            'sigmaI': 39.24,
                            'sigmaJ': 15.79,
                            'tau': 0.3432
                        },
                        'NonRes': {
                            'a1': 0.267,
                            'a2': 1.953,
                            'fracI': 0.1148,
                            'l': -1.546,
                            'n1': 50.0,
                            'n2': 2.03,
                            'nu': -0.1927,
                            'sigmaI': 37.4,
                            'sigmaJ': 15.59,
                            'tau': 0.3465
                        },
                        'PhiPi': {
                            'a1': 0.2725,
                            'a2': 2.643,
                            'fracI': 0.1248,
                            'l': -1.477,
                            'n1': 50.0,
                            'n2': 1.07,
                            'nu': -0.2027,
                            'sigmaI': 39.69,
                            'sigmaJ': 16.14,
                            'tau': 0.36
                        },
                        'PiPiPi': {
                            'a1': 0.2819,
                            'a2': 3.022,
                            'fracI': 0.1991,
                            'l': -1.39,
                            'n1': 50.0,
                            'n2': 1.158,
                            'nu': -0.04837,
                            'sigmaI': 33.32,
                            'sigmaJ': 15.97,
                            'tau': 0.3528
                        },
                        'all': {
                            'fb': 0.0,
                            'mean': 5366.0,
                            'zeta': 0.0,
                            'Type': 'IpatiaJohnsonSU'
                        }
                    },
                    '2017': {
                        'KstK': {
                            'a1': 0.2621,
                            'a2': 2.725,
                            'fracI': 0.1264,
                            'l': -1.463,
                            'n1': 50.0,
                            'n2': 0.9997,
                            'nu': -0.1688,
                            'sigmaI': 35.71,
                            'sigmaJ': 15.36,
                            'tau': 0.3448
                        },
                        'NonRes': {
                            'a1': 0.3145,
                            'a2': 2.761,
                            'fracI': 0.1422,
                            'l': -1.594,
                            'n1': 50.0,
                            'n2': 1.278,
                            'nu': -0.1625,
                            'sigmaI': 33.82,
                            'sigmaJ': 15.01,
                            'tau': 0.3195
                        },
                        'PhiPi': {
                            'a1': 0.2665,
                            'a2': 2.96,
                            'fracI': 0.1621,
                            'l': -1.368,
                            'n1': 50.0,
                            'n2': 0.9661,
                            'nu': -0.1253,
                            'sigmaI': 39.26,
                            'sigmaJ': 15.6,
                            'tau': 0.3282
                        },
                        'PiPiPi': {
                            'a1': 0.2706,
                            'a2': 3.484,
                            'fracI': 0.2468,
                            'l': -1.34,
                            'n1': 50.0,
                            'n2': 0.9798,
                            'nu': -0.03509,
                            'sigmaI': 30.81,
                            'sigmaJ': 15.56,
                            'tau': 0.3111
                        },
                        'all': {
                            'fb': 0.0,
                            'mean': 5366.0,
                            'zeta': 0.0,
                            'Type': 'IpatiaJohnsonSU'
                        }
                    },
                    '2018': {
                        'KstK': {
                            'a1': 0.3972,
                            'a2': 2.277,
                            'fracI': 0.1263,
                            'l': -1.615,
                            'n1': 50.0,
                            'n2': 1.176,
                            'nu': -0.1796,
                            'sigmaI': 34.72,
                            'sigmaJ': 15.48,
                            'tau': 0.3378
                        },
                        'NonRes': {
                            'a1': 0.3808,
                            'a2': 2.408,
                            'fracI': 0.1633,
                            'l': -1.77,
                            'n1': 50.0,
                            'n2': 1.687,
                            'nu': -0.2108,
                            'sigmaI': 29.97,
                            'sigmaJ': 14.91,
                            'tau': 0.2943
                        },
                        'PhiPi': {
                            'a1': 0.2387,
                            'a2': 2.795,
                            'fracI': 0.1413,
                            'l': -1.337,
                            'n1': 50.0,
                            'n2': 0.986,
                            'nu': -0.1599,
                            'sigmaI': 39.9,
                            'sigmaJ': 15.67,
                            'tau': 0.3457
                        },
                        'PiPiPi': {
                            'a1': 0.5402,
                            'a2': 2.396,
                            'fracI': 0.1418,
                            'l': -3.35,
                            'n1': 50.0,
                            'n2': 1.518,
                            'nu': -0.2869,
                            'sigmaI': 28.69,
                            'sigmaJ': 15.38,
                            'tau': 0.3382
                        },
                        'all': {
                            'fb': 0.0,
                            'mean': 5366.0,
                            'zeta': 0.0,
                            'Type': 'IpatiaJohnsonSU'
                        }
                    }
                }
            },
            "Combinatorial": {
                "Bs2DsPi": {
                    '20152016': {
                        'KstK': {
                            'cB1': -0.009898,
                            'cB2': -0.0006459,
                            'frac': 0.7049
                        },
                        'NonRes': {
                            'cB1': -0.006502,
                            'cB2': -0.0005537,
                            'frac': 0.8336
                        },
                        'PhiPi': {
                            'cB1': -0.01009,
                            'cB2': -0.0002633,
                            'frac': 0.6948
                        },
                        'PiPiPi': {
                            'cB1': -0.008656,
                            'cB2': -0.0004751,
                            'frac': 0.3972
                        },
                        'all': {
                            'Type': 'DoubleExponential'
                        }
                    },
                    '2017': {
                        'KstK': {
                            'cB1': -0.007839,
                            'cB2': -0.0006459,
                            'frac': 0.8061
                        },
                        'NonRes': {
                            'cB1': -0.006376,
                            'cB2': -0.0005537,
                            'frac': 0.8012
                        },
                        'PhiPi': {
                            'cB1': -0.009344,
                            'cB2': -0.0002633,
                            'frac': 0.7361
                        },
                        'PiPiPi': {
                            'cB1': -0.00898,
                            'cB2': -0.0004751,
                            'frac': 0.3877
                        },
                        'all': {
                            'Type': 'DoubleExponential'
                        }
                    },
                    '2018': {
                        'KstK': {
                            'cB1': -0.009069,
                            'cB2': -0.0006459,
                            'frac': 0.6845
                        },
                        'NonRes': {
                            'cB1': -0.006048,
                            'cB2': -0.0005537,
                            'frac': 0.8423
                        },
                        'PhiPi': {
                            'cB1': -0.01112,
                            'cB2': -0.0002633,
                            'frac': 0.7341
                        },
                        'PiPiPi': {
                            'cB1': -0.009395,
                            'cB2': -0.0004751,
                            'frac': 0.4191
                        },
                        'all': {
                            'Type': 'DoubleExponential'
                        }
                    }
                }
            },
            "Bd2DPi": {
                "Bs2DsPi":
                getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBd2DPiPdf_m_both_{year}"),
            },
            "Lb2LcPi": {
                "Bs2DsPi":
                getPdfTemplateConfigFromWorkspace(
                    "PhysBkgLb2LcPiPdf_m_both_{year}"),
            },
            "Bs2DsK": {
                "Bs2DsPi":
                getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsKPdf_m_both_{year}"),
            },
            "Bs2DsstPi": {
                "Bs2DsPi":
                getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsstPiPdf_m_both_{year}"),
            },
        },
        "CharmMass": {
            "Signal": {
                "Bs2DsPi": {
                    "20152016": {
                        'KstK': {
                            'a1': 0.1725,
                            'a2': 0.3794,
                            'fracI': 0.08704,
                            'sigmaI': 26.27,
                            'sigmaJ': 6.984,
                            'tau': 0.446
                        },
                        'NonRes': {
                            'a1': 0.1605,
                            'a2': 0.3948,
                            'fracI': 0.08644,
                            'sigmaI': 29.91,
                            'sigmaJ': 6.868,
                            'tau': 0.4621
                        },
                        'PhiPi': {
                            'a1': 0.19,
                            'a2': 0.375,
                            'fracI': 0.1297,
                            'sigmaI': 26.73,
                            'sigmaJ': 6.922,
                            'tau': 0.4855
                        },
                        'PiPiPi': {
                            'a1': 0.1833,
                            'a2': 0.8443,
                            'fracI': 0.3127,
                            'sigmaI': 30.48,
                            'sigmaJ': 9.757,
                            'tau': 0.3801
                        },
                        'all': {
                            'fb': 0.0,
                            'l': -1.1,
                            'mean': 1968.0,
                            'n1': 8.0,
                            'n2': 8.0,
                            'nu': 0.0,
                            'zeta': 0.0,
                            'Type': 'IpatiaJohnsonSU'
                        }
                    },
                    "2017": {
                        'KstK': {
                            'a1': 0.1421,
                            'a2': 0.3104,
                            'fracI': 0.06777,
                            'sigmaI': 26.87,
                            'sigmaJ': 6.842,
                            'tau': 0.4542
                        },
                        'NonRes': {
                            'a1': 0.1271,
                            'a2': 0.2609,
                            'fracI': 0.08021,
                            'sigmaI': 26.66,
                            'sigmaJ': 6.69,
                            'tau': 0.4539
                        },
                        'PhiPi': {
                            'a1': 0.2028,
                            'a2': 0.494,
                            'fracI': 0.1362,
                            'sigmaI': 27.2,
                            'sigmaJ': 6.776,
                            'tau': 0.4685
                        },
                        'PiPiPi': {
                            'a1': 0.173,
                            'a2': 0.8052,
                            'fracI': 0.2715,
                            'sigmaI': 30.37,
                            'sigmaJ': 9.553,
                            'tau': 0.3871
                        },
                        'all': {
                            'fb': 0.0,
                            'l': -1.1,
                            'mean': 1968.0,
                            'n1': 8.0,
                            'n2': 8.0,
                            'nu': 0.0,
                            'zeta': 0.0,
                            'Type': 'IpatiaJohnsonSU'
                        }
                    },
                    "2018": {
                        'KstK': {
                            'a1': 0.1365,
                            'a2': 0.272,
                            'fracI': 0.06934,
                            'sigmaI': 24.85,
                            'sigmaJ': 6.885,
                            'tau': 0.4485
                        },
                        'NonRes': {
                            'a1': 0.1624,
                            'a2': 0.3777,
                            'fracI': 0.09478,
                            'sigmaI': 27.78,
                            'sigmaJ': 6.635,
                            'tau': 0.4653
                        },
                        'PhiPi': {
                            'a1': 0.1653,
                            'a2': 0.2984,
                            'fracI': 0.1209,
                            'sigmaI': 24.31,
                            'sigmaJ': 6.836,
                            'tau': 0.4903
                        },
                        'PiPiPi': {
                            'a1': 0.1844,
                            'a2': 0.8881,
                            'fracI': 0.3081,
                            'sigmaI': 29.31,
                            'sigmaJ': 9.37,
                            'tau': 0.3879
                        },
                        'all': {
                            'fb': 0.0,
                            'l': -1.1,
                            'mean': 1968.0,
                            'n1': 8.0,
                            'n2': 8.0,
                            'nu': 0.0,
                            'zeta': 0.0,
                            'Type': 'IpatiaJohnsonSU'
                        }
                    }
                },
            },
            "Combinatorial": {
                "Bs2DsPi": {
                    '20152016': {
                        'KstK': {
                            'cD': -0.00651,
                            'fracD': 0.7764
                        },
                        'NonRes': {
                            'cD': -0.004086,
                            'fracD': 0.9546
                        },
                        'PhiPi': {
                            'cD': -0.007506,
                            'fracD': 0.6024
                        },
                        'PiPiPi': {
                            'cD': -0.005947,
                            'fracD': 0.9756
                        },
                        'all': {
                            'Type': 'ExponentialPlusSignal',
                        }
                    },
                    '2017': {
                        'KstK': {
                            'cD': -0.008503,
                            'fracD': 0.8232
                        },
                        'NonRes': {
                            'cD': -0.005393,
                            'fracD': 0.9571
                        },
                        'PhiPi': {
                            'cD': -0.006314,
                            'fracD': 0.6075
                        },
                        'PiPiPi': {
                            'cD': -0.006253,
                            'fracD': 0.9768
                        },
                        'all': {
                            'Type': 'ExponentialPlusSignal',
                        }
                    },
                    '2018': {
                        'KstK': {
                            'cD': -0.007638,
                            'fracD': 0.8543
                        },
                        'NonRes': {
                            'cD': -0.005545,
                            'fracD': 0.9513
                        },
                        'PhiPi': {
                            'cD': -0.00792,
                            'fracD': 0.6018
                        },
                        'PiPiPi': {
                            'cD': -0.005895,
                            'fracD': 0.9572
                        },
                        'all': {
                            'Type': 'ExponentialPlusSignal',
                        }
                    }
                }
            },
            "Bd2DPi": {
                "Bs2DsPi":
                getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBd2DPiPdf_m_both_{year}_Ds"),
            },
            "Lb2LcPi": {
                "Bs2DsPi":
                getPdfTemplateConfigFromWorkspace(
                    "PhysBkgLb2LcPiPdf_m_both_{year}_Ds"),
            },
        },
    }

    # translate "all" configs on dmode level
    for obskey in configdict["PDFList"]:
        for compkey in configdict["PDFList"][obskey]:
            for decaykey in configdict["PDFList"][obskey][compkey]:
                for yearkey in configdict["PDFList"][obskey][compkey][
                        decaykey]:
                    if 'all' in configdict["PDFList"][obskey][compkey][
                            decaykey][yearkey]:
                        for dmode in configdict["CharmModes"]:
                            configdict["PDFList"][obskey][compkey][decaykey][
                                yearkey][dmode].update(
                                    configdict["PDFList"][obskey][compkey]
                                    [decaykey][yearkey]['all'])
                        configdict["PDFList"][obskey][compkey][decaykey][
                            yearkey].pop('all')
    # copy signal config for Bd2DsPi and adjust mass shift
    configdict["PDFList"]["BeautyMass"]["Bd2DsPi"] = copy.deepcopy(
        configdict["PDFList"]["BeautyMass"]["Signal"])
    for yearkey in configdict["PDFList"]["BeautyMass"]["Bd2DsPi"]["Bs2DsPi"]:
        for dmodekey in configdict["PDFList"]["BeautyMass"]["Bd2DsPi"][
                "Bs2DsPi"][yearkey]:
            configdict["PDFList"]["BeautyMass"]["Bd2DsPi"]["Bs2DsPi"][yearkey][
                dmodekey]["mean"] -= 87.38

    configdict["PDFList"]["CharmMass"]["Bs2DsK"] = configdict["PDFList"][
        "CharmMass"]["Signal"]
    configdict["PDFList"]["CharmMass"]["Bs2DsstPi"] = configdict["PDFList"][
        "CharmMass"]["Signal"]
    configdict["PDFList"]["CharmMass"]["Bd2DsPi"] = configdict["PDFList"][
        "CharmMass"]["Signal"]

    # now copy the 2015 config for 2016 as we use only one model in the
    # actual md fit
    for var in configdict["PDFList"].keys():
        for comp in configdict["PDFList"][var].keys():
            for decay in configdict["PDFList"][var][comp].keys():
                if "2015" in configdict["PDFList"][var][comp][decay]:
                    configdict["PDFList"][var][comp][decay][
                        "2016"] = configdict["PDFList"][var][comp][decay][
                            "2015"]
                elif "20152016" in configdict["PDFList"][var][comp][decay]:
                    configdict["PDFList"][var][comp][decay][
                        "2015"] = configdict["PDFList"][var][comp][decay][
                            "20152016"]
                    configdict["PDFList"][var][comp][decay][
                        "2016"] = configdict["PDFList"][var][comp][decay][
                            "20152016"]

    ############################################################
    # Tagging calibration and mistag PDF. If "MistagPDF" : None,
    # then a average mistag is used
    ############################################################
    BackgroundMistagConfig = {
        "SS": {
            "Calibration": {
                "p0": [0.45],
                "p1": [1.],
                "deltap0": [0],
                "deltap1": [0],
                "avgeta": [0.45],
                "tageff": [0.50],
                "tagasymm": [0]
            },
            "MistagPDF": {
                "Type": "Mock",
                "eta0": [0.1],
                "etaavg": [0.4],
                "f": [1.0],
            }
        },
        "OS": {
            "Calibration": {
                "p0": [0.3],
                "p1": [1.],
                "deltap0": [0.0],
                "deltap1": [0.0],
                "avgeta": [0.3],
                "tageff": [0.40],
                "tagasymm": [0.0]
            },
            "MistagPDF": {
                "Type": "Mock",
                "eta0": [0.1],
                "etaavg": [0.4],
                "f": [1.0],
            }
        },
    }

    configdict["Taggers"] = {
        "Signal": {
            "SS": {
                # DecRateCoeff_Bd is using omega = p0 + p1(eta - average)
                # i.e. perfect calibration == p0 = average, p1 = 1
                "Calibration": {
                    "p0": [0.437],
                    "p1": [0.759],
                    "deltap0": [-0.018],
                    "deltap1": [0.037],
                    "avgeta": [0.416],
                    "tageff": [0.696],
                    "tagasymm": [-0.001]
                },
                "MistagPDF": {
                    "Type":
                    "FromData",
                    "TemplateFile":
                    os.path.join(os.environ["WORKDIR"],
                                 "mdfit/work_bs2dspi_data_Final.root"),
                },
            },
            "OS": {
                # DecRateCoeff_Bd is using omega = p0 + p1(eta - average)
                # i.e. perfect calibration == p0 = average, p1 = 1
                "Calibration": {
                    "p0": [0.378],
                    "p1": [0.898],
                    "deltap0": [0.00791],
                    "deltap1": [0.0392],
                    "avgeta": [0.35],
                    "tageff": [0.411],
                    "tagasymm": [0.00336]
                },
                "MistagPDF": {
                    "Type":
                    "FromData",
                    "TemplateFile":
                    os.path.join(os.environ["WORKDIR"],
                                 "mdfit/work_bs2dspi_data_Final.root"),
                },
            },
        },
        "Combinatorial": BackgroundMistagConfig,
        "Bd2DPi": BackgroundMistagConfig,
        "Lb2LcPi": BackgroundMistagConfig,
        "Bd2DsPi": BackgroundMistagConfig,  # same as signal
        "Bs2DsstPi": BackgroundMistagConfig,  # same as signal
        "Bs2DsK": BackgroundMistagConfig,
    }

    ############################################################
    # Time resolution and acceptance (there is a single dict because
    # they are strongly connected in the way they are built).
    # If "TimeErrorPDF" : None, then an average resolution model
    # is used.
    ############################################################

    # use same config for every mode
    ResolutionAcceptance = {
        "TimeErrorPDF": {
            "Type":
            "FromData",
            "TemplateFile":
            os.path.join(os.environ["WORKDIR"],
                         "mdfit/work_bs2dspi_data_Final.root"),
        },
        "Acceptance": {
            "Type": "Spline",
            "KnotPositions": [0.5, 1.0, 1.5, 2.0, 3.0, 12.0],
            "KnotCoefficients": [0.348, 0.49, 0.821, 0.954, 1.09, 1.25],
            "extrapolate": True,
        },
        "Resolution": {
            "Type": "GaussianWithPEDTE",
            "Bias": -0.003,
            "ScaleFactor": 1.0,
            "P0": 0.006,
            "P1": 1.05,
            "P2": 0.0,
        }
    }
    configdict["ResolutionAcceptance"] = {
        c: ResolutionAcceptance
        for c in configdict["Components"].keys()
    }

    ############################################################
    # Production and detection asymmetries
    ############################################################

    configdict["ProductionAsymmetry"] = {}
    configdict["DetectionAsymmetry"] = {}
    for comp in configdict["Components"].keys():
        configdict["ProductionAsymmetry"][comp] = [-0.002]
        configdict["DetectionAsymmetry"][comp] = [-0.0015]

    ############################################################
    # Time PDF parameters
    ############################################################

    cpParamsBd = {
        "Gamma": [0.658],
        "DeltaGamma": [0.000658],
        "DeltaM": [0.5065],
        "C": [1.0],
        "S": [0.058],
        "Sbar": [0.038],
        "D": [0.0],
        "Dbar": [0.0],
        "ParameteriseIntegral": True,
        "NBinsAcceptance": 0,  # keep at zero if using splineacceptance!
        "NBinsProperTimeErr": 100
    }
    cpParamsBs = {
        "Gamma": [0.6624],
        "DeltaGamma": [0.089424],
        "DeltaM": [17.757],
        "C": [0.19],
        "S": [0.17],
        "Sbar": [0],
        "D": [-0.75],
        "Dbar": [0.0],
        "ParameteriseIntegral": True,
        "NBinsAcceptance": 0,  # keep at zero if using splineacceptance!
        "NBinsProperTimeErr": 100
    }

    configdict["ACP"] = {
        # Signal (use more convenient interface with ArgLf_d,
        # ArgLbarfbar_d and ModLf_d)
        "Signal": {
            "Gamma": [0.6624],
            "DeltaGamma": [0.089424],
            "DeltaM": [17.757],
            "C": [1.0],
            "S": [0.0],
            "Sbar": [0.0],
            "D": [0.0],
            "Dbar": [0.0],
            "ParameteriseIntegral": True,
            "NBinsAcceptance": 0,  # keep at zero if using spline acceptance!
            "NBinsProperTimeErr": 100,
        },
        "Combinatorial": {
            "Gamma": [0.8],
            "DeltaGamma": [0.0],
            "DeltaM": [0.0],
            "C": [0.0],
            "S": [0.0],
            "Sbar": [0.0],
            "D": [0.0],
            "Dbar": [0.0],
            "ParameteriseIntegral": True,
            "NBinsAcceptance": 0,  # keep at zero if using splineacceptance!
            "NBinsProperTimeErr": 100
        },
        "Bd2DPi": cpParamsBd,
        "Bd2DsPi": cpParamsBd,
        "Bs2DsstPi": cpParamsBs,
        "Bs2DsK": cpParamsBs,
        "Lb2LcPi": {
            "Gamma": [0.68],
            "DeltaGamma": [0.0],
            "DeltaM": [0.0],
            "C": [0.19],
            "S": [0.17],
            "Sbar": [0.0],
            "D": [-0.5],
            "Dbar": [0.0],
            "ParameteriseIntegral": True,
            "NBinsAcceptance": 0,  # keep at zero if using splineacceptance!
            "NBinsProperTimeErr": 100
        }
    }

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main
    main(getconfig())
