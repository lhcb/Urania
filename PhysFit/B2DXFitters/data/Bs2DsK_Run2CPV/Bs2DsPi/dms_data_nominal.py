###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys
import itertools

import mdfit_nominal


def getconfig():
    configdict = mdfit_nominal.getconfig()

    # PHYSICAL PARAMETERS
    configdict["Gammas"] = 0.6600  # in ps^{-1}
    configdict["DeltaGammas"] = 0.085
    configdict["DeltaMs"] = 17.757  # in ps^{-1}
    configdict["Labels"] = ["20152016", "2017", "2018"]

    configdict["Asymmetries"] = {"Detection": 0.0, "Production": 0.0}

    configdict["ConstrainsForTaggingCalib"] = False

    configdict["SWeightCorrection"] = "Read"

    configdict["Resolution"] = {
        "20152016": {
            "scaleFactor": {
                "p0": 0.008377,
                "p1": 1.002,
                "p2": 0.0
            },
            "meanBias": -0.002254
        },
        "2017": {
            "scaleFactor": {
                "p0": 0.006092,
                "p1": 1.048,
                "p2": 0.0
            },
            "meanBias": -0.003047
        },
        "2018": {
            "scaleFactor": {
                "p0": 0.0055521,
                "p1": 1.052,
                "p2": 0.0
            },
            "meanBias": -0.002394
        }
    }

    configdict["TaggingCalibration"] = {
        "OS": {
            y: {
                "p0": 0.3467,
                "dp0": 0.0,
                "p1": 1.0,
                "dp1": 0.0,
                "average": 0.3467,
                "tagEff": 0.4106,
                "aTagEff": 0.0,
                "use": True,
            }
            for y in configdict["Labels"]
        },
        "SS": {
            y: {
                "p0": 0.4161,
                "dp0": 0.0,
                "p1": -0.21 + 1,
                "dp1": 0.0,
                "average": 0.4161,
                "tagEff": 0.6960,
                "aTagEff": 0.0,
                "use": True,
            }
            for y in configdict["Labels"]
        },
    }
    configdict["TaggingCalibration"]["OS"]["20152016"]["tagEff"] = 0.4126
    configdict["TaggingCalibration"]["OS"]["2017"]["tagEff"] = 0.4084
    configdict["TaggingCalibration"]["OS"]["2018"]["tagEff"] = 0.4123
    configdict["TaggingCalibration"]["SS"]["20152016"]["tagEff"] = 0.6918
    configdict["TaggingCalibration"]["SS"]["2017"]["tagEff"] = 0.6992
    configdict["TaggingCalibration"]["SS"]["2018"]["tagEff"] = 0.6973

    configdict["TaggingCalibration"]["OS"]["20152016"]["average"] = 0.3562
    configdict["TaggingCalibration"]["OS"]["2017"]["average"] = 0.3463
    configdict["TaggingCalibration"]["OS"]["2018"]["average"] = 0.3464
    configdict["TaggingCalibration"]["SS"]["20152016"]["average"] = 0.4162
    configdict["TaggingCalibration"]["SS"]["2017"]["average"] = 0.4164
    configdict["TaggingCalibration"]["SS"]["2018"]["average"] = 0.4156

    configdict["Acceptance"] = {
        "knots": [0.50, 1.0, 1.5, 2.0, 3.0, 12.0],
        "20152016": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
        "2017": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
        "2018": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
    }

    configdict["constParams"] = [
        'Gammas_Bs2DsPi',
        'deltaGammas_Bs2DsPi',
        'C_Bs2DsPi',
        'Cbar_Bs2DsPi',
        'S_Bs2DsPi',
        'Sbar_Bs2DsPi',
        'D_Bs2DsPi',
        'Dbar_Bs2DsPi',
        'tagEff_OS',
        'tagEff_SS',
        'average_OS',
        'average_SS',
    ]

    # Add some name variants
    configdict["constParams"] += [
        c + "_" + l for c, l in itertools.product(configdict["constParams"],
                                                  configdict["Labels"])
    ]

    return configdict


if __name__ == '__main__':
    sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'Toys'))
    from configutils import main
    main(getconfig())
