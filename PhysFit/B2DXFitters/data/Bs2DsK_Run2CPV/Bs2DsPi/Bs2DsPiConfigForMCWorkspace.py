###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()
    configdict["YearOfDataTaking"] = {"2018"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1"
    }

    isMuon = "lab1_isMuon==0&&lab3_isMuon==0&&lab4_isMuon==0&&lab5_isMuon==0"
    hasRICH = "lab1_hasRich==1&&lab3_hasRich==1&&lab4_hasRich==1&&lab5_hasRich==1"
    app = "&&"
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "lab1_isMuon==0",
        "MC": "lab1_M<200&&lab1_PIDK !=-1000.0" + app + isMuon + app + hasRICH,
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": True,
        "DsHypo": True
    }
    configdict["Sim09h"] = False
    if not configdict["Sim09h"]:
        configdict[
            "dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi.txt"
    else:
        configdict[
            "dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi_Sim09h.txt"

    #weighting templates by PID eff/misID
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2015": {
                "FileLabel":
                "#PIDK Pion 2015",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
            "2016": {
                "FileLabel":
                "#PIDK Pion 2016",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
            "2017": {
                "FileLabel":
                "#PIDK Pion 2017",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
            "2018": {
                "FileLabel":
                "#PIDK Pion 2018",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
        },
        "PIDBachMisID": {
            "2015": {
                "FileLabel":
                "#PIDK Kaon 2015",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "K_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
            "2016": {
                "FileLabel":
                "#PIDK Kaon 2016",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "K_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
            "2017": {
                "FileLabel":
                "#PIDK Kaon 2017",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "K_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
            "2018": {
                "FileLabel":
                "#PIDK Kaon 2018",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "K_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
        },
        "PIDChildKaonPionMisID": {
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["nTracks", "lab3_P"],
                "HistName":
                "Pi_DLLK > 10 && IsMuon ==0.0 && HasRich ==1.0_All;"
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["nTracks", "lab3_P"],
                "HistName":
                "Pi_DLLK > 10 && IsMuon ==0.0 && HasRich ==1.0_All;"
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2017",
                "Var": ["nTracks", "lab3_P"],
                "HistName":
                "Pi_DLLK > 10 && IsMuon ==0.0 && HasRich ==1.0_All;"
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2018",
                "Var": ["nTracks", "lab3_P"],
                "HistName":
                "Pi_DLLK > 10 && IsMuon ==0.0 && HasRich ==1.0_All;"
            },
        },
        "PIDChildProtonMisID": {
            "2015": {
                "FileLabel":
                "#PIDK Proton 2015",
                "Var": ["nTracks", "lab3_P"],
                "HistName":
                "P_DLLK>5  && (DLLp - DLLK < -5) && IsMuon ==0.0 && HasRich ==1.0_All;"
            },
            "2016": {
                "FileLabel":
                "#PIDK Proton 2016",
                "Var": ["nTracks", "lab3_P"],
                "HistName":
                "P_DLLK>5  && (DLLp - DLLK < -5) && IsMuon ==0.0 && HasRich ==1.0_All;"
            },
            "2017": {
                "FileLabel":
                "#PIDK Proton 2017",
                "Var": ["nTracks", "lab3_P"],
                "HistName":
                "P_DLLK>5  && (DLLp - DLLK < -5) && IsMuon ==0.0 && HasRich ==1.0_All;"
            },
            "2018": {
                "FileLabel":
                "#PIDK Proton 2018",
                "Var": ["nTracks", "lab3_P"],
                "HistName":
                "P_DLLK>5  && (DLLp - DLLK < -5) && IsMuon ==0.0 && HasRich ==1.0_All;"
            },
        },
        "RatioDataMC": {
            "2015": {
                "FileLabel": "#DataMC 2015 Sim09c",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2016": {
                "FileLabel": "#DataMC 2016 Sim09c",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2017": {
                "FileLabel": "#DataMC 2017 Sim09g",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
            "2018": {
                "FileLabel": "#DataMC 2018 Sim09h",
                "Var": ["lab1_P", "nTracks"],
                "HistName": "histRatio"
            },
        },
        "Shift": {
            "2011": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
            "2012": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
            "2015": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
            "2016": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
            "2017": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
            "2018": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
        }
    }
    if configdict["Sim09h"]:
        configdict["WeightingMassTemplates"]["RatioDataMC"]["2017"][
            "FileLabel"] = "#DataMC 2017 Sim09h"

    return configdict
