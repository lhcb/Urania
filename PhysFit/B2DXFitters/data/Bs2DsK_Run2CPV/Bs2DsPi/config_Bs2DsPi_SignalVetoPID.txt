#Signal Bs2DsPi KKPi 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping24r1_Sim09c/BDTG/
B2DX_MC_13264021_Bs_Dspi_KKpi_2015_dw_Bs_BDTG.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2015_up_Bs_BDTG.root
DecayTree
DecayTree
###

#Signal Bs2DsPi NonRes 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping24r1_Sim09c/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2015_dw_Bs_BDTG_NonRes_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2015_up_Bs_BDTG_NonRes_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi PhiPi 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping24r1_Sim09c/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2015_dw_Bs_BDTG_PhiPi_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2015_up_Bs_BDTG_PhiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi KstK 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping24r1_Sim09c/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2015_dw_Bs_BDTG_KstK_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2015_up_Bs_BDTG_KstK_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi KPiPi 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping24r1_Sim09c/OFFLINE/
B2DX_MC_13164044_Bs_Dspi_Kpipi_2015_dw_Bs_BDTG_KPiPi_pidcorr.root
B2DX_MC_13164044_Bs_Dspi_Kpipi_2015_up_Bs_BDTG_KPiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi PiPiPi 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping24r1_Sim09c/OFFLINE/
B2DX_MC_13164042_Bs_Dspi_pipipi_2015_dw_Bs_BDTG_PiPiPi_pidcorr.root
B2DX_MC_13164042_Bs_Dspi_pipipi_2015_up_Bs_BDTG_PiPiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi KKPi 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping28r1_Sim09h/BDTG/
B2DX_MC_13264021_Bs_Dspi_KKpi_2016_dw_Bs_BDTG_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2016_up_Bs_BDTG_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi NonRes 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping28r1_Sim09h/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2016_dw_Bs_BDTG_NonRes_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2016_up_Bs_BDTG_NonRes_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi PhiPi 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping28r1_Sim09h/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2016_dw_Bs_BDTG_PhiPi_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2016_up_Bs_BDTG_PhiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi KstK 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping28r1_Sim09h/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2016_dw_Bs_BDTG_KstK_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2016_up_Bs_BDTG_KstK_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi KPiPi 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping28r1_Sim09h/OFFLINE/
B2DX_MC_13164044_Bs_Dspi_Kpipi_2016_dw_Bs_BDTG_KPiPi_pidcorr.root
B2DX_MC_13164044_Bs_Dspi_Kpipi_2016_up_Bs_BDTG_KPiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi PiPiPi 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping28r1_Sim09h/OFFLINE/
B2DX_MC_13164042_Bs_Dspi_pipipi_2016_dw_Bs_BDTG_PiPiPi_pidcorr.root
B2DX_MC_13164042_Bs_Dspi_pipipi_2016_up_Bs_BDTG_PiPiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi NonRes 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping29r2_Sim09h/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2017_dw_Bs_BDTG_NonRes_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2017_up_Bs_BDTG_NonRes_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi PhiPi 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping29r2_Sim09h/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2017_dw_Bs_BDTG_PhiPi_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2017_up_Bs_BDTG_PhiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi KstK 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping29r2_Sim09h/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2017_dw_Bs_BDTG_KstK_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2017_up_Bs_BDTG_KstK_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi KPiPi 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping29r2_Sim09h/OFFLINE/
B2DX_MC_13164044_Bs_Dspi_Kpipi_2017_dw_Bs_BDTG_KPiPi_pidcorr.root
B2DX_MC_13164044_Bs_Dspi_Kpipi_2017_up_Bs_BDTG_KPiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi PiPiPi 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping29r2_Sim09h/OFFLINE/
B2DX_MC_13164042_Bs_Dspi_pipipi_2017_dw_Bs_BDTG_PiPiPi_pidcorr.root
B2DX_MC_13164042_Bs_Dspi_pipipi_2017_up_Bs_BDTG_PiPiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi NonRes 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping34_Sim09h/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2018_dw_Bs_BDTG_NonRes_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2018_up_Bs_BDTG_NonRes_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi PhiPi 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping34_Sim09h/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2018_dw_Bs_BDTG_PhiPi_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2018_up_Bs_BDTG_PhiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi KstK 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping34_Sim09h/OFFLINE/
B2DX_MC_13264021_Bs_Dspi_KKpi_2018_dw_Bs_BDTG_KstK_pidcorr.root
B2DX_MC_13264021_Bs_Dspi_KKpi_2018_up_Bs_BDTG_KstK_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi KPiPi 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping34_Sim09h/OFFLINE/
B2DX_MC_13164044_Bs_Dspi_Kpipi_2018_dw_Bs_BDTG_KPiPi_pidcorr.root
B2DX_MC_13164044_Bs_Dspi_Kpipi_2018_up_Bs_BDTG_KPiPi_pidcorr.root
DecayTree
DecayTree
###

#Signal Bs2DsPi PiPiPi 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/MC_Run2/Stripping34_Sim09h/OFFLINE/
B2DX_MC_13164042_Bs_Dspi_pipipi_2018_dw_Bs_BDTG_PiPiPi_pidcorr.root
B2DX_MC_13164042_Bs_Dspi_pipipi_2018_up_Bs_BDTG_PiPiPi_pidcorr.root
DecayTree
DecayTree
###


#PIDK Kaon 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_K_Turbo15_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_K_Turbo15_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Kaon 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_K_Turbo16_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_K_Turbo16_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Kaon 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_K_Turbo17_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_K_Turbo17_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Kaon 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_K_Turbo18_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_K_Turbo18_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Pion 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_Pi_Turbo15_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_Pi_Turbo15_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Pion 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_Pi_Turbo16_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_Pi_Turbo16_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Pion 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_Pi_Turbo17_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_Pi_Turbo17_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Pion 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_Pi_Turbo18_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_Pi_Turbo18_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Proton 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_P_Turbo15_MagDown_highMomProton_nTracks_Brunel_Brunel_P.root
PerfHists_P_Turbo15_MagUp_highMomProton_nTracks_Brunel_Brunel_P.root
###

#PIDK Proton 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_P_Turbo16_MagDown_highMomProton_nTracks_Brunel_Brunel_P.root
PerfHists_P_Turbo16_MagUp_highMomProton_nTracks_Brunel_Brunel_P.root
###

#PIDK Proton 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_P_Turbo17_MagDown_highMomProton_nTracks_Brunel_Brunel_P.root
PerfHists_P_Turbo17_MagUp_highMomProton_nTracks_Brunel_Brunel_P.root
###

#PIDK Proton 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_P_Turbo18_MagDown_highMomProton_nTracks_Brunel_Brunel_P.root
PerfHists_P_Turbo18_MagUp_highMomProton_nTracks_Brunel_Brunel_P.root
###

#DataMC 2015 Sim09c
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping24r1_Sim09c/
weights_DPi_2015_dw.root
weights_DPi_2015_up.root
###

#DataMC 2016 Sim09h
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping28r1_Sim09h/
weights_DPi_2016_dw.root
weights_DPi_2016_up.root
###

#DataMC 2017 Sim09h
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping29r2_Sim09h/
weights_DPi_2017_dw.root
weights_DPi_2017_up.root
###

#DataMC 2018 Sim09h
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping34_Sim09h/
weights_DPi_2018_dw.root
weights_DPi_2018_up.root
###


#DataMC 2015
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/lab0vs2_ENDVERTEX_ZERR/Stripping24r1_Sim09c/
weights_DPi_2015_dw.root
weights_DPi_2015_up.root
###

#DataMC 2016
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/lab0vs2_ENDVERTEX_ZERR/Stripping28r1_Sim09c/
weights_DPi_2016_dw.root
weights_DPi_2016_up.root
###

#DataMC 2017
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/lab0vs2_ENDVERTEX_ZERR/Stripping29r2_Sim09g/
weights_DPi_2017_dw.root
weights_DPi_2017_up.root
###

#DataMC 2018
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/lab0vs2_ENDVERTEX_ZERR/Stripping34_Sim09h/
weights_DPi_2018_dw.root
weights_DPi_2018_up.root
###

