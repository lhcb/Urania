###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys
import itertools
from math import pi

import Bs2DsKConfigForNominalMassFit


def getconfig():
    configdict = Bs2DsKConfigForNominalMassFit.getconfig()

    # PHYSICAL PARAMETERS
    configdict["Gammas"] = 0.6600  # in ps^{-1}
    configdict["DeltaGammas"] = 0.085
    configdict["DeltaMs"] = 17.7683  # in ps^{-1}
    configdict["Labels"] = ["20152016", "2017", "2018"]
    configdict["StrongPhase"] = 20. / 180. * pi
    configdict["WeakPhase"] = 70. / 180. * pi
    configdict["ModLf"] = 0.372
    configdict["CPlimit"] = {"upper": 4.0, "lower": -4.0}

    configdict["Asymmetries"] = {"Detection": 0.0096, "Production": -0.0031}

    configdict["ConstrainsForTaggingCalib"] = True

    configdict["SWeightCorrection"] = "AsymptoticallyCorrect"

    configdict["Resolution"] = {
        "20152016": {
            "scaleFactor": {
                "p0": 0.0125,
                "p1": 0.950,
                "p2": 0.0
            },
            "meanBias": -0.002254
        },
        "2017": {
            "scaleFactor": {
                "p0": 0.0099,
                "p1": 0.955,
                "p2": 0.0
            },
            "meanBias": -0.003047
        },
        "2018": {
            "scaleFactor": {
                "p0": 0.0098,
                "p1": 0.948,
                "p2": 0.0
            },
            "meanBias": -0.002394
        }
    }

    # Todo: update ft parameters. Values here are from dspi ananote (wrong) use the values from dspi fit result
    configdict["TaggingCalibration"] = {
        "OS": {
            "20152016": {
                "p0":
                3.8483e-01,
                "dp0":
                7.7974e-03,
                "p1":
                9.8998e-01,
                "dp1":
                -8.3170e-04,
                "cov": [[1.94885268e-07, 0, 0, 0], [0, 1.48832710e-05, 0, 0],
                        [0, 0, 7.79541070e-07, 0], [0, 0, 0, 5.95330839e-05]],
                "average":
                0.3562,
                "tagEff":
                0.4126,
                "aTagEff":
                6.6836e-03,
                "use":
                True,
            },
            "2017": {
                "p0":
                3.7631e-01,
                "dp0":
                4.1433e-03,
                "p1":
                8.8059e-01,
                "dp1":
                7.2437e-02,
                "cov": [[1.75991921e-07, 0, 0, 0], [0, 1.36987584e-05, 0, 0],
                        [0, 0, 7.03967685e-07, 0], [0, 0, 0, 5.47950336e-05]],
                "average":
                0.3463,
                "tagEff":
                0.4084,
                "aTagEff":
                3.0749e-03,
                "use":
                True,
            },
            "2018": {
                "p0":
                3.7444e-01,
                "dp0":
                1.1832e-02,
                "p1":
                8.8212e-01,
                "dp1":
                2.4240e-02,
                "cov": [[1.81228092e-07, 0, 0, 0], [0, 1.37089792e-05, 0, 0],
                        [0, 0, 7.24912367e-07, 0], [0, 0, 0, 5.48359164e-05]],
                "average":
                0.3464,
                "tagEff":
                0.4123,
                "aTagEff":
                -3.1091e-03,
                "use":
                True,
            },
        },
        "SS": {
            "20152016": {
                "p0":
                4.3452e-01,
                "dp0":
                -1.6347e-02,
                "p1":
                7.4698e-01,
                "dp1":
                6.9697e-03,
                "cov": [[1.26068407e-07, 0, 0, 0], [0, 1.45272893e-05, 0, 0],
                        [0, 0, 5.04273630e-07, 0], [0, 0, 0, 5.81091570e-05]],
                "average":
                0.4162,
                "tagEff":
                0.6918,
                "aTagEff":
                -1.3327e-03,
                "use":
                True,
            },
            "2017": {
                "p0":
                4.3728e-01,
                "dp0":
                -2.2045e-02,
                "p1":
                7.0713e-01,
                "dp1":
                5.8384e-02,
                "cov": [[1.13045047e-07, 0, 0, 0], [0, 1.31705786e-05, 0, 0],
                        [0, 0, 4.52180189e-07, 0], [0, 0, 0, 5.26823144e-05]],
                "average":
                0.4164,
                "tagEff":
                0.6992,
                "aTagEff":
                1.7918e-03,
                "use":
                True,
            },
            "2018": {
                "p0":
                4.3734e-01,
                "dp0":
                -1.2254e-02,
                "p1":
                7.8264e-01,
                "dp1":
                4.9329e-02,
                "cov": [[1.18870863e-07, 0, 0, 0], [0, 1.34528091e-05, 0, 0],
                        [0, 0, 4.75483452e-07, 0], [0, 0, 0, 5.38112363e-05]],
                "average":
                0.4156,
                "tagEff":
                0.6973,
                "aTagEff":
                -7.4611e-03,
                "use":
                True,
            },
        }
    }

    configdict["Acceptance"] = {
        "knots": [0.50, 1.0, 1.5, 2.0, 3.0, 12.0],
        "20152016": {
            "values":
            ## Nominal values
            [0.304, 0.445, 0.758, 0.945, 1.090, 1.325]
            # Pre March 22 values
            # [2.89e-01, 4.34e-01, 7.36e-01, 9.00e-01, 1.118e+00, 1.048e+00]
            ## Data/MC reqweighted in lap1_P and nTracks for systematic (Nov '21)
            #[2.85e-01, 4.29e-01, 7.53e-01, 8.88e-01, 1.128e+00, 1.031e+00]
        },
        "2017": {
            "values":
            ## Nominal values
            [0.331, 0.461, 0.824, 0.905, 1.074, 1.199]
            # Pre March 22 values
            # [3.14e-01, 4.25e-01, 7.91e-01, 8.54e-01, 1.021e+00, 1.144e+00]
            ## Data/MC reqweighted in lap1_P and nTracks for systematic (Nov '21)
            #[2.99e-01, 4.06e-01, 7.50e-01, 8.10e-01, 9.70e-01, 1.122e+00]
        },
        "2018": {
            "values":
            ## Nominal values
            [0.358, 0.512, 0.827, 0.994, 1.110, 1.278]
            # Pre March 22 values
            # [3.40e-01, 4.75e-01, 7.92e-01, 9.38e-01, 1.055e+00, 1.220e+00]
            ## Data/MC reqweighted in lap1_P and nTracks for systematic (Nov '21)
            #[3.22e-01, 4.52e-01, 7.46e-01, 8.84e-01, 9.93e-01, 1.190e+00]
        },
    }

    configdict["constParams"] = [
        'Gammas_Bs2DsK',
        'deltaGammas_Bs2DsK',
        'DeltaMs_Bs2DsK',
        # 'tagEff_OS',
        # 'tagEff_SS',
        'average_OS',
        'average_SS',
        'var1_20152016',
        'var1_2017',
        'var1_2018',
        'var2_20152016',
        'var2_2017',
        'var2_2018',
        'var3_20152016',
        'var3_2017',
        'var3_2018',
        'var4_20152016',
        'var4_2017',
        'var4_2018',
        'var5_20152016',
        'var5_2017',
        'var5_2018',
        'var6_20152016',
        'var6_2017',
        'var6_2018',
        'dp0_mean_OS_20152016',
        'dp0_mean_OS_2017',
        'dp0_mean_OS_2018',
        'dp0_mean_SS_20152016',
        'dp0_mean_SS_2017',
        'dp0_mean_SS_2018',
        'dp1_mean_OS_20152016',
        'dp1_mean_OS_2017',
        'dp1_mean_OS_2018',
        'dp1_mean_SS_20152016',
        'dp1_mean_SS_2017',
        'dp1_mean_SS_2018',
        'p0_mean_OS_20152016',
        'p0_mean_OS_2017',
        'p0_mean_OS_2018',
        'p0_mean_SS_20152016',
        'p0_mean_SS_2017',
        'p0_mean_SS_2018',
        'p1_mean_OS_20152016',
        'p1_mean_OS_2017',
        'p1_mean_OS_2018',
        'p1_mean_SS_20152016',
        'p1_mean_SS_2017',
        'p1_mean_SS_2018',
        # 'aTagEff_OS_20152016',
        # 'aTagEff_OS_2017',
        # 'aTagEff_OS_2018',
        # 'aTagEff_SS_20152016',
        # 'aTagEff_SS_2017',
        # 'aTagEff_SS_2018',
        'aprod',
        'adet',
    ]

    # Add some name variants
    configdict["constParams"] += [
        c + "_" + l for c, l in itertools.product(configdict["constParams"],
                                                  configdict["Labels"])
    ]

    return configdict


if __name__ == '__main__':
    sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'Toys'))
    from configutils import main
    main(getconfig())
