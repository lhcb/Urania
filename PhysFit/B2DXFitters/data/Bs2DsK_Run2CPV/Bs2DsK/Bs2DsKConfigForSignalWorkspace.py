###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsKConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    import sys

    configdict["YearOfDataTaking"] = {"2015", "2016", "2017", "2018"}
    configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK", "KPiPi", "PiPiPi"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [1.61, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1"
    }

    # file name with paths to MC/data samples
    configdict["dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsK/config_Bs2DsK.txt"

    configdict["AdditionalVariables"] = {}

    hasRICH = "(lab1_hasRich==1&&lab3_hasRich==1&&lab4_hasRich==1&&lab5_hasRich==1)"
    isMuon = "lab1_isMuon==0&&lab3_isMuon==0&&lab4_isMuon==0&&lab5_isMuon==0"

    p = "lab1_P > 3000.0&&lab1_P < 650000.00"
    pt = "lab1_PT> 400.0&&lab1_PT<45000.0"
    nTracks = "nTracks>15.0&&nTracks<500.0"
    time = "(lab0_LifetimeFit_ctau[0] > 0.119917)&&(lab0_LifetimeFit_ctau[0] < 4.496887)"
    timeErr = "(lab0_LifetimeFit_ctauErr[0] > 0.002998)&&(lab0_LifetimeFit_ctauErr[0] < 0.029979)"
    bdtg = "(BDTGResponse_XGB_1 > 0.475000)&&(BDTGResponse_XGB_1 < 1.000000)"

    vetoD = "(fabs(lab2_MassHypo_KKPi_D - 1869.6) > 30. || lab3_PIDK_corr > 10.)"
    vetoLc = "(fabs(lab2_MassHypo_KKPi_Lambda - 2286.4) > 30. || (lab3_PIDK_corr - lab3_PIDp_corr > 5.))"
    vetoD0 = "lab34_MM < 1800."
    vetoKst = "(fabs(lab15_MM-892)>30.)"
    vetoD0_misID = "(fabs(sqrt(pow(sqrt(pow(139.6,2)+pow(lab1_P,2))+sqrt(pow(lab3_M,2)+pow(lab3_P,2)),2)-pow(lab1_PX+lab3_PX,2)-pow(lab1_PY+lab3_PY,2)-pow(lab1_PZ+lab3_PZ,2)) - 1864.83) > 30.)"

    vetoD_kpipi = "(fabs(lab2_MassHypo_KPiPi_D - 1869.6) > 30. || lab3_PIDK_corr > 20. || lab4_PIDK_corr < -10.)"
    vetoLc_kpipi = "(fabs(lab2_MassHypo_KPiPi_LambdaSup - 2286.4) > 30. || (lab3_PIDK_corr - lab3_PIDp_corr > 5.))"
    vetoD0_kpipi = "lab34_MM < 1750. && (fabs(lab15_MM-1864.83)>30.)"
    vetoKst_kpipi = "lab15_MM>1000."

    vetoD_pipipi = "fabs(lab135_MM-1869.58)>30."
    vetoD0_pipipi = "lab34_MM<1700. && lab45_MM<1700. && fabs(lab13_MM-1864.83)>30. && fabs(lab15_MM-1864.83)>30."
    vetoKst_pipipi = "lab13_MM>1000. && lab15_MM>1000."

    pid_phipi = "lab3_PIDK_corr > -2. && lab4_PIDK_corr > -2."
    pid_kstk = "lab3_PIDK_corr >  5. && lab4_PIDK_corr > -2."
    pid_nonres = "lab3_PIDK_corr >  5. && lab4_PIDK_corr >  5. && lab5_PIDK_corr < 10."
    pid_kpipi = "lab3_PIDK_corr > 10. && lab4_PIDK_corr <  5. && lab5_PIDK_corr < 5. && lab4_PIDp_corr < 10. && lab5_PIDp_corr < 10."
    pid_pipipi = "lab3_PIDK_corr < 2.0 && lab4_PIDK_corr <  2. && lab5_PIDK_corr < 2. && lab3_PIDp_corr < 5.  && lab4_PIDp_corr < 5. && lab5_PIDp_corr < 5."

    pidp = "lab5_PIDp_corr < 10."
    pid_bac = "lab1_PIDK_corr > 5."

    lab2_FDCHI2_0 = "lab2_FDCHI2_ORIVX > 0"
    lab2_FDCHI2_9 = "lab2_FDCHI2_ORIVX > 9"

    t = "&&"

    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "MC":
        lab2_FDCHI2_0 + t + hasRICH + t + isMuon + t + p + t + pt + t + nTracks
        + t + time + t + timeErr + t + bdtg + t + pid_bac,
        "MCID":
        True,
        "MCTRUEID":
        True,
        "BKGCAT":
        True,
        "DsHypo":
        True
    }
    configdict["AdditionalCuts"]["KKPi"] = {
        "MC":
        lab2_FDCHI2_0 + t + vetoD + t + vetoLc + t + vetoD0 + t + vetoKst + t +
        vetoD0_misID + t + pidp
    }
    configdict["AdditionalCuts"]["PhiPi"] = {"MC": pid_phipi}
    configdict["AdditionalCuts"]["KstK"] = {"MC": pid_kstk}
    configdict["AdditionalCuts"]["NonRes"] = {"MC": pid_nonres}
    configdict["AdditionalCuts"]["KPiPi"] = {
        "MC":
        lab2_FDCHI2_9 + t + pid_kpipi + t + vetoD_kpipi + t + vetoLc_kpipi + t
        + vetoD0_kpipi + t + vetoKst_kpipi + t + vetoD0_misID
    }
    configdict["AdditionalCuts"]["PiPiPi"] = {
        "MC":
        lab2_FDCHI2_9 + t + pid_pipipi + t + vetoD_pipipi + t + vetoD0_pipipi +
        t + vetoKst_pipipi
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    configdict["GlobalWeight"] = {
        "2015": {
            "NonRes": {
                "Down": 0.114182,
                "Up": 0.086149
            },
            "PhiPi": {
                "Down": 0.232224,
                "Up": 0.175209
            },
            "KstK": {
                "Down": 0.171514,
                "Up": 0.129405
            },
            "KPiPi": {
                "Down": 0.037552,
                "Up": 0.028332
            },
            "PiPiPi": {
                "Down": 0.087084,
                "Up": 0.065703
            }
        },
        "2016": {
            "NonRes": {
                "Down": 0.525233,
                "Up": 0.491690
            },
            "PhiPi": {
                "Down": 1.068220,
                "Up": 1.000000
            },
            "KstK": {
                "Down": 0.788958,
                "Up": 0.738573
            },
            "KPiPi": {
                "Down": 0.172735,
                "Up": 0.161704
            },
            "PiPiPi": {
                "Down": 0.400583,
                "Up": 0.375000
            }
        },
        "2017": {
            "NonRes": {
                "Down": 0.519122,
                "Up": 0.493204
            },
            "PhiPi": {
                "Down": 1.052550,
                "Up": 1.000000
            },
            "KstK": {
                "Down": 0.768464,
                "Up": 0.730097
            },
            "KPiPi": {
                "Down": 0.173313,
                "Up": 0.164660
            },
            "PiPiPi": {
                "Down": 0.365020,
                "Up": 0.346796
            }
        },
        "2018": {
            "NonRes": {
                "Down": 0.437200,
                "Up": 0.476014
            },
            "PhiPi": {
                "Down": 0.918461,
                "Up": 1.000000
            },
            "KstK": {
                "Down": 0.682020,
                "Up": 0.742568
            },
            "KPiPi": {
                "Down": 0.167557,
                "Up": 0.182432
            },
            "PiPiPi": {
                "Down": 0.350629,
                "Up": 0.381757
            }
        }
    }

    #weighting templates by PID eff/misID
    configdict["WeightingMassTemplates"] = {
        "RatioDataMC": {
            "2015": {
                "FileLabel": "#DataMC 2015",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio"
            },
            "2016": {
                "FileLabel": "#DataMC 2016",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio"
            },
            "2017": {
                "FileLabel": "#DataMC 2017",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio"
            },
            "2018": {
                "FileLabel": "#DataMC 2018",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio"
            }
        },
        "Shift": {
            "2015": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
            "2016": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
            "2017": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
            "2018": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
        }
    }

    #----------------------------------------------------------------------#
    ###                 FitShapes fitting settings
    #----------------------------------------------------------------------#

    happystar = "#lower[-0.65]{#scale[0.6]{*}}"

    configdict["pdfList"] = {}
    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsK"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsK"]["BeautyMass"][
        "IpatiaPlusJohnsonSU"] = {
            "Title": "B_{s}#rightarrow #D_{s}K",
            #     "Bins": 175,
            #     "Min": 5100.0,
            #     "Max": 5800.0,
            #     "Unit": "MeV/c^{2}",
            #     "mean": [5368.0, 5300, 5500],
            #     "sigmaI": [20.5, 10.0, 25.0],
            #     "sigmaJ": [13.3, 10.0, 20.0],
            #     "zeta": [0.0],  #1,0.0,5.0],
            #     "fb": [0.0],
            #     "l": [-1.6, -5.0, 0.0],
            #     "a1": [1.2, 0.5, 3.0],
            #     "a2": [3.4, 1.0, 5.0],
            #     "n1": [1.4, 0.8, 4.0],
            #     "n2": [1.8, 0.8, 5.0],
            #     "nu": [-0.3, -2.0, 2.0],
            #     "tau": [0.3, -2.0, 2.0],
            #     "fracI": [0.28, 0.01, 0.9]
            # }

            #     #Fit separate modes
            #     "Bins": 175,
            #     "Min": 5100.0,
            #     "Max": 5800.0,
            #     "Unit": "MeV/c^{2}",
            #     "mean": [5367.8256, 5365, 5370],
            #     "sigmaI": [20.6, 10.0, 25.0],
            #     "sigmaJ": [13.3, 10.0, 20.0],
            #     "zeta": [0.0],  #1,0.0,5.0],
            #     "fb": [0.0],
            #     "l": [-1.65, -5.0, 0.0],
            #     "a1": [1.18, 0.5, 3.0],
            #     "a2": [3.4, 1.0, 5.0],
            #     "n1": [1.38, 0.8, 4.0],
            #     "n2": [1.8, 0.8, 5.0],
            #     "nu": [-0.34, -2.0, 2.0],
            #     "tau": [0.32, 0.0, 2.0],
            #     "fracI": [0.26169]
            # }  #,0.2,0.5

            # BeautyMass range 5300-5800
            "Bins": 125,
            "Min": 5300.0,
            "Max": 5800.0,
            "Unit": "MeV/c^{2}",
            "mean": [5368.0, 5300, 5500],
            "sigmaI": [24.8, 15.0, 35.0],
            "sigmaJ": [12.9, 10.0, 20.0],
            "zeta": [0.0],  #1,0.0,5.0],
            "fb": [0.0],
            "l": [-1.7, -5.0, 0.0],
            "a1": [0.5, 0.1, 3.0],
            "a2": [2.9, 1.0, 5.0],
            "n1": [5.0],  #4.5, 0.8, 100.0
            "n2": [1.8, 0.8, 5.0],
            "nu": [-0.17, -2.0, 2.0],
            "tau": [0.3, 0.0, 2.0],
            "fracI": [0.15, 0.01, 0.9]  #[0.14828]
        }

    #     #All result
    #     "Bins": 125,
    #     "Min": 5300.0,
    #     "Max": 5800.0,
    #     "Unit": "MeV/c^{2}",
    #     "mean": [5367.8297],
    #     "sigmaI": [2.0385e+01],
    #     "sigmaJ": [1.3239e+01],
    #     "zeta": [0.0],  #1,0.0,5.0],
    #     "fb": [0.0],
    #     "l": [-1.6717],
    #     "a1": [1.2051],
    #     "a2": [3.3447],
    #     "n1": [1.3802],
    #     "n2": [1.8561],
    #     "nu": [-3.4359e-01],
    #     "tau": [3.2299e-01],
    #     "fracI": [0.26169, 0.2, 0.5]
    # }

    configdict["pdfList"]["Signal"]["Bs2DsK"]["CharmMass"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsK"]["CharmMass"][
        "IpatiaPlusJohnsonSU"] = {
            "Title": "B_{s}#rightarrow #D_{s}K",
            "Bins": 170,
            "Min": 1930.0,
            "Max": 2015.0,
            "Unit": "MeV/c^{2}",
            #     "mean": [1969.034, 1965.0, 1975.0],
            #     "sigmaI": [21.9, 1.0, 40.0],
            #     "sigmaJ": [6.3, 1.0, 14.0],
            #     "zeta": [0.0],
            #     "fb": [0.0],
            #     "l": [-1.1],
            #     "a1": [0.19, 0.05, 5.0],
            #     "a2": [0.38, 0.05, 5.0],
            #     "n1": [8.0, 0.0, 12.0],
            #     "n2": [8.0, 0.0, 12.0],
            #     "nu": [0.0, -2.0, 2.0],
            #     "tau": [0.45, -2.0, 2.0],
            #     "fracI": [0.16, 0.01, 0.99]
            # }

            # For KKPi
            "mean": [1968.8, 1968.0, 1971.0],
            "sigmaI": [24.4, 20.0, 40.0],
            "sigmaJ": [6.5, 4.0, 10.0],
            "zeta": [0.0],
            "fb": [0.0],
            "l": [-1.1],
            "a1": [0.19, 0.05, 2.0],
            "a2": [0.38, 0.05, 2.0],
            "n1": [6.0],
            "n2": [6.0],
            "nu": [0.0],
            "tau": [0.49, 0.0, 2.0],
            "fracI": [0.13, 0.01, 0.99]
        }

    #     # For KPiPi
    #     "mean": [1968.8, 1968.0, 1971.0],
    #     "sigmaI": [26.0, 20.0, 40.0],
    #     "sigmaJ": [7.5, 4.0, 12.0],
    #     "zeta": [0.0],
    #     "fb": [0.0],
    #     "l": [-1.1],
    #     "a1": [0.19, 0.05, 2.0],
    #     "a2": [0.38, 0.05, 2.0],
    #     "n1": [6.0],
    #     "n2": [6.0],
    #     "nu": [0.0],
    #     "tau": [0.49, 0.0, 2.0],
    #     "fracI": [0.13, 0.01, 0.99]
    # }

    #     # For PiPiPi
    #     "mean": [1968.8, 1968.0, 1971.0],
    #     "sigmaI": [30.0, 20.0, 40.0],
    #     "sigmaJ": [9.0, 4.0, 12.0],
    #     "zeta": [0.0],
    #     "fb": [0.0],
    #     "l": [-1.1],
    #     "a1": [0.19, 0.05, 2.0],
    #     "a2": [0.38, 0.05, 2.0],
    #     "n1": [6.0],
    #     "n2": [6.0],
    #     "nu": [0.0],
    #     "tau": [0.49, 0.0, 2.0],
    #     "fracI": [0.13, 0.01, 0.99]
    # }

    #     #Agnieszka
    #     "mean": [1968.49, 1950, 1990],
    #     "sigmaI": [26.0, 10.0, 50.0],
    #     "sigmaJ": [6.3, 1.0, 30.0],
    #     "zeta": [0.0],
    #     "fb": [0.0],
    #     "l": [-1.1, -5.0, 0.0],  #-5.0,0.0],
    #     "a1": [0.2, 0.01, 5.0],
    #     "a2": [2.0, 0.01, 5.0],
    #     "n1": [4.9, 0.1, 10.0],  #8182,0.01,8.0],
    #     "n2": [4.1, 0.1, 10.0],  #0.01,50.0],
    #     "nu": [0.0],  #15, -5.0, 0.0],
    #     "tau": [0.5, 0.0, 1.0],
    #     "fracI": [0.12, 0.05, 0.40]
    # }

    configdict["pdfList"]["Bs2DsPi"] = {}
    configdict["pdfList"]["Bs2DsPi"]["Bs2DsK"] = {}
    configdict["pdfList"]["Bs2DsPi"]["Bs2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsPi"]["Bs2DsK"]["BeautyMass"]["Ipatia"] = {
        "Title": "B_{s}#rightarrow #D_{s}K",
        "Bins": 125,
        "Min": 5300.0,
        "Max": 5800.0,
        "Unit": "MeV/c^{2}",
        "mean": [5.3987e+03, 5375, 5425],
        "sigma": [27.3, 17.5, 30.0],
        "zeta": [0.0],  #1,0.0,5.0],
        "fb": [0.03],  #,0.05
        "l": [-1.8, -15.0, -0.1],  #,-15.0,0.0
        "a1": [1.18, 0.5, 3.0],
        "a2": [1.4, 0.5, 3.0],
        "n1": [1.2, 0.5, 5.0],  #,0.1,15.0
        "n2": [2.7, 0.5, 5.0]
    }

    #     "mean": [5398.40, 5375, 5425],
    #     "sigma": [2.4743e+01, 17.5, 30.0],
    #     "zeta": [0.0],  #1,0.0,5.0],
    #     "fb": [3.1322e-02, 0.01, 0.05],  #,0.05
    #     "l": [-2.3],  #,-15.0,0.0
    #     "a1": [1.3251, 0.5, 3.0],
    #     "a2": [1.2986, 0.0, 5.0],
    #     "n1": [1.0],  #,0.1,15.0
    #     "n2": [2.8337, 0.1, 5.0]
    # }

    #     #20152016 Result
    #     "mean": [5398.8],
    #     "sigma": [24.776],
    #     "zeta": [0.0],
    #     "fb": [3.1756e-02],
    #     "l": [-2.3],
    #     "a1": [1.3044],
    #     "a2": [1.2489],
    #     "n1": [1.0],
    #     "n2": [3.2284]
    # }

    #     # 20172018 Result
    #     "mean": [5.3987e+03, 5375, 5425],
    #     "sigma": [2.7268e+01],
    #     "zeta": [0.0],
    #     "fb": [3.0e-02],
    #     "l": [-1.8549],
    #     "a1": [1.1734],
    #     "a2": [1.3483],
    #     "n1": [1.1878],
    #     "n2": [2.7181]
    # }

    configdict["pdfList"]["Bs2DsPi"]["Bs2DsK"]["BeautyMass"][
        "IpatiaPlusJohnsonSU"] = {
            "Title": "B_{s}#rightarrow #D_{s}#pi",
            "Bins": 125,
            "Min": 5300.0,
            "Max": 5800.0,
            "Unit": "MeV/c^{2}",
            "mean": [5.413e+03, 5375, 5425],
            "sigmaI": [66.0, 1.0, 100.0],
            "sigmaJ": [24.0, 1.0, 100.0],
            "zeta": [0.0],
            "fb": [0.0],
            "l": [-1.1],  #,-10,-.01
            "a1": [0.75, 0.05, 10.0],
            "a2": [0.09, 0.05, 10.0],
            "n1": [1.2, 0.0, 12.0],
            "n2": [4.2, 0.0, 12.0],
            "nu": [1.4, -2.0, 2.0],
            "tau": [0.51, 0.0, 2.0],
            "fracI": [0.527]
        }  #,0.01,0.99

    configdict["pdfList"]["Bs2DsPi"]["Bs2DsK"]["BeautyMass"][
        "DoubleCrystalBall"] = {
            "Title": "B_{s}#rightarrow #D_{s}K",
            "Bins": 175,
            "Min": 5100.0,
            "Max": 5800.0,
            "Unit": "MeV/c^{2}",
            "mean": [5410.0, 5300, 5500],
            "sameMean": True,
            "sigma1": [18.8, 10.0, 28.0],
            "sigma2": [18.8, 10.0, 50.0],
            "alpha1": [1.2, 0.0, 10.0],
            "alpha2": [3.4, 0.0, 5.0],
            "n1": [1.4, 0.8, 35.0],
            "n2": [1.8, 0.8, 35.0],
            "frac": [0.5]
        }  #,0.0,1.0

    configdict["pdfList"]["Bs2DsstPi"] = {}
    configdict["pdfList"]["Bs2DsstPi"]["Bs2DsK"] = {}
    configdict["pdfList"]["Bs2DsstPi"]["Bs2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsstPi"]["Bs2DsK"]["BeautyMass"]["HORNSdini"] = {
        "Title": "B_{s}#rightarrowD_{s}" + happystar + "#pi",
        "Bins": 175,
        "Min": 5100.0,
        "Max": 5800.0,
        "Unit": "MeV/c^{2}",
        "a": [4700, 3000, 5500],
        "b": [5300, 5000, 6000],
        "csi": [5.0, 0.0, 10.0],
        "shift": [0.0],
        "sigma": [50.0, 10.0, 80.0],
        "R": [3.6, 0.5, 15.0],
        "frac": [0.84, 0.0, 1.0]
    }

    configdict["pdfList"]["Bs2DsstPi"]["Bs2DsK"]["BeautyMass"]["Ipatia"] = {
        "Title": "B_{s}#rightarrowD_{s}" + happystar + "#pi",
        "Bins": 100,
        "Min": 5300.0,
        "Max": 5800.0,
        "Unit": "MeV/c^{2}",
        "mean": [5288],  #.0,5250,5300
        "sigma": [65.0, 10.0, 90.0],
        "zeta": [0.0],  #1,0.0,5.0],
        "fb": [0.0],
        "l": [-2.2],  #,-15.0,0.0
        "a1": [1.0],  #,0.0,5.0
        "a2": [0.9, 0.1, 3.0],
        "n1": [1.0],  #,0.8,15.0
        "n2": [2.8, 0.8, 10.0]
    }

    configdict["pdfList"]["Bs2DsstPi"]["Bs2DsK"]["BeautyMass"][
        "CrystalBall"] = {
            "Title": "B_{s}#rightarrowD_{s}" + happystar + "#pi",
            "Bins": 175,
            "Min": 5300.0,
            "Max": 5800.0,
            "Unit": "MeV/c^{2}",
            "mean": [5300.0],  #,5250,5350
            "sigma": [25.0, 10.0, 100.0],
            "alpha": [4.0, 0.0, 80.0],
            "n": [8.0, 0.0, 25.0]
        }
    # "alpha"      : [1.1,0.0,25.0],
    # "n"          : [1.0,-25.0,0.0]}

    configdict["pdfList"]["Bs2DsRho"] = {}
    configdict["pdfList"]["Bs2DsRho"]["Bs2DsK"] = {}
    configdict["pdfList"]["Bs2DsRho"]["Bs2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsRho"]["Bs2DsK"]["BeautyMass"]["HORNSdini"] = {
        "Title": "B_{s}#rightarrowD_{s}#rho",
        "Bins": 175,
        "Min": 5100.0,
        "Max": 5800.0,
        "Unit": "MeV/c^{2}",
        "a": [3800, 3000, 5500],
        "b": [5400, 5000, 6000],
        "csi": [1.5, 0.0, 10.0],
        "shift": [0.0],
        "sigma": [20.0, 10.0, 80.0],
        "R": [8.0, 0.5, 15.0],
        "frac": [0.95, 0.0, 1.0]
    }

    configdict["pdfList"]["Bs2DsRho"]["Bs2DsK"]["BeautyMass"][
        "Exponential"] = {
            "Title": "B_{s}#rightarrowD_{s}#rho",
            "Bins": 175,
            "Min": 5300.0,
            "Max": 5800.0,
            "Unit": "MeV/c^{2}",
            "cB": [-0.012, -0.5, 0]
        }

    configdict["pdfList"]["Bs2DsRho"]["Bs2DsK"]["BeautyMass"][
        "ExponentialPlusConstant"] = {
            "Title": "B_{s}#rightarrowD_{s}#rho",
            "Bins": 175,
            "Min": 5300.0,
            "Max": 5500.0,
            "Unit": "MeV/c^{2}",
            "cB": [-0.05, -0.5, 0],
            "fracExpo": [0.5, 0.0, 1.0]
        }

    configdict["pdfList"]["Bs2DsRho"]["Bs2DsK"]["BeautyMass"][
        "DoubleExponential"] = {
            "Title": "B_{s}#rightarrowD_{s}#rho",
            "Bins": 100,
            "Min": 5300.0,
            "Max": 5800.0,
            "Unit": "MeV/c^{2}",
            "cB1": [-0.007, -0.5, 0],
            "cB2": [-0.03, -0.5, 0],
            "frac": [0.6, 0.5, 1.0]
        }

    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Bs2DsK_",
        "Bd2DK": "dataSetMC_Bd2DK_",
        "Bd2DRho": "dataSetMC_Bd2DRho_",
        "Bd2DstPi": "dataSetMC_Bd2DstPi_",
        "Bd2DKst": "dataSetMC_Bd2DKst_",
        "Bs2DsPi": "dataSetMC_Bs2DsPi_",
        "Bs2DsstPi": "dataSetMC_Bs2DsstPi_",
        "Bs2DsRho": "dataSetMC_Bs2DsRho_",
        "Comb": "dataSetCombPi_"
    }

    #Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {
        "Bs2DsK": "m(D_{s}^{\mp}K^{\pm}) [MeV/c^{2}]",
        "Bs2DsPi": "m(D_{s}^{-}K^{+}) [MeV/c^{2}]",
        "Bs2DsstPi": "m(D_{s}^{-}K^{+}) [MeV/c^{2}]",
    }
    configdict["AxisTitle"]["CharmMass"] = {
        "Bs2DsK": "m(h^{\mp}h^{\pm}h^{\mp}) [MeV/c^{2}] ",
        "Bs2DsPi": "m(h^{-}h^{+}h^{-}) [MeV/c^{2}]",
        "Bs2DsstPi": "m(h^{-}h^{+}h^{-}) [MeV/c^{2}]"
    }

    return configdict
