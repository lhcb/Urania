###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import itertools


def getconfig():
    from Bs2DsKConfigForSignalWorkspace import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    from math import pi

    # PHYSICAL PARAMETERS
    configdict["Tau_H"] = 1.661000  #1.660999991
    configdict["Tau_L"] = 1.405000  #1.404999988
    configdict["Gamma_H"] = 1.0 / configdict["Tau_H"]
    configdict["Gamma_L"] = 1.0 / configdict["Tau_L"]
    configdict["Gammas"] = (
        configdict["Gamma_H"] + configdict["Gamma_L"]) / 2.0
    configdict["Tau"] = 1.52231245
    configdict["DeltaGammas"] = (configdict["Gamma_H"] - configdict["Gamma_L"])

    configdict["CPlimit"] = {"upper": 4.0, "lower": -4.0}

    configdict["DeltaMs"] = 17.8
    configdict["cos"] = 0.0
    configdict["sin"] = 0.0
    configdict["sinh"] = 0.0

    configdict["Bins"] = 1000

    configdict["Resolution"] = {
        "2015": {
            "scaleFactor": {
                "p0": 0.012540524660857423,
                "p1": 0.9501071535400626,
                "p2": 0.0
            },
            "meanBias": 0.00
        },
        "2016": {
            "scaleFactor": {
                "p0": 0.012540524660857423,
                "p1": 0.9501071535400626,
                "p2": 0.0
            },
            "meanBias": 0.00
        },
        "20152016": {
            "scaleFactor": {
                "p0": 0.012540524660857423,
                "p1": 0.9501071535400626,
                "p2": 0.0
            },
            "meanBias": 0.00
        },
        "2017": {
            "scaleFactor": {
                "p0": 0.00988835748159421,
                "p1": 0.9547832846636067,
                "p2": 0.0
            },
            "meanBias": 0.0
        },
        "2018": {
            "scaleFactor": {
                "p0": 0.009817949897958371,
                "p1": 0.948081567596144,
                "p2": 0.0
            },
            "meanBias": 0.0
        },
        "run2": {
            "scaleFactor": {
                "p0": 0.009817949897958371,
                "p1": 0.948081567596144,
                "p2": 0.0
            },
            "meanBias": 0.0
        }
    }

    configdict["Acceptance"] = {
        "knots": [0.50, 1.0, 1.5, 2.0, 3.0, 12.0],
        "2015": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
        "2016": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
        "20152016": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
        "2017": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
        "2018": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
        "run2": {
            "values": [
                3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00,
                1.1872e+00
            ]
        },
    }

    configdict["constParams"] = []

    return configdict
