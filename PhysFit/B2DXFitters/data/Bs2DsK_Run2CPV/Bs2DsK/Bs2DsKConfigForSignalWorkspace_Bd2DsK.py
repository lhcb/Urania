###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsKConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    import sys

    configdict["Decay"] = "Bd2DsK"
    configdict["YearOfDataTaking"] = {"2015", "2016", "2017", "2018"}
    configdict["CharmModes"] = {"KKPi"}

    # file name with paths to MC/data samples
    configdict[
        "dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsK/config_Bs2DsK_forBd2DsK.txt"

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [1.61, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["AdditionalVariables"] = {}

    p = "lab1_P > 3000.0&&lab1_P < 650000.00"
    pt = "lab1_PT> 400.0&&lab1_PT<45000.0"
    nTracks = "nTracks>15.0&&nTracks<500.0"
    time = "(lab0_LifetimeFit_ctau[0] > 0.119917)&&(lab0_LifetimeFit_ctau[0] < 4.496887)"
    timeErr = "(lab0_LifetimeFit_ctauErr[0] > 0.002998)&&(lab0_LifetimeFit_ctauErr[0] < 0.029979)"
    bdtg = "(BDTGResponse_XGB_1 > 0.475000)&&(BDTGResponse_XGB_1 < 1.000000)"

    vetoD0 = "lab34_MM < 1800."
    vetoKst = "(fabs(lab15_MM-892)>30.)"
    vetoD0_misID = "(fabs(sqrt(pow(sqrt(pow(139.6,2)+pow(lab1_P,2))+sqrt(pow(lab3_M,2)+pow(lab3_P,2)),2)-pow(lab1_PX+lab3_PX,2)-pow(lab1_PY+lab3_PY,2)-pow(lab1_PZ+lab3_PZ,2)) - 1864.83) > 30.)"

    lab2_FDCHI2_0 = "lab2_FDCHI2_ORIVX > 0"

    t = "&&"

    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "MC":
        lab2_FDCHI2_0 + t + p + t + pt + t + nTracks + t + time + t + timeErr +
        t + bdtg,
        "MCID":
        True,
        "MCTRUEID":
        True,
        "BKGCAT":
        True,
        "DsHypo":
        True
    }
    configdict["AdditionalCuts"]["KKPi"] = {
        "MC": lab2_FDCHI2_0 + t + vetoD0 + t + vetoKst + t + vetoD0_misID
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    #weighting templates by PID eff/misID
    #weighting templates by PID eff/misID
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2015": {
                "FileLabel":
                "#PIDK Kaon 2015",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "K_DLLK > 5 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All"
            },
            "2016": {
                "FileLabel":
                "#PIDK Kaon 2016",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "K_DLLK > 5 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All"
            },
            "2017": {
                "FileLabel":
                "#PIDK Kaon 2017",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "K_DLLK > 5 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All"
            },
            "2018": {
                "FileLabel":
                "#PIDK Kaon 2018",
                "Var": ["nTracks", "lab1_P"],
                "HistName":
                "K_DLLK > 5 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All"
            }
        },
        "RatioDataMC": {
            "2015": {
                "FileLabel": "#DataMC 2015",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio"
            },
            "2016": {
                "FileLabel": "#DataMC 2016",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio"
            },
            "2017": {
                "FileLabel": "#DataMC 2017",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio"
            },
            "2018": {
                "FileLabel": "#DataMC 2018",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio"
            }
        },
        "Shift": {
            "2015": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
            "2016": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
            "2017": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
            "2018": {
                "BeautyMass": -1.5,
                "CharmMass": 0.0
            },
        }
    }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               FitShapes fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    configdict["pdfList"] = {}
    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bd2DsK"] = {}
    configdict["pdfList"]["Signal"]["Bd2DsK"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bd2DsK"]["BeautyMass"][
        "DoubleExponential"] = {
            "Title": "B_{d}#rightarrow #D_{s}K",
            "Bins": 100,
            "Min": 5300.0,
            "Max": 5800.0,
            "Unit": "MeV/c^{2}",
            "cB1": [-0.1, -0.5, 0],
            "cB2": [-0.01, -0.1, 0],
            "frac": [0.7, 0.0, 1.0]
        }
    configdict["pdfList"]["Signal"]["Bd2DsK"]["BeautyMass"]["Exponential"] = {
        "Title": "B_{d}#rightarrow #D_{s}K",
        "Bins": 100,
        "Min": 5300.0,
        "Max": 5800.0,
        "Unit": "MeV/c^{2}",
        "cB": [-0.001, -0.1, 0]
    }
    configdict["pdfList"]["Signal"]["Bd2DsK"]["BeautyMass"]["Ipatia"] = {
        "Title": "B_{d}#rightarrow #D_{s}K",
        "Bins": 100,
        "Min": 5300.0,
        "Max": 5800.0,
        "Unit": "MeV/c^{2}",
        "mean": [5280.7],
        "sigma": [15.0, 1.0, 80.0],
        "zeta": [0.0],  #1,0.0,5.0],
        "fb": [0.0],
        "l": [-3.0, -5.0, -0.5],  #,-15.0,0.0
        "a1": [1.0],  #,0.0,5.0
        "a2": [3.5, 0.0, 6.0],
        "n1": [1.0],  #,0.8,15.0
        "n2": [2.0, 0.8, 15.0]
    }

    configdict["dataSetPrefix"] = {"Signal": "dataSetMC_Bd2DsK_"}

    #Axes titles
    configdict["AxisTitle"] = {
        "BeautyMass": {
            "Bs2DsK": "m(D_{s}^{\mp}K^{\pm}) [MeV/c^{2}]",
            "Bd2DsK": "m(D_{s}^{\mp}K^{\pm}) [MeV/c^{2}]"
        },
        "CharmMass": {
            "Bs2DsK": "m(K^{-}K^{+}#pi^{-}) [MeV/c^{2}]",
            "Bd2DsK": "m(K^{-}K^{+}#pi^{-}) [MeV/c^{2}]"
        }
    }

    return configdict
