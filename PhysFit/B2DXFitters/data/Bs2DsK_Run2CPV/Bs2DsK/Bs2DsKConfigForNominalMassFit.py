###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bs2DsK"
    configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK", "KPiPi", "PiPiPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi", "Bd2DK", "Lb2LcK", "Lb2LcPi", "Bs2DsPi", "Bs2DsRho",
        "Bs2DsstPi", "Bd2DsK", "Lb2Dsp", "Lb2Dsstp"
    ]
    # year of data taking
    configdict["YearOfDataTaking"] = {"2015", "2016", "2017", "2018"}

    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        }
    }
    # file name with paths to MC/data samples
    configdict["dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsK/config_Bs2DsK.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsK_Nominal",
        "Extension": "pdf"
    }

    configdict["MoreVariables"] = True

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [1.61, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 500.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1"
    }
    configdict["BasicVariables"]["TagDecOS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "OS_Combination_DEC"
    }
    configdict["BasicVariables"]["TagDecSS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "lab0_SSKaonLatest_TAGDEC"
    }
    configdict["BasicVariables"]["MistagOS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "OS_Combination_ETA"
    }
    configdict["BasicVariables"]["MistagSS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "lab0_SSKaonLatest_TAGETA"
    }

    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    # additional variables in data sets
    if configdict["MoreVariables"] == True:
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSCharm_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSElectronLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSKaonLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSMuonLatest_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSVtxCh_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSPion_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSProton_TAGDEC"
        }
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSCharm_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSElectronLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSKaonLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSMuonLatest_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSVtxCh_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSPion_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSProton_TAGETA"
        }
        configdict["AdditionalVariables"]["lab0_P"] = {
            "Range": [0.0, 1600000.0],
            "InputName": "lab0_P"
        }
        configdict["AdditionalVariables"]["lab0_PT"] = {
            "Range": [0.0, 40000.0],
            "InputName": "lab0_PT"
        }
        configdict["AdditionalVariables"]["lab0_ETA"] = {
            "Range": [0.0, 6.0],
            "InputName": "lab0_ETA"
        }
        configdict["AdditionalVariables"]["nCandidate"] = {
            "Range": [0.0, 500.0],
            "InputName": "nCandidate"
        }
        configdict["AdditionalVariables"]["totCandidates"] = {
            "Range": [0.0, 500.0],
            "InputName": "totCandidates"
        }
        configdict["AdditionalVariables"]["runNumber"] = {
            "Range": [0.0, 1000000.0],
            "InputName": "runNumber"
        }
        configdict["AdditionalVariables"]["eventNumber"] = {
            "Range": [0.0, 10000000000.0],
            "InputName": "eventNumber"
        }
        configdict["AdditionalVariables"]["BDTGResponse_3"] = {
            "Range": [-1.0, 1.0],
            "InputName": "BDTGResponse_3"
        }
        configdict["AdditionalVariables"]["nSPDHits"] = {
            "Range": [0.0, 1400.0],
            "InputName": "nSPDHits"
        }
        configdict["AdditionalVariables"]["nTracksLinear"] = {
            "Range": [15.0, 1000.0],
            "InputName": "clone_nTracks"
        }

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -----     Bs signal shapes     -----   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "20152016": {
            "All": 5367.51
        },
        "2017": {
            "All": 5367.51
        },
        "2018": {
            "All": 5367.51
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "20152016": {
            "phipi": 20.73546,
            "kstk": 19.7633,
            "nonres": 21.87735,
            "kpipi": 21.33548,
            "pipipi": 19.97077
        },
        "2017": {
            "phipi": 19.62015,
            "kstk": 20.088,
            "nonres": 19.82998,
            "kpipi": 20.66482,
            "pipipi": 20.03069
        },
        "2018": {
            "phipi": 22.12797,
            "kstk": 20.13534,
            "nonres": 14.40412,
            "kpipi": 20.02122,
            "pipipi": 19.39511
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "20152016": {
            "phipi": 13.35251,
            "kstk": 13.21325,
            "nonres": 13.29894,
            "kpipi": 13.42541,
            "pipipi": 14.13975
        },
        "2017": {
            "phipi": 13.14443,
            "kstk": 13.22707,
            "nonres": 13.02213,
            "kpipi": 12.90643,
            "pipipi": 13.5707
        },
        "2018": {
            "phipi": 13.17188,
            "kstk": 13.13044,
            "nonres": 13.45006,
            "kpipi": 13.23743,
            "pipipi": 13.99472
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "phipi": 1.20588,
            "kstk": 1.2978,
            "nonres": 1.09395,
            "kpipi": 1.02773,
            "pipipi": 1.2706
        },
        "2017": {
            "phipi": 1.25632,
            "kstk": 1.2651,
            "nonres": 1.26265,
            "kpipi": 1.21297,
            "pipipi": 1.21289
        },
        "2018": {
            "phipi": 1.04208,
            "kstk": 1.26697,
            "nonres": 1.511,
            "kpipi": 1.23771,
            "pipipi": 1.2424
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "phipi": 3.13529,
            "kstk": 3.29145,
            "nonres": 4.85329,
            "kpipi": 4.14649,
            "pipipi": 3.17663
        },
        "2017": {
            "phipi": 2.82556,
            "kstk": 3.97698,
            "nonres": 2.90435,
            "kpipi": 3.60338,
            "pipipi": 3.28651
        },
        "2018": {
            "phipi": 3.91792,
            "kstk": 3.69238,
            "nonres": 2.06492,
            "kpipi": 4.21727,
            "pipipi": 3.16893
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "20152016": {
            "phipi": 1.37828,
            "kstk": 1.3537,
            "nonres": 1.37028,
            "kpipi": 1.51461,
            "pipipi": 1.28609
        },
        "2017": {
            "phipi": 1.35482,
            "kstk": 1.29922,
            "nonres": 1.34016,
            "kpipi": 1.56921,
            "pipipi": 1.525
        },
        "2018": {
            "phipi": 1.45496,
            "kstk": 1.33787,
            "nonres": 1.35409,
            "kpipi": 1.41836,
            "pipipi": 1.44162
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "phipi": 1.94936,
            "kstk": 1.78552,
            "nonres": 1.16829,
            "kpipi": 1.29876,
            "pipipi": 1.80314
        },
        "2017": {
            "phipi": 2.10636,
            "kstk": 1.44037,
            "nonres": 2.13902,
            "kpipi": 2.05324,
            "pipipi": 1.99855
        },
        "2018": {
            "phipi": 1.72882,
            "kstk": 1.64603,
            "nonres": 2.37116,
            "kpipi": 1.23429,
            "pipipi": 1.87184
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "phipi": -1.6621,
            "kstk": -1.82136,
            "nonres": -1.47225,
            "kpipi": -1.52389,
            "pipipi": -1.74565
        },
        "2017": {
            "phipi": -1.81905,
            "kstk": -1.63608,
            "nonres": -1.7694,
            "kpipi": -1.89095,
            "pipipi": -1.89278
        },
        "2018": {
            "phipi": -1.47274,
            "kstk": -1.71279,
            "nonres": -4.99997,
            "kpipi": -1.76636,
            "pipipi": -1.81751
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2017": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2017": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "20152016": {
            "phipi": -0.30099,
            "kstk": -0.2964,
            "nonres": -0.36169,
            "kpipi": -0.46055,
            "pipipi": -0.3198
        },
        "2017": {
            "phipi": -0.29859,
            "kstk": -0.38443,
            "nonres": -0.37706,
            "kpipi": -0.45701,
            "pipipi": -0.42401
        },
        "2018": {
            "phipi": -0.3528,
            "kstk": -0.36079,
            "nonres": -0.13944,
            "kpipi": -0.42836,
            "pipipi": -0.32853
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "20152016": {
            "phipi": 0.32924,
            "kstk": 0.31484,
            "nonres": 0.29687,
            "kpipi": 0.28946,
            "pipipi": 0.37558
        },
        "2017": {
            "phipi": 0.32691,
            "kstk": 0.32638,
            "nonres": 0.3323,
            "kpipi": 0.32955,
            "pipipi": 0.35867
        },
        "2018": {
            "phipi": 0.29624,
            "kstk": 0.32074,
            "nonres": 0.3795,
            "kpipi": 0.35045,
            "pipipi": 0.41165
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {
            "All": 0.25451
        },
        "Fixed": True
    }  # 51005800: 0.26169

    #   ------------------------------------   #
    #   -----     Ds signal shapes     -----   #
    #   ------------------------------------   #

    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["CharmMass"]["mean"] = {
        "20152016": {
            "All": 1968.7
        },
        "2017": {
            "All": 1968.7
        },
        "2018": {
            "All": 1968.7
        },
        "Fixed": False
    }
    configdict["SignalShape"]["CharmMass"]["sigmaI"] = {
        "20152016": {
            "phipi": 27.1781,
            "kstk": 20.33142,
            "nonres": 25.49292,
            "kpipi": 27.04672,
            "pipipi": 30.22234
        },
        "2017": {
            "phipi": 25.11736,
            "kstk": 28.44072,
            "nonres": 25.92218,
            "kpipi": 26.38575,
            "pipipi": 30.43684
        },
        "2018": {
            "phipi": 25.64772,
            "kstk": 29.00566,
            "nonres": 23.68577,
            "kpipi": 28.40836,
            "pipipi": 32.36752
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["sigmaJ"] = {
        "20152016": {
            "phipi": 6.418,
            "kstk": 6.52792,
            "nonres": 6.33897,
            "kpipi": 7.97396,
            "pipipi": 9.21019
        },
        "2017": {
            "phipi": 6.12951,
            "kstk": 6.4656,
            "nonres": 6.16808,
            "kpipi": 7.6163,
            "pipipi": 9.05661
        },
        "2018": {
            "phipi": 6.18741,
            "kstk": 6.40526,
            "nonres": 6.14705,
            "kpipi": 7.87079,
            "pipipi": 9.42357
        },
        "Fixed": False
    }
    configdict["SignalShape"]["CharmMass"]["a1"] = {
        "20152016": {
            "phipi": 0.18658,
            "kstk": 0.11823,
            "nonres": 0.16212,
            "kpipi": 0.14479,
            "pipipi": 0.20262
        },
        "2017": {
            "phipi": 0.21247,
            "kstk": 0.16689,
            "nonres": 0.18402,
            "kpipi": 0.17108,
            "pipipi": 0.18387
        },
        "2018": {
            "phipi": 0.21118,
            "kstk": 0.17154,
            "nonres": 0.19635,
            "kpipi": 0.14486,
            "pipipi": 0.1191
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["a2"] = {
        "20152016": {
            "phipi": 1.65989,
            "kstk": 0.21224,
            "nonres": 0.33529,
            "kpipi": 0.328,
            "pipipi": 0.59629
        },
        "2017": {
            "phipi": 0.65397,
            "kstk": 0.64131,
            "nonres": 0.42568,
            "kpipi": 0.413,
            "pipipi": 0.64319
        },
        "2018": {
            "phipi": 0.65339,
            "kstk": 0.64207,
            "nonres": 0.47013,
            "kpipi": 0.32991,
            "pipipi": 0.31224
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["n1"] = {
        "20152016": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "2017": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "2018": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["n2"] = {
        "20152016": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "2017": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "2018": {
            "phipi": 6.0,
            "kstk": 6.0,
            "nonres": 6.0,
            "kpipi": 6.0,
            "pipipi": 6.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["l"] = {
        "20152016": {
            "phipi": -1.1,
            "kstk": -1.1,
            "nonres": -1.1,
            "kpipi": -1.1,
            "pipipi": -1.1
        },
        "2017": {
            "phipi": -1.1,
            "kstk": -1.1,
            "nonres": -1.1,
            "kpipi": -1.1,
            "pipipi": -1.1
        },
        "2018": {
            "phipi": -1.1,
            "kstk": -1.1,
            "nonres": -1.1,
            "kpipi": -1.1,
            "pipipi": -1.1
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["fb"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2017": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["zeta"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2017": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["nu"] = {
        "20152016": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2017": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "2018": {
            "phipi": 0.0,
            "kstk": 0.0,
            "nonres": 0.0,
            "kpipi": 0.0,
            "pipipi": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["tau"] = {
        "20152016": {
            "phipi": 0.50871,
            "kstk": 0.44742,
            "nonres": 0.44931,
            "kpipi": 0.48069,
            "pipipi": 0.3969
        },
        "2017": {
            "phipi": 0.46257,
            "kstk": 0.47264,
            "nonres": 0.43898,
            "kpipi": 0.40576,
            "pipipi": 0.39679
        },
        "2018": {
            "phipi": 0.48452,
            "kstk": 0.4497,
            "nonres": 0.43685,
            "kpipi": 0.48123,
            "pipipi": 0.46551
        },
        "Fixed": True
    }
    configdict["SignalShape"]["CharmMass"]["fracI"] = {
        "20152016": {
            "phipi": 0.10768,
            "kstk": 0.06776,
            "nonres": 0.09864,
            "kpipi": 0.12241,
            "pipipi": 0.28355
        },
        "2017": {
            "phipi": 0.14496,
            "kstk": 0.06516,
            "nonres": 0.11191,
            "kpipi": 0.15823,
            "pipipi": 0.2559
        },
        "2018": {
            "phipi": 0.13286,
            "kstk": 0.07042,
            "nonres": 0.13813,
            "kpipi": 0.11261,
            "pipipi": 0.15006
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    # combinatorial background
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    # configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
    #     "20152016": {
    #         "PhiPi": [-1.0960e-02, -0.08, 0.0],
    #         "KstK": [-9.0653e-03, -0.05, 0.0],
    #         "NonRes": [-9.2536e-03, -0.05, 0.0],
    #         # "KPiPi": [-8.7816e-04, -0.05, 0.0],
    #         # "PiPiPi": [-1.3254e-03, -0.05, 0.0]
    #         "KPiPi": [-5.1323e-04, -0.05, 0.0],
    #         "PiPiPi": [-5.3280e-04, -0.05, 0.0]
    #     },
    #     "2017": {
    #         "PhiPi": [-1.0960e-02, -0.08, 0.0],
    #         "KstK": [-9.0653e-03, -0.05, 0.0],
    #         "NonRes": [-9.2536e-03, -0.05, 0.0],
    #         # "KPiPi": [-8.7816e-04, -0.05, 0.0],
    #         # "PiPiPi": [-1.3254e-03, -0.05, 0.0]
    #         "KPiPi": [-5.1323e-04, -0.05, 0.0],
    #         "PiPiPi": [-5.3280e-04, -0.05, 0.0]
    #     },
    #     "2018": {
    #         "PhiPi": [-1.0960e-02, -0.08, 0.0],
    #         "KstK": [-9.0653e-03, -0.05, 0.0],
    #         "NonRes": [-9.2536e-03, -0.05, 0.0],
    #         # "KPiPi": [-8.7816e-04, -0.05, 0.0],
    #         # "PiPiPi": [-1.3254e-03, -0.05, 0.0]
    #         "KPiPi": [-5.1323e-04, -0.05, 0.0],
    #         "PiPiPi": [-5.3280e-04, -0.05, 0.0]
    #     },
    #     "Fixed": False
    # }
    # configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
    #     "20152016": {
    #         "PhiPi": -5.2031e-04,
    #         "KstK": -6.2061e-04,
    #         "NonRes": -4.5085e-04,
    #         "KPiPi": 0.0,
    #         "PiPiPi": 0.0
    #     },
    #     "2017": {
    #         "PhiPi": -5.2031e-04,
    #         "KstK": -6.2061e-04,
    #         "NonRes": -4.5085e-04,
    #         "KPiPi": 0.0,
    #         "PiPiPi": 0.0
    #     },
    #     "2018": {
    #         "PhiPi": -5.2031e-04,
    #         "KstK": -6.2061e-04,
    #         "NonRes": -4.5085e-04,
    #         "KPiPi": 0.0,
    #         "PiPiPi": 0.0
    #     },
    #     "Fixed": True
    # }
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "20152016": {
            "PhiPi": [-4.1294e-03, -0.08, 0.0],
            "KstK": [-3.1939e-03, -0.05, 0.0],
            "NonRes": [-4.9422e-03, -0.05, 0.0],
            "KPiPi": [-5.1307e-04, -0.05, 0.0],
            "PiPiPi": [-5.3280e-04, -0.05, 0.0]
        },
        "2017": {
            "PhiPi": [-4.1294e-03, -0.08, 0.0],
            "KstK": [-3.1939e-03, -0.05, 0.0],
            "NonRes": [-4.9422e-03, -0.05, 0.0],
            "KPiPi": [-5.1307e-04, -0.05, 0.0],
            "PiPiPi": [-5.3280e-04, -0.05, 0.0]
        },
        "2018": {
            "PhiPi": [-4.1294e-03, -0.08, 0.0],
            "KstK": [-3.1939e-03, -0.05, 0.0],
            "NonRes": [-4.9422e-03, -0.05, 0.0],
            "KPiPi": [-5.1307e-04, -0.05, 0.0],
            "PiPiPi": [-5.3280e-04, -0.05, 0.0]
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "20152016": {
            "PhiPi": 0.0,
            "KstK": 0.0,
            "NonRes": 0.0,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2017": {
            "PhiPi": 0.0,
            "KstK": 0.0,
            "NonRes": 0.0,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2018": {
            "PhiPi": 0.0,
            "KstK": 0.0,
            "NonRes": 0.0,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True
    }

    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "20152016": {
            # "PhiPi": 8.9264e-02,
            # "KstK": 9.1456e-02,
            # "NonRes": 1.3025e-01,
            # "KPiPi": 5.9153e-01,
            # "PiPiPi": 4.1545e-01
            "PhiPi": 0.3,
            "KstK": 0.3,
            "NonRes": 0.3,
            "KPiPi": 1.0,
            "PiPiPi": 1.0
        },
        "2017": {
            # "PhiPi": 8.9264e-02,
            # "KstK": 9.1456e-02,
            # "NonRes": 1.3025e-01,
            # "KPiPi": 5.9153e-01,
            # "PiPiPi": 4.1545e-01
            "PhiPi": 0.3,
            "KstK": 0.3,
            "NonRes": 0.3,
            "KPiPi": 1.0,
            "PiPiPi": 1.0
        },
        "2018": {
            # "PhiPi": 8.9264e-02,
            # "KstK": 9.1456e-02,
            # "NonRes": 1.3025e-01,
            # "KPiPi": 5.9153e-01,
            # "PiPiPi": 4.1545e-01
            "PhiPi": 0.3,
            "KstK": 0.3,
            "NonRes": 0.3,
            "KPiPi": 1.0,
            "PiPiPi": 1.0
        },
        # "Run2": {
        #     "PhiPi": 8.9264e-02,
        #     "KstK": 9.1456e-02,
        #     "NonRes": 1.3025e-01,
        #     "KPiPi": 1.0,
        #     "PiPiPi": 1.0
        # },
        "Fixed": {
            "KPiPi": True,
            "PiPiPi": True
        }
    }

    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"] = "ExponentialPlusSignal"
    configdict["CombBkgShape"]["CharmMass"]["cD"] = {
        "20152016": {
            "PhiPi": [-6.6075e-03, -5.0e-2, 0.0],
            "KstK": [-3.3159e-03, -5.0e-2, 0.0],
            "NonRes": [-2.1752e-03, -5.0e-2, 0.0],
            "KPiPi": [-2.6575e-04, -5.0e-3, 0.0],
            "PiPiPi": [-1.8805e-03, -5.0e-2, 0.0]
        },
        "2017": {
            "PhiPi": [-6.6075e-03, -5.0e-2, 0.0],
            "KstK": [-3.3159e-03, -5.0e-2, 0.0],
            "NonRes": [-2.1752e-03, -5.0e-2, 0.0],
            "KPiPi": [-2.6575e-04, -5.0e-3, 0.0],
            "PiPiPi": [-1.8805e-03, -5.0e-2, 0.0]
        },
        "2018": {
            "PhiPi": [-6.6075e-03, -5.0e-2, 0.0],
            "KstK": [-3.3159e-03, -5.0e-2, 0.0],
            "NonRes": [-2.1752e-03, -5.0e-2, 0.0],
            "KPiPi": [-2.6575e-04, -5.0e-3, 0.0],
            "PiPiPi": [-1.8805e-03, -5.0e-2, 0.0]
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {
        "20152016": {
            "PhiPi": 3.5780e-01,
            "KstK": 5.4897e-01,
            "NonRes": 8.4653e-01,
            "KPiPi": 9.6697e-01,
            "PiPiPi": 9.1997e-01
        },
        "2017": {
            "PhiPi": 3.5780e-01,
            "KstK": 5.4897e-01,
            "NonRes": 8.4653e-01,
            "KPiPi": 9.6697e-01,
            "PiPiPi": 9.1997e-01
        },
        "2018": {
            "PhiPi": 3.5780e-01,
            "KstK": 5.4897e-01,
            "NonRes": 8.4653e-01,
            "KPiPi": 9.6697e-01,
            "PiPiPi": 9.1997e-01
        },
        "Fixed": False
    }

    #   ------------------------------------   #
    #   ----   Additional parameters   -----   #
    #   ------------------------------------   #

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_fsig"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.03,
                    "Range": [0.0, 0.1]
                }
            }
        },
        "BgName": "Bs2DsDsstKKst",
        "Fixed": False
    }  # Link group 1 to signal
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.6,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }  #used <5300
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.2,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }  #used <5250
    configdict["AdditionalParameters"]["g1_f3_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.8,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }  #used <5150
    # Anna's studies: g2_f1 = 0.766417, g2_f2 = 0.813774
    configdict["AdditionalParameters"]["g2_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.766417,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": True
    }
    configdict["AdditionalParameters"]["g2_f2_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.813774,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": True
    }
    configdict["AdditionalParameters"]["g2_f3_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.8,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }  #used <5200
    configdict["AdditionalParameters"]["g3_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.75,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": True
    }  #0.5 for 5100
    configdict["AdditionalParameters"]["g5_f1_frac"] = {
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.92,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }

    #   ------------------------------------   #
    #   -----    Bs2DsPi background    -----   #
    #   ------------------------------------   #

    #shape for BeautyMass, for CharmMass taken by default the same as signal
    configdict["Bs2DsPiShape"] = {}
    configdict["Bs2DsPiShape"]["BeautyMass"] = {}
    configdict["Bs2DsPiShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["Bs2DsPiShape"]["BeautyMass"]["mean"] = {
        "20152016": {
            "All": 5413.9
        },
        "2017": {
            "All": 5412.87
        },
        "2018": {
            "All": 5412.61
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["sigmaI"] = {
        "20152016": {
            "All": 7.0208e+01
        },
        "2017": {
            "All": 6.6566e+01
        },
        "2018": {
            "All": 6.4551e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["sigmaJ"] = {
        "20152016": {
            "All": 2.4141e+01
        },
        "2017": {
            "All": 2.3747e+01
        },
        "2018": {
            "All": 2.3970e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "All": 1.0394
        },
        "2017": {
            "All": 7.9704e-01
        },
        "2018": {
            "All": 7.6308e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "All": 1.1463e-01
        },
        "2017": {
            "All": 9.8741e-02
        },
        "2018": {
            "All": 8.7353e-02
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["n1"] = {
        "20152016": {
            "All": 4.4613e-01
        },
        "2017": {
            "All": 1.1453
        },
        "2018": {
            "All": 1.0957
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "All": 3.8162
        },
        "2017": {
            "All": 4.0707
        },
        "2018": {
            "All": 4.1679
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "All": -1.1
        },
        "2017": {
            "All": -1.1
        },
        "2018": {
            "All": -1.1
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["fb"] = {
        "20152016": {
            "All": 0.0
        },
        "2017": {
            "All": 0.0
        },
        "2018": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["zeta"] = {
        "20152016": {
            "All": 0.0
        },
        "2017": {
            "All": 0.0
        },
        "2018": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["tau"] = {
        "20152016": {
            "All": 4.9975e-01
        },
        "2017": {
            "All": 5.0082e-01
        },
        "2018": {
            "All": 5.2378e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["nu"] = {
        "20152016": {
            "All": 1.6494
        },
        "2017": {
            "All": 1.5068
        },
        "2018": {
            "All": 1.3168
        },
        "Fixed": True
    }
    configdict["Bs2DsPiShape"]["BeautyMass"]["fracI"] = {
        "20152016": {
            "All": 0.527
        },
        "2017": {
            "All": 0.527
        },
        "2018": {
            "All": 0.527
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Bs2DsstPi background   -----   #
    #   ------------------------------------   #

    #shape for BeautyMass, for CharmMass taken by default the same as signal
    configdict["Bs2DsstPiShape"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"]["type"] = "Ipatia"
    configdict["Bs2DsstPiShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5288.0
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["sigma"] = {
        "20152016": {
            "All": 60.647
        },
        "2017": {
            "All": 64.970
        },
        "2018": {
            "All": 67.335
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "All": 0.90768
        },
        "2017": {
            "All": 0.91529
        },
        "2018": {
            "All": 0.88945
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "All": 2.7337
        },
        "2017": {
            "All": 2.9243
        },
        "2018": {
            "All": 2.7439
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "All": -2.2
        },
        "2017": {
            "All": -2.2
        },
        "2018": {
            "All": -2.2
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "All": 1.0
        },
        "2017": {
            "All": 1.0
        },
        "2018": {
            "All": 1.0
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["n1"] = {
        "20152016": {
            "All": 1.0
        },
        "2017": {
            "All": 1.0
        },
        "2018": {
            "All": 1.0
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["fb"] = {
        "20152016": {
            "All": 0.0
        },
        "2017": {
            "All": 0.0
        },
        "2018": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["zeta"] = {
        "20152016": {
            "All": 0.0
        },
        "2017": {
            "All": 0.0
        },
        "2018": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Bd2DsRho background    -----   #
    #   ------------------------------------   #

    #shape for BeautyMass, for CharmMass taken by default the same as signal
    configdict["Bs2DsRhoShape"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"]["type"] = "DoubleExponential"
    configdict["Bs2DsRhoShape"]["BeautyMass"]["cB1"] = {
        "20152016": {
            "All": -7.6745e-03
        },
        "2017": {
            "All": -6.9675e-03
        },
        "2018": {
            "All": -6.0720e-03
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["cB2"] = {
        "20152016": {
            "All": -2.7527e-02
        },
        "2017": {
            "All": -2.5049e-02
        },
        "2018": {
            "All": -2.1769e-02
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["frac"] = {
        "20152016": {
            "All": 6.8500e-01
        },
        "2017": {
            "All": 6.0639e-01
        },
        "2018": {
            "All": 5.7916e-01
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----    Bd2DsK background     -----   #
    #   ------------------------------------   #

    #shape for BeautyMass, for CharmMass taken by default the same as signal
    configdict["Bd2DsKShape"] = {}
    configdict["Bd2DsKShape"]["BeautyMass"] = {}
    configdict["Bd2DsKShape"]["BeautyMass"]["type"] = "Ipatia"
    configdict["Bd2DsKShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5279.2
        },
        "Fixed": True
    }  #5280.7 - 1.5
    configdict["Bd2DsKShape"]["BeautyMass"]["sigma"] = {
        "20152016": {
            "All": 12.711
        },
        "2017": {
            "All": 13.108
        },
        "2018": {
            "All": 13.530
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "All": 3.5247
        },
        "2017": {
            "All": 3.0684
        },
        "2018": {
            "All": 3.1044
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "All": 1.7307
        },
        "2017": {
            "All": 1.7022
        },
        "2018": {
            "All": 1.8570
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "All": -2.4663
        },
        "2017": {
            "All": -3.5219
        },
        "2018": {
            "All": -2.8899
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "All": 1.0
        },
        "2017": {
            "All": 1.0
        },
        "2018": {
            "All": 1.0
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["n1"] = {
        "20152016": {
            "All": 1.0
        },
        "2017": {
            "All": 1.0
        },
        "2018": {
            "All": 1.0
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["fb"] = {
        "20152016": {
            "All": 0.0
        },
        "2017": {
            "All": 0.0
        },
        "2018": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["zeta"] = {
        "20152016": {
            "All": 0.0
        },
        "2017": {
            "All": 0.0
        },
        "2018": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}
    configdict["Yields"]["Bd2DK"] = {
        "2015": {
            "NonRes": 12.8,
            "PhiPi": 0.22,
            "KstK": 3.76,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 54.56,
            "PhiPi": 1.02,
            "KstK": 15.8,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 62.1,
            "PhiPi": 1.474,
            "KstK": 17.68,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 74.84,
            "PhiPi": 1.439,
            "KstK": 21.29,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True,
    }

    configdict["Yields"]["Bd2DPi"] = {
        "2015": {
            "NonRes": 5.12,
            "PhiPi": 0.09,
            "KstK": 1.51,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 21.824,
            "PhiPi": 0.42,
            "KstK": 6.32,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 24.84,
            "PhiPi": 0.59,
            "KstK": 7.07,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 29.936,
            "PhiPi": 0.57,
            "KstK": 8.5,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True,
    }

    configdict["Yields"]["Lb2LcK"] = {
        "2015": {
            "NonRes": 7.77,
            "PhiPi": 0.41,
            "KstK": 1.76,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 44.32,
            "PhiPi": 4.15,
            "KstK": 9.49,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 43.33,
            "PhiPi": 3.48,
            "KstK": 8.86,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 51.00,
            "PhiPi": 4.96,
            "KstK": 10.62,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True
    }

    configdict["Yields"]["Lb2LcPi"] = {
        "2015": {
            "NonRes": 3.08,
            "PhiPi": 0.16,
            "KstK": 0.7,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2016": {
            "NonRes": 17.72,
            "PhiPi": 1.66,
            "KstK": 3.80,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2017": {
            "NonRes": 17.32,
            "PhiPi": 1.39,
            "KstK": 3.55,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "2018": {
            "NonRes": 20.40,
            "PhiPi": 1.98,
            "KstK": 4.25,
            "KPiPi": 0.0,
            "PiPiPi": 0.0
        },
        "Fixed": True
    }

    # configdict["Yields"]["Bs2DsDsstKKst"] = {
    #     "20152016": {
    #         "PhiPi": 100.0,
    #         "KstK": 100.0,
    #         "NonRes": 100.0,
    #         "KPiPi": 100.0,
    #         "PiPiPi": 100.0
    #     },
    #     "2017": {
    #         "PhiPi": 100.0,
    #         "KstK": 100.0,
    #         "NonRes": 100.0,
    #         "KPiPi": 100.0,
    #         "PiPiPi": 100.0
    #     },
    #     "2018": {
    #         "PhiPi": 100.0,
    #         "KstK": 100.0,
    #         "NonRes": 100.0,
    #         "KPiPi": 100.0,
    #         "PiPiPi": 100.0
    #     },
    #     "Fixed": False
    # }

    configdict["Yields"]["BsLb2DsDsstPPiRho"] = {
        "20152016": {
            "PhiPi": 2000.0,
            "KstK": 1500.0,
            "NonRes": 1200.0,
            "KPiPi": 500.0,
            "PiPiPi": 800.0
        },
        "2017": {
            "PhiPi": 2000.0,
            "KstK": 1500.0,
            "NonRes": 1200.0,
            "KPiPi": 500.0,
            "PiPiPi": 800.0
        },
        "2018": {
            "PhiPi": 2000.0,
            "KstK": 1500.0,
            "NonRes": 1200.0,
            "KPiPi": 500.0,
            "PiPiPi": 800.0
        },
        "Fixed": False
    }

    # configdict["Yields"]["CombBkg"] = {
    #     "20152016": {
    #         "PhiPi": 800.0,
    #         "KstK": 800.0,
    #         "NonRes": 1500.0,
    #         "KPiPi": 1500.0,
    #         "PiPiPi": 2200.0
    #     },
    #     "2017": {
    #         "PhiPi": 800.0,
    #         "KstK": 800.0,
    #         "NonRes": 1500.0,
    #         "KPiPi": 1500.0,
    #         "PiPiPi": 2200.0
    #     },
    #     "2018": {
    #         "PhiPi": 800.0,
    #         "KstK": 800.0,
    #         "NonRes": 1500.0,
    #         "KPiPi": 1500.0,
    #         "PiPiPi": 2200.0
    #     },
    #     "Fixed": False
    # }
    configdict["Yields"]["CombBkg"] = {
        "20152016": {
            "PhiPi": 600.0,
            "KstK": 600.0,
            "NonRes": 1400.0,
            "KPiPi": 1400.0,
            "PiPiPi": 2000.0
        },
        "2017": {
            "PhiPi": 600.0,
            "KstK": 600.0,
            "NonRes": 1400.0,
            "KPiPi": 1400.0,
            "PiPiPi": 2000.0
        },
        "2018": {
            "PhiPi": 600.0,
            "KstK": 600.0,
            "NonRes": 1400.0,
            "KPiPi": 1400.0,
            "PiPiPi": 2000.0
        },
        "Fixed": False
    }

    configdict["Yields"]["Signal"] = {
        "20152016": {
            "PhiPi": 2900.0,
            "KstK": 2100.0,
            "NonRes": 1500.0,
            "KPiPi": 500.0,
            "PiPiPi": 1000.0
        },
        "2017": {
            "PhiPi": 2900.0,
            "KstK": 2100.0,
            "NonRes": 1500.0,
            "KPiPi": 500.0,
            "PiPiPi": 1000.0
        },
        "2018": {
            "PhiPi": 2900.0,
            "KstK": 2100.0,
            "NonRes": 1500.0,
            "KPiPi": 500.0,
            "PiPiPi": 1000.0
        },
        "Fixed": False
    }

    #----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    #----------------------------------------------------------------------#

    import ROOT as R
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = {
        "EPDF": [
            "Sig", "CombBkg", "Lb2LcK", "Lb2LcPi", "Bd2DK", "Bd2DPi",
            "BsLb2DsDsstPPiRho", "Bs2DsDsstKKst"
        ],
        "PDF": [
            "Sig", "CombBkg", "Lb2LcK", "Lb2LcPi", "Lb2DsDsstP",
            "Bs2DsDsstPiRho", "Bd2DK", "Bd2DPi", "Bs2DsDsstKKst"
        ],
        "Legend": [
            "Sig", "CombBkg", "Lb2LcKPi", "Lb2DsDsstP", "Bs2DsDsstPiRho",
            "Bd2DKPi", "Bs2DsDsstKKst"
        ]
    }
    configdict["PlotSettings"]["colors"] = {
        "PDF": [
            R.kRed - 7, R.kMagenta - 2, R.kGreen - 3, R.kGreen - 3,
            R.kYellow - 9, R.kBlue - 6, R.kRed, R.kRed, R.kBlue - 10
        ],
        "Legend": [
            R.kRed - 7, R.kMagenta - 2, R.kGreen - 3, R.kYellow - 9,
            R.kBlue - 6, R.kRed, R.kBlue - 10
        ]
    }

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.03,
        "LHCbText": [0.75, 0.9]
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.03,
        "LHCbText": [0.75, 0.9]
    }
    return configdict
