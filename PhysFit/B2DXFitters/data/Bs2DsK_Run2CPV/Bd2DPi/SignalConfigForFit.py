###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():

    configdict = {}

    from math import pi
    from math import log

    # considered decay mode
    configdict["Decay"] = "Bd2DPi"
    configdict["CharmModes"] = {"KPiPi"}
    configdict["Backgrounds"] = [
        "Bd2DK", "Bd2DRho", "Bd2DstPi", "Lb2LcPi", "Bs2DsPi"
    ]
    # year of data taking
    configdict["YearOfDataTaking"] = {"2016"}  # "2015","2016"
    # stripping (necessary in case of PIDK shapes)
    configdict["Stripping"] = {"2015": "24r1", "2016": "28r1", "2017": "34"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2015": {
            "Down": 1.0,
            "Up": 1.0
        },
        "2016": {
            "Down": 1.0,
            "Up": 1.0
        },
        "2017": {
            "Down": 1.0,
            "Up": 1.0
        }
    }
    # file name with paths to MC/data samples
    configdict["dataName"] = "../data/Bs2DsK_Run2CPV/Bd2DPi/config_Bd2DPi.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBd2DPi",
        "Extension": "pdf"
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5000, 5600],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1830, 1920],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 150.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1"
    }
    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data":
        "lab1_M<200&&lab1_PIDK!=-1000.0&&(lab1_hasRich==1&&lab3_hasRich==1&&lab4_hasRich==1&&lab5_hasRich==1)&&(lab1_isMuon==0&&lab3_isMuon==0&&lab4_isMuon==0&&lab5_isMuon==0)",
        "MC":
        "lab1_M<200&&lab1_PIDK!=-1000.0&&(lab1_hasRich==1&&lab3_hasRich==1&&lab4_hasRich==1&&lab5_hasRich==1)&&(lab1_isMuon==0&&lab3_isMuon==0&&lab4_isMuon==0&&lab5_isMuon==0)",
        "MCID":
        True,
        "MCTRUEID":
        True,
        "BKGCAT":
        True,
        "DsHypo":
        True
    }
    configdict["AdditionalCuts"]["KPiPi"] = {
        "Data":
        "lab3_M>200&&lab4_M<200&&lab5_M<200&&((lab2_MM-lab34_MM>200)&&(lab2_MM-lab35_MM>200))",
        "MC":
        "lab3_M>200&&lab4_M<200&&lab5_M<200&&((lab2_MM-lab34_MM>200)&&(lab2_MM-lab35_MM>200))&&(!(abs(lab14_MassHypo_KPi_MM-1870)<20))&&(!(abs(lab15_MassHypo_KPi_MM-1870)<20))&&(!(fabs(lab2_MassHypo_Lambda_pi1-2286.4)<30.&&lab4_PIDp>0))&&(!(fabs(lab2_MassHypo_Lambda_pi2-2286.4)<30.&&lab5_PIDp>0))&&(lab2_MassHypo_Ds_pi1<1949.||lab2_MassHypo_Ds_pi1>2029.||lab4_PIDK<0.)&&(lab2_MassHypo_Ds_pi2<1949.||lab2_MassHypo_Ds_pi2>2029.||lab5_PIDK<0.)&&(lab1_PIDe<5&&lab5_PIDe<5&&lab4_PIDe<5)"
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    # weighting mass templates
    configdict["WeightingMassTemplates"] = {}
    #configdict["WeightingMassTemplates"] = {"RatioDataMC": { "2015":{"FileLabel":"#DataMC 2015", "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
    #                                                         "2016":{"FileLabel":"#DataMC 2016", "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
    #                                                         "2017":{"FileLabel":"#DataMC 2017", "Var":["lab1_P","nTracks"], "HistName":"histRatio"}}
    #                                       }

    if True:  # switch more variables on/off
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
            "Range": [0.0, 2500.0],
            "InputName": "lab0_ENDVERTEX_ZERR"
        }
        configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
            "Range": [0.0, 3000.0],
            "InputName": "lab2_ENDVERTEX_ZERR"
        }
        configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
            "Range": [0.0, 30.0],
            "InputName": "lab0_ENDVERTEX_CHI2"
        }
        configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
            "Range": [0.0, 30.0],
            "InputName": "lab2_ENDVERTEX_CHI2"
        }
        configdict["AdditionalVariables"]["lab0_FDCHI2_OWNPV"] = {
            "Range": [-100000.0, 30000000.0],
            "InputName": "lab0_FDCHI2_OWNPV"
        }
        configdict["AdditionalVariables"]["lab0_P"] = {
            "Range": [0.0, 1600000.0],
            "InputName": "lab0_P"
        }
        configdict["AdditionalVariables"]["lab0_PT"] = {
            "Range": [0.0, 40000.0],
            "InputName": "lab0_PT"
        }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    # Bs signal shapes
    configdict["BsSignalShape"] = {}
    #configdict["BsSignalShape"]["type"]    = "DoubleCrystalBall"
    #configdict["BsSignalShape"]["mean"]    = {"Run2": {"All":5283.}, "Fixed":False}
    #configdict["BsSignalShape"]["sigma1"]  = {"Run2": {"KPiPi":8.6},  "Fixed":False}
    #configdict["BsSignalShape"]["sigma2"]  = {"Run2": {"KPiPi":14.5},  "Fixed":False}
    #configdict["BsSignalShape"]["alpha1"]  = {"Run2": {"KPiPi":0.9},  "Fixed":False}
    #configdict["BsSignalShape"]["alpha2"]  = {"Run2": {"KPiPi":-2.1}, "Fixed":False}
    #configdict["BsSignalShape"]["n1"]      = {"Run2": {"KPiPi":1.8},  "Fixed":False}
    #configdict["BsSignalShape"]["n2"]      = {"Run2": {"KPiPi":5.4},  "Fixed":False}
    #configdict["BsSignalShape"]["frac"]    = {"Run2": {"KPiPi":0.25},  "Fixed":False}
    configdict["BsSignalShape"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["BsSignalShape"]["mean"] = {
        "Run2": {
            "All": 5.28009e+03
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["sigmaI"] = {
        "Run2": {
            "All": 2.91096e+01
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["sigmaJ"] = {
        "Run2": {
            "All": 1.59093e+01
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["zeta"] = {"Run2": {"All": 0.}, "Fixed": True}
    configdict["BsSignalShape"]["fb"] = {"Run2": {"All": 0.}, "Fixed": True}
    configdict["BsSignalShape"]["l"] = {
        "Run2": {
            "All": -1.31535e+00
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["a1"] = {
        "Run2": {
            "All": 9.42766e-01
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["a2"] = {
        "Run2": {
            "All": 3.97764e+00
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["n1"] = {
        "Run2": {
            "All": 1.37902e+00
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["n2"] = {
        "Run2": {
            "All": 1.17766e+00
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["nu"] = {
        "Run2": {
            "All": -4.89365e-01
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["tau"] = {
        "Run2": {
            "All": 3.77491e-01
        },
        "Fixed": False
    }
    configdict["BsSignalShape"]["fracI"] = {
        "Run2": {
            "All": 4.14556e-01
        },
        "Fixed": False
    }

    # Ds signal shapes
    configdict["DsSignalShape"] = {}
    configdict["DsSignalShape"]["type"] = "DoubleCrystalBall"
    configdict["DsSignalShape"]["mean"] = {
        "Run2": {
            "All": 1869.8
        },
        "Fixed": False
    }
    configdict["DsSignalShape"]["sigma1"] = {
        "Run2": {
            "KPiPi": 10.0
        },
        "Fixed": False
    }
    configdict["DsSignalShape"]["sigma2"] = {
        "Run2": {
            "KPiPi": 5.6
        },
        "Fixed": False
    }
    configdict["DsSignalShape"]["alpha1"] = {
        "Run2": {
            "KPiPi": 1.7
        },
        "Fixed": False
    }
    configdict["DsSignalShape"]["alpha2"] = {
        "Run2": {
            "KPiPi": -3.0
        },
        "Fixed": False
    }
    configdict["DsSignalShape"]["n1"] = {
        "Run2": {
            "KPiPi": 1.5
        },
        "Fixed": False
    }
    configdict["DsSignalShape"]["n2"] = {
        "Run2": {
            "KPiPi": 0.6
        },
        "Fixed": False
    }
    configdict["DsSignalShape"]["frac"] = {
        "Run2": {
            "KPiPi": 0.41
        },
        "Fixed": False
    }

    # expected yields
    configdict["Yields"] = {}
    configdict["Yields"]["Signal"] = {
        "2015": {
            "KPiPi": 100000.0
        },
        "2016": {
            "KPiPi": 200000.0
        },
        "Fixed": False
    }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit plotting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig"]
    configdict["PlotSettings"]["colors"] = [kBlue + 2]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9]
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }

    return configdict
