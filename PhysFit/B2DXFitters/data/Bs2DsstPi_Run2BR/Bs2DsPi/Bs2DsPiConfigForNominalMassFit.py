##########make#####################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    configdict = {}
    diffBdBs = 87.38
    from math import pi
    from math import log

    # considered decay mode
    configdict["Decay"] = "Bs2DsPi"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi", "Lb2LcPi", "Bs2DsRho", "Bs2DsstRho", "Bs2DsstPi", "Bd2DsPi",
        "Bs2DsK", "Bd2DsstPi"
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2011", "2012", "2015", "2016", "2017", "2018"
    }
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {
            "Down": 0.5600,
            "Up": 0.4200
        },
        "2012": {
            "Down": 0.9912,
            "Up": 0.9988
        },
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        },
    }

    # stripping (necessary in case of PIDK shapes)
    configdict[
        "dataName"] = "../data/Bs2DsstPi_Run2BR/Bs2DsPi/config_Bs2DsPi.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotLb2Dsp",
        "Extension": "pdf"
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5000, 6000],
        "InputName": "lab0_MassFitConsD_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1948, 1988],
        "InputName": "lab2_MM"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "lab1_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "lab1_PT"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3"
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR"
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR"
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2"
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2"
    }
    configdict["AdditionalVariables"]["lab0_L0HadronDecision_TOS"] = {
        "Range": [-1.0, 2.0],
        "InputName": "lab0_L0HadronDecision_TOS"
    }
    configdict["AdditionalVariables"]["lab0_L0Global_TIS"] = {
        "Range": [-1.0, 2.0],
        "InputName": "lab0_L0Global_TIS"
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "(lab1_isMuon==0)",
        "MC": " ",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": True
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5"
    }

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": 5367.61198
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {
            "All": 2.1200e+01
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {
            "All": 1.4164e+01
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {
            "All": 1.2216e+00
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {
            "All": 2.9330e+00
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {
            "All": 1.3014e+00
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {
            "All": 1.8071e+00
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {
            "All": -1.7000e+00
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {
            "All": -7.3818e-01
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {
            "All": 2.4993e-01
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {
            "All": 4.0231e-01
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    single = True
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    if single:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
            "Run1": {
                "All": -1.4315e-03
            },
            "Run2": {
                "All": -1.1186e-03
            },
            "Fixed": False
        }
    else:
        configdict["CombBkgShape"] = {}
        configdict["CombBkgShape"]["BeautyMass"] = {}
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
            "Run1": {
                "All": -7.0421e-04
            },
            "Run2": {
                "All": -1.1186e-03
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
            "Run1": {
                "All": -2.4427e-03
            },
            "Run2": {
                "All": -3.9865e-02
            },
            "Fixed": False
        }
        configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
            "Run1": {
                "All": 2.1079e-01
            },
            "Run2": {
                "All": 4.3067e-01
            },
            "Fixed": False
        }

    #   ------------------------------------   #
    #   -----  Bs2DsstPi background    -----   #
    #   ------------------------------------   #

    configdict["Bs2DsstPiShape"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"][
        "type"] = "HILLdiniPlusHORNSdini"
    configdict["Bs2DsstPiShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 3.3131e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["ahill"] = {
        "Run2": {
            "All": 4.5428e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {
            "All": 5.0995e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["bhill"] = {
        "Run2": {
            "All": 5.3128e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {
            "All": 5.2086e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["csihill"] = {
        "Run2": {
            "All": -1.4189e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["csihorns"] = {
        "Run2": {
            "All": 8.8532e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 4.3284e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["frachorns"] = {
        "Run2": {
            "All": 1.8269e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 4.2754e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["sigmahorns"] = {
        "Run2": {
            "All": 1.8912e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----  Bd2DsstPi background    -----   #
    #   ------------------------------------   #

    configdict["Bd2DsstPiShape"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"][
        "type"] = "HILLdiniPlusHORNSdini"
    configdict["Bd2DsstPiShape"]["BeautyMass"]["R"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["R"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["ahill"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstPiShape"]["BeautyMass"]["ahill"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstPiShape"]["BeautyMass"]["ahorns"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["bhill"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstPiShape"]["BeautyMass"]["bhill"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstPiShape"]["BeautyMass"]["bhorns"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True
    }

    configdict["Bd2DsstPiShape"]["BeautyMass"]["csihill"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["csihill"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["csihorns"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["csihorns"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["frac"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["frac"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["frachorns"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["frachorns"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["sigma"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["sigma"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["sigmahorns"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["sigmahorns"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Bs2DsRho background    -----   #
    #   ------------------------------------   #

    configdict["Bs2DsRhoShape"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bs2DsRhoShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 6.2480e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "All": 4.1313e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["b"] = {
        "Run2": {
            "All": 5.2272e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["csi"] = {
        "Run2": {
            "All": 4.1840e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 9.8288e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 1.5453e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Bd2DsRho background    -----   #
    #   ------------------------------------   #

    configdict["Bd2DsRhoShape"] = {}
    configdict["Bd2DsRhoShape"]["BeautyMass"] = {}
    configdict["Bd2DsRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bd2DsRhoShape"]["BeautyMass"]["R"] = configdict[
        "Bs2DsRhoShape"]["BeautyMass"]["R"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "All":
            configdict["Bs2DsRhoShape"]["BeautyMass"]["a"]["Run2"]["All"] -
            diffBdBs
        },
        "Fixed": True
    }
    configdict["Bd2DsRhoShape"]["BeautyMass"]["b"] = {
        "Run2": {
            "All":
            configdict["Bs2DsRhoShape"]["BeautyMass"]["b"]["Run2"]["All"] -
            diffBdBs
        },
        "Fixed": True
    }

    configdict["Bd2DsRhoShape"]["BeautyMass"]["csi"] = configdict[
        "Bs2DsRhoShape"]["BeautyMass"]["csi"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["frac"] = configdict[
        "Bs2DsRhoShape"]["BeautyMass"]["frac"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["sigma"] = configdict[
        "Bs2DsRhoShape"]["BeautyMass"]["sigma"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----  Bs2DsstRho background   -----   #
    #   ------------------------------------   #

    configdict["Bs2DsstRhoShape"] = {}
    configdict["Bs2DsstRhoShape"]["BeautyMass"] = {}
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 7.9142e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "All": 3.0000e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["b"] = {
        "Run2": {
            "All": 5.0933e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["csi"] = {
        "Run2": {
            "All": 5.0000e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 9.9800e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 3.9731e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   -----   Bd2DsPi background     -----   #
    #   ------------------------------------   #

    configdict["Bd2DsPiShape"] = {}
    configdict["Bd2DsPiShape"]["BeautyMass"] = {}
    if configdict["SignalShape"]["BeautyMass"][
            "type"] == "IpatiaPlusJohnsonSU":
        configdict["Bd2DsPiShape"]["BeautyMass"][
            "type"] = "ShiftedSignalIpatiaJohnsonSU"
        configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
            "Run1": {
                "All": -diffBdBs
            },
            "Run2": {
                "All": -diffBdBs
            },
            "Fixed": True
        }
    else:
        configdict["Bd2DsPiShape"]["BeautyMass"]["type"] = "ShiftedSignal"
        configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
            "Run2": {
                "All": diffBdBs
            },
            "Fixed": True
        }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale1"] = {
        "Run1": {
            "All": 1.00808721452
        },
        "Run2": {
            "All": 1.00808721452
        },
        "Fixed": True
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale2"] = {
        "Run1": {
            "All": 1.03868673310
        },
        "Run2": {
            "All": 1.03868673310
        },
        "Fixed": True
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}
    configdict["Yields"]["Bd2DPi"] = {
        "Run2": {
            "KKPi": [2972.6 * 1.1, 300.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bd2DsPi"] = {
        "2011": {
            "KKPi": 1077.43
        },
        "2012": {
            "KKPi": 1077.43
        },
        "2015": {
            "KKPi": 196.60
        },
        "2016": {
            "KKPi": 1077.43
        },
        "2017": {
            "KKPi": 982.239
        },
        "2018": {
            "KKPi": 1275.19
        },
        "Fixed": False
    }
    configdict["Yields"]["Bd2DsstPi"] = {
        "2011": {
            "KKPi": 1077.43
        },
        "2012": {
            "KKPi": 1077.43
        },
        "2015": {
            "KKPi": 196.60
        },
        "2016": {
            "KKPi": 1077.43
        },
        "2017": {
            "KKPi": 982.239
        },
        "2018": {
            "KKPi": 1275.19
        },
        "Fixed": False
    }
    configdict["Yields"]["Bd2DsRho"] = {
        "2011": {
            "KKPi": 1077.43
        },
        "2012": {
            "KKPi": 1077.43
        },
        "2015": {
            "KKPi": 196.60
        },
        "2016": {
            "KKPi": 1077.43
        },
        "2017": {
            "KKPi": 982.239
        },
        "2018": {
            "KKPi": 1275.19
        },
        "Fixed": False
    }
    configdict["Yields"]["Lb2LcPi"] = {
        "Run2": {
            "KKPi": [2230.0 * 1.1, 223.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsK"] = {
        "Run2": {
            "KKPi": [1289.0 * 1.1, 130.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstPi"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Bs2DsRho"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Bs2DsstRho"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Signal"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }

    #----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    #----------------------------------------------------------------------#

    from ROOT import (kRed, kBlue, kOrange, kMagenta, kGreen, kSolid)

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Lb2LcPi",
        "Bd2DPi",
        "Bs2DsK",
        "Bd2DsPi",
        "Bd2DsstPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#d7301f",
        "#cccccc",
        "#238443",
        "#fbb4b9",
        "#08519c",
        "#c51b8a",
        "#7a0177",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
    ]
    configdict["PlotSettings"]["pattern"] = [
        kSolid, 3004, kSolid, 3005, kSolid, 3005, 3005, kSolid, kSolid, kSolid
    ]
    configdict["PlotSettings"]["patterncolor"] = [
        "#d7301f",
        "#969696",
        "#238443",
        "#feebe2",
        "#08519c",
        "#feebe2",
        "#feebe2",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict
