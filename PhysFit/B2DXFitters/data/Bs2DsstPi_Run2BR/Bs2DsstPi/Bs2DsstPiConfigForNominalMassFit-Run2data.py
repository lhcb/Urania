###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    configdict = {}

    from math import pi
    from math import log

    # considered decay mode
    configdict["Decay"] = "Bs2DsstPi"
    # configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK", "KPiPi", "PiPiPi"}
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = ["Bs2DsRho", "Bs2DsstRho", "Bd2DsstPi"]
    # year of data taking
    configdict["YearOfDataTaking"] = {"2016", "2017", "2018"
                                      }  # {"2015", "2016", "2017", "2018"}
    # stripping (necessary in case of PIDK shapes)
    configdict["Stripping"] = {"2016": "28r2"}  # {"2012":"21", "2011":"21r1"}
    # integrated luminosity in each year of data taking (necessary in case of fitting several data samples simultaneously)
    configdict["IntegratedLuminosity"] = {
        "2015": {
            "Down": 1.0000,
            "Up": 1.0000
        },  # 2011/2012
        "2016": {
            "Down": (0.18695 + 0.85996),
            "Up": (0.14105 + 0.80504)
        },  # 2015/2016 Down: (0.18695+0.85996) Up: (0.14105+0.80504)
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        }
    }
    # file name with paths to MC/data samples
    configdict["dataName"] = "../data/DsstPi2DsPi/config_Bs2DsstPi.txt"

    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsstPi",
        "Extension": "pdf"
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5100, 5600],
        "InputName": "FBs_LF_M"
    }  # 6000 5600
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [2080, 2150],
        "InputName": "FDsst_DeltaM_M"
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0., 999999.0],
        "InputName": "FBs_LF_ctau"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000., 150000.0],
        "InputName": "FBac_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250., 45000.0],
        "InputName": "FBac_Ptr"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150., 0.0],
        "InputName": "FBac_PIDK"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15., 1000.0],
        "InputName": "FnTracks"
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0., 999999.0],
        "InputName": "FBs_LF_ctauErr"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "FBac_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [-1.0, 1.0],
        "InputName": "FBDT_Var"
    }
    configdict["BasicVariables"]["TagDecOS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "FBs_OSKaon_TAGDEC"
    }
    configdict["BasicVariables"]["TagDecSS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "FBs_SSKaon_TAGDEC"
    }
    configdict["BasicVariables"]["MistagOS"] = {
        "Range": [0.0, 0.5],
        "InputName": "FBs_OSKaon_TAGETA"
    }
    configdict["BasicVariables"]["MistagSS"] = {
        "Range": [0.0, 0.5],
        "InputName": "FBs_SSKaon_TAGETA"
    }
    '''
    # additional variables in data sets
    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["BacEta"]   = { "Range" : [-9.0,    9.0     ], "InputName" : "FBac_Eta"}
    configdict["AdditionalVariables"]["PhEta"]    = { "Range" : [-9.0,    9.0     ], "InputName" : "FPh_Eta"}
    configdict["AdditionalVariables"]["PhPT"]     = { "Range" : [ 0.0,    1.e+9   ], "InputName" : "FPh_Ptr"}
    configdict["AdditionalVariables"]["PhCL"]     = { "Range" : [ 0.0,    1.0     ], "InputName" : "FPh_CL"}
    configdict["AdditionalVariables"]["PhisNotE"] = { "Range" : [ 0.0,    1.0     ], "InputName" : "FPh_isNotE"}
    configdict["AdditionalVariables"]["BsEta"]    = { "Range" : [-9.0,    9.0     ], "InputName" : "FBs_Eta"}
    configdict["AdditionalVariables"]["BsPhi"]    = { "Range" : [ 0.0,    9.0     ], "InputName" : "FBs_Phi"}
    configdict["AdditionalVariables"]["BsPT"]     = { "Range" : [ 0.0,    1.e+9   ], "InputName" : "FBs_Ptr"}
    configdict["AdditionalVariables"]["DsDec"]    = { "Range" : [ 0.0,    6.0     ], "InputName" : "FDs_Dec"}
    configdict["AdditionalVariables"]["KpPT"]     = { "Range" : [100.,45000.0     ], "InputName" : "FKp_Ptr"}
    configdict["AdditionalVariables"]["KmPT"]     = { "Range" : [100.,45000.0     ], "InputName" : "FKm_Ptr"}
    configdict["AdditionalVariables"]["PiPT"]     = { "Range" : [100.,45000.0     ], "InputName" : "FPi_Ptr"}
    configdict["AdditionalVariables"]["PtrRel"]   = { "Range" : [ 0.0,  200.      ], "InputName" : "FPtr_Rel"}
    configdict["AdditionalVariables"]["CosTheS"]  = { "Range" : [-1.0,     1.0    ], "InputName" : "FCTS_Ds"}
    configdict["AdditionalVariables"]["BsIpChi2Own"] = { "Range" : [ 0.0,  100.    ], "InputName" : "FBs_IpChi2Own"}
    configdict["AdditionalVariables"]["BsDIRAOwn"]= { "Range" : [-1.0,     1.0    ], "InputName" : "FBs_DIRAOwn"}
    configdict["AdditionalVariables"]["BsRFD"]    = { "Range" : [ 0.0,   100.0    ], "InputName" : "FBs_RFD"}
    configdict["AdditionalVariables"]["DsDIRAOri"]= { "Range" : [-1.0,     1.0    ], "InputName" : "FDs_DIRAOri"}
    configdict["AdditionalVariables"]["nSPDHits"] = { "Range" : [ 0.0,  1000.0    ], "InputName" : "FnSPDHits"}
    '''

    # tagging calibration
    configdict["TaggingCalibration"] = {}
    configdict["TaggingCalibration"]["OS"] = {
        "p0": 0.3834,
        "p1": 0.9720,
        "average": 0.3813
    }
    configdict["TaggingCalibration"]["SS"] = {
        "p0": 0.4244,
        "p1": 1.2180,
        "average": 0.4097
    }

    # additional cuts applied to data sets
    app = "&&"
    l0_cuts = "((FBs_L0HadDec_TOS+FBs_L0Global_TIS)>0.)"
    hlt1_cuts = "((FBs_Hlt1TrkMVA_TOS[0]+FBs_Hlt1TrkMVA_TOS[1])>0.)"
    hlt2_cuts = "((FBs_Hlt2Topo_TOS[0]+FBs_Hlt2Topo_TOS[1]+FBs_Hlt2Topo_TOS[2])>0.)"
    eta_cuts = "(FBs_Eta>2.&&FBs_Eta<5.)&&(FBac_Eta>2.&&FBac_Eta<5.)&&(FKp_Eta>2.&&FKp_Eta<5.)&&(FKm_Eta>2.&&FKm_Eta<5.)&&(FPi_Eta>2.&&FPi_Eta<5.)"
    ntrks_cuts = "(FnTracks>15.&&FnTracks<500.)"
    p_trks_cuts = "(FKp_P>2000.&&FKp_P<150000.)&&(FKm_P>2000.&&FKm_P<150000.)&&(FPi_P>2000.&&FPi_P<150000.)"
    pt_trks_cuts = "(FKp_Ptr>250.&&FKp_Ptr<45000.)&&(FKm_Ptr>250.&&FKm_Ptr<45000.)&&(FPi_Ptr>250.&&FPi_Ptr<45000.)"
    pt_bac_cuts = "(FBac_Ptr>250.&&FBac_Ptr<45000.)"
    sl_veto_cuts = "FisMuon_Sum==0."
    spec_cuts_1 = "FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.06"
    spec_cuts_2 = "FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>111.5&&(FDsst_M-FDs_M)<181.5"
    # PID cuts
    has_rich_cuts = "FhasRich_Sum==15."
    pidk_k_cuts = "(FKp_PID[0]>5.&&((abs(FKm_ID)==321&&FKm_PID[0]>5.)||(abs(FPi_ID)==321&&FPi_PID[0]>5.)))"
    pidk_pi_cuts = "((abs(FKm_ID)==211&&(FKm_PID[0]<0.&&FKm_PID[0]!=-1000.))||(abs(FPi_ID)==211&&(FPi_PID[0]<0.&&FPi_PID[0]!=-1000.)))"
    bac_pid_cuts = "(FBac_PIDK<0.&&FBac_PIDK!=-1000.)"
    full_cuts=l0_cuts+app+hlt1_cuts+app+hlt2_cuts+app+ \
              eta_cuts+app+ntrks_cuts+app+p_trks_cuts+app+pt_trks_cuts+app+pt_bac_cuts+app+sl_veto_cuts+app+ \
              spec_cuts_1+app+spec_cuts_2
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data":
        full_cuts + app + has_rich_cuts + app + pidk_k_cuts + app +
        pidk_pi_cuts + app + bac_pid_cuts,
        "MC":
        full_cuts
    }
    configdict["AdditionalCuts"]["KKPi"] = {
        "Data": "(FDs_Dec>0.&&FDs_Dec<4.)",
        "MC": "(FDs_Dec>0.&&FDs_Dec<4.)"
    }

    configdict["CreateCombinatorial"] = {}
    configdict["CreateCombinatorial"]["BeautyMass"] = {}
    configdict["CreateCombinatorial"]["BeautyMass"]["All"] = {
        "Cut":
        "FBs_LF_M>5100.&&FBs_LF_M<6000.&&FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>195.&&(FDsst_M-FDs_M)<205.&&FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.06&&FBac_P>1000.&&FBac_P<650000.&&FBac_Ptr>500.&&FBac_Ptr<45000.&&FBac_PIDK<0",
        "Rho":
        3.5,
        "Mirror":
        "Left"
    }
    configdict["CreateCombinatorial"]["BeautyMass"]["NonRes"] = {
        "Cut": "FDs_Dec==3."
    }
    configdict["CreateCombinatorial"]["BeautyMass"]["PhiPi"] = {
        "Cut": "FDs_Dec==1."
    }
    configdict["CreateCombinatorial"]["BeautyMass"]["KstK"] = {
        "Cut": "FDs_Dec==2."
    }
    configdict["CreateCombinatorial"]["BeautyMass"]["KPiPi"] = {
        "Cut":
        "FDs_Dec==5.&&FDs_FDCHI2_ORIVX>9.&&FPi_PIDK>10.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FKst_M<1750."
    }
    configdict["CreateCombinatorial"]["BeautyMass"]["PiPiPi"] = {
        "Cut":
        "FDs_Dec==4.&&FDs_FDCHI2_ORIVX>9.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FPi_PIDK<0.&&FPhi_M<1700.&&FKst_M<1700."
    }

    configdict["CreateCombinatorial"]["BacPIDK"] = {}
    configdict["CreateCombinatorial"]["BacPIDK"]["All"] = {
        "Cut":
        "FBs_DsstMC_M>5100.&&FBs_DsstMC_M<6000.&&FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>195.&&(FDsst_M-FDs_M)<205.&&FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.06&&FBac_P>1000.&&FBac_P<650000.&&FBac_Ptr>500.&&FBac_Ptr<45000.",
        "Rho":
        1.25,
        "Mirror":
        "Left"
    }

    # configdict["WeightingMassTemplates"] = { "RatioDataMC":{"FileLabel":{"2011":"#RatioDataMC 2011 PNTr","2012":"#RatioDataMC 2012 PNTr"},
    #                                                         "Var":["FBac_P","FnTracks"],"HistName":"histRatio"},
    #                                          "Shift":{ "BeautyMass": 3.3, "CharmMass": 2.8} }
    # configdict["WeightingMassTemplates"] = { "Shift":{ "BeautyMass": 3.3, "CharmMass": 2.8} }
    #configdict["WeightingMassTemplates"]={"PIDBachEff":      {"FileLabel":{"2011":"#PIDK Pion 2011","2012":"#PIDK Pion 2012"},
    #                                                          "Var":["nTracks","lab1_P"],"HistName":"MyPionEff_0_mu2"},
    #                                      "PIDBachMisID":    {"FileLabel":{"2011":"#PIDK Kaon 2011","2012":"#PIDK Kaon 2012"},
    #                                                          "Var":["nTracks","lab1_P"],"HistName":"MyKaonMisID_0_mu2"},
    #                                      "PIDChildKaonPionMisID":{"FileLabel":{"2011":"#PIDK Pion 2011","2012":"#PIDK Pion 2012"},
    #                                                               "Var":["nTracks","lab3_P"],"HistName":"MyPionMisID_0"},
    #                                      "PIDChildProtonMisID": {"FileLabel":{"2011":"#PIDK Proton 2011","2012":"#PIDK Proton 2012"},
    #                                                              "Var":["nTracks","lab4_P"],"HistName":"MyProtonMisID_pKm5_KPi5"},
    #                                      "RatioDataMC":{"FileLabel":{"2011":"#RatioDataMC 2011 PNTr","2012":"#RatioDataMC 2012 PNTr"},
    #                                                     "Var":["lab1_P","nTracks"],"HistName":"histRatio"},
    #                                      "Shift":{"BeautyMass":-2.0,"CharmMass":0.0}}
    configdict["WeightingMassTemplates"] = {}

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    if (False):
        configdict["SignalShape"]["BeautyMass"][
            "type"] = "DoubleCrystalBall"  #WithWidthRatio"
        configdict["SignalShape"]["BeautyMass"]["mean"] = {
            "Run2": {
                "KKPi": 5.3696e+03
            },
            "Fixed": False
        }
        configdict["SignalShape"]["BeautyMass"]["sigma1"] = {
            "Run2": {
                "KKPi": 3.2552e+01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["sigma2"] = {
            "Run2": {
                "KKPi": 2.0662e+01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["alpha1"] = {
            "Run2": {
                "KKPi": 1.8701e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["alpha2"] = {
            "Run2": {
                "KKPi": -1.2380e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["n1"] = {
            "Run2": {
                "KKPi": 1.2910e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["n2"] = {
            "Run2": {
                "KKPi": 1.2120e+01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["frac"] = {
            "Run2": {
                "KKPi": 4.5187e-01
            },
            "Fixed": True
        }
        #configdict["SignalShape"]["BeautyMass"]["R"]    =    {"Run2": {"KKPi": 1.}, "Fixed":True}
    if (True):
        configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaJohnsonSU"
        configdict["SignalShape"]["BeautyMass"]["mean"] = {
            "Run2": {
                "KKPi": 5.3710e+03
            },
            "Fixed": False
        }
        configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
            "Run2": {
                "KKPi": 3.8652e+01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
            "Run2": {
                "KKPi": 2.2123e+01
            },
            "Fixed": False
        }
        configdict["SignalShape"]["BeautyMass"]["zeta"] = {
            "Run2": {
                "KKPi": 0.
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["fb"] = {
            "Run2": {
                "KKPi": 0.
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["a1"] = {
            "Run2": {
                "KKPi": (2. * 1.9169e+00)
            },
            "Fixed": False
        }
        configdict["SignalShape"]["BeautyMass"]["a2"] = {
            "Run2": {
                "KKPi": 3.2251e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["n1"] = {
            "Run2": {
                "KKPi": 1.0845e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["n2"] = {
            "Run2": {
                "KKPi": 6.3872e-01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["l"] = {
            "Run2": {
                "KKPi": -3.9829e+01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["tau"] = {
            "Run2": {
                "KKPi": 1.4308e-01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["nu"] = {
            "Run2": {
                "KKPi": 5.9493e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["BeautyMass"]["fracI"] = {
            "Run2": {
                "KKPi": 3.7587e-01
            },
            "Fixed": True
        }

    # Ds signal shapes
    configdict["SignalShape"]["CharmMass"] = {}
    if (False):
        configdict["SignalShape"]["CharmMass"][
            "type"] = "DoubleCrystalBall"  #WithWidthRatio"
        configdict["SignalShape"]["CharmMass"]["mean"] = {
            "Run2": {
                "KKPi": 2.1116e+03
            },
            "Fixed": False
        }
        configdict["SignalShape"]["CharmMass"]["sigma1"] = {
            "Run2": {
                "KKPi": 1.0479e+01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["sigma2"] = {
            "Run2": {
                "KKPi": 8.7262e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["alpha1"] = {
            "Run2": {
                "KKPi": 1.1616e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["alpha2"] = {
            "Run2": {
                "KKPi": -6.1971e-01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["n1"] = {
            "Run2": {
                "KKPi": 2.5925e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["n2"] = {
            "Run2": {
                "KKPi": 6.0097e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["frac"] = {
            "Run2": {
                "KKPi": 2.5731e-01
            },
            "Fixed": True
        }
        #configdict["SignalShape"]["CharmMass"]["R"]    =    {"Run2": {"KKPi": 1.}, "Fixed":True}
    if (True):
        configdict["SignalShape"]["CharmMass"]["type"] = "IpatiaJohnsonSU"
        configdict["SignalShape"]["CharmMass"]["mean"] = {
            "Run2": {
                "KKPi": 2.1139e+03
            },
            "Fixed": False
        }
        configdict["SignalShape"]["CharmMass"]["sigmaI"] = {
            "Run2": {
                "KKPi": 1.8218e+01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["sigmaJ"] = {
            "Run2": {
                "KKPi": 1.3007e+01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["zeta"] = {
            "Run2": {
                "KKPi": 0.
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["fb"] = {
            "Run2": {
                "KKPi": 0.
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["a1"] = {
            "Run2": {
                "KKPi": 7.4062e-01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["a2"] = {
            "Run2": {
                "KKPi": 6.2236e-02
            },
            "Fixed": True
        }  # 5.* for 2018
        configdict["SignalShape"]["CharmMass"]["n1"] = {
            "Run2": {
                "KKPi": 0.
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["n2"] = {
            "Run2": {
                "KKPi": 1.2331e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["l"] = {
            "Run2": {
                "KKPi": -1.0910e+00
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["tau"] = {
            "Run2": {
                "KKPi": 5.5876e-01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["nu"] = {
            "Run2": {
                "KKPi": 9.4052e-01
            },
            "Fixed": True
        }
        configdict["SignalShape"]["CharmMass"]["fracI"] = {
            "Run2": {
                "KKPi": 1.9591e-01
            },
            "Fixed": True
        }

    # combinatorial background
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    #configdict["CombBkgShape"]["BeautyMass"]["type"] = "RooKeysPdf"
    #configdict["CombBkgShape"]["BeautyMass"]["name"] = {"2015": {"NonRes": "PhysBkgCombPi_BeautyMassPdf_m_both_nonres_2015",
    #                                                             "PhiPi":  "PhysBkgCombPi_BeautyMassPdf_m_both_phipi_2015",
    #                                                             "KstK":   "PhysBkgCombPi_BeautyMassPdf_m_both_kstk_2015",
    #                                                             "KPiPi":  "PhysBkgCombPi_BeautyMassPdf_m_both_kpipi_2015",
    #                                                             "PiPiPi": "PhysBkgCombPi_BeautyMassPdf_m_both_pipipi_2015"},
    #                                                    "2016": {"NonRes": "PhysBkgCombPi_BeautyMassPdf_m_both_nonres_2016",
    #                                                             "PhiPi":  "PhysBkgCombPi_BeautyMassPdf_m_both_phipi_2016",
    #                                                             "KstK":   "PhysBkgCombPi_BeautyMassPdf_m_both_kstk_2016",
    #                                                             "KPiPi":  "PhysBkgCombPi_BeautyMassPdf_m_both_kpipi_2016",
    #                                                             "PiPiPi": "PhysBkgCombPi_BeautyMassPdf_m_both_pipipi_2016"}}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "RooHILLidni"
    configdict["CombBkgShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "KKPi": 5.1000e+03
        },
        "Fixed": True
    }
    configdict["CombBkgShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "KKPi": 0.0
        },
        "Fixed": True
    }
    # 225 250
    if (True):
        configdict["CombBkgShape"]["BeautyMass"]["b"] = {
            "2015": {
                "KKPi": 5.4863e+03
            },
            "2016": {
                "KKPi": 5.4911e+03
            },
            "2017": {
                "KKPi": 5.5126e+03
            },
            "2018": {
                "KKPi": 5.5242e+03
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["csi"] = {
            "2015": {
                "KKPi": 1.6219e+00
            },
            "2016": {
                "KKPi": 1.9556e+00
            },
            "2017": {
                "KKPi": 2.2243e+00
            },
            "2018": {
                "KKPi": 2.7542e+00
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["sigma"] = {
            "2015": {
                "KKPi": 3.6669e+02
            },
            "2016": {
                "KKPi": 1.3986e+03
            },
            "2017": {
                "KKPi": 1.4392e+03
            },
            "2018": {
                "KKPi": 1.4400e+03
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["R"] = {
            "2015": {
                "KKPi": 2.3084e-01
            },
            "2016": {
                "KKPi": 6.1730e-02
            },
            "2017": {
                "KKPi": 6.3017e-02
            },
            "2018": {
                "KKPi": 6.3704e-02
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
            "2015": {
                "KKPi": 6.1555e-01
            },
            "2016": {
                "KKPi": 8.6999e-01
            },
            "2017": {
                "KKPi": 8.5762e-01
            },
            "2018": {
                "KKPi": 8.7067e-01
            },
            "Fixed": True
        }
    if (False):
        configdict["CombBkgShape"]["BeautyMass"]["b"] = {
            "Run2": {
                "KKPi": 5.5089e+03
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["csi"] = {
            "Run2": {
                "KKPi": 2.2714e+00
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["sigma"] = {
            "Run2": {
                "KKPi": 1.4359e+03
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["R"] = {
            "Run2": {
                "KKPi": 6.2534e-02
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
            "Run2": {
                "KKPi": 8.6696e-01
            },
            "Fixed": True
        }
    # 195 205
    #configdict["CombBkgShape"]["BeautyMass"]["b"]     = {"2015": {"KKPi": 5.4918e+03}, "2016": {"KKPi": 5.4502e+03}, "2017": {"KKPi": 5.4914e+03}, "2018": {"KKPi": 5.4744e+03}, "Fixed":True}
    #configdict["CombBkgShape"]["BeautyMass"]["csi"]   = {"2015": {"KKPi": 1.6156e+00}, "2016": {"KKPi": 1.4417e+00}, "2017": {"KKPi": 1.6442e+00}, "2018": {"KKPi": 1.5956e+00}, "Fixed":True}
    #configdict["CombBkgShape"]["BeautyMass"]["sigma"] = {"2015": {"KKPi": 1.4036e+02}, "2016": {"KKPi": 1.6963e+02}, "2017": {"KKPi": 1.2748e+02}, "2018": {"KKPi": 1.3906e+02}, "Fixed":True}
    #configdict["CombBkgShape"]["BeautyMass"]["R"]     = {"2015": {"KKPi": 5.4401e+01}, "2016": {"KKPi": 2.6040e+01}, "2017": {"KKPi": 5.8426e+01}, "2018": {"KKPi": 7.8673e+01}, "Fixed":True}
    #configdict["CombBkgShape"]["BeautyMass"]["frac"]  = {"2015": {"KKPi": 1.5695e-01}, "2016": {"KKPi": 4.4513e-01}, "2017": {"KKPi": 1.7154e-01}, "2018": {"KKPi": 1.2222e-01}, "Fixed":True}

    configdict["CombBkgShape"]["CharmMass"] = {}
    #configdict["CombBkgShape"]["CharmMass"]["type"]  = "Exponential"
    #configdict["CombBkgShape"]["CharmMass"]["cB"]    = {"Run2": {"KKPi": -1.0e-2}, "Fixed":False}
    configdict["CombBkgShape"]["CharmMass"]["type"] = "ExponentialPlusSignal"
    configdict["CombBkgShape"]["CharmMass"]["cD"] = {
        "Run2": {
            "KKPi": -1.0e-2
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {
        "Run2": {
            "KKPi": 1.0
        },
        "Fixed": True
    }
    #configdict["CombBkgShape"]["CharmMass"]["type"] = "DoubleExponential"
    #configdict["CombBkgShape"]["CharmMass"]["cB1"]  = {"Run2": {"KKPi": -1.0e-2}, "Fixed":False}
    #configdict["CombBkgShape"]["CharmMass"]["cB2"]  = {"Run2": {"KKPi": 0.0    }, "Fixed":True}
    #configdict["CombBkgShape"]["CharmMass"]["frac"] = {"Run2": {"KKPi": 5.00e-1}, "Fixed":False}

    configdict["CombBkgShape"]["BacPIDK"] = {}
    configdict["CombBkgShape"]["BacPIDK"]["type"] = "Fixed"
    configdict["CombBkgShape"]["BacPIDK"]["components"] = {
        "Kaon": True,
        "Pion": True,
        "Proton": False
    }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK1"] = {
        "Run1": {
            "PhiPi": 0.5,
            "NonRes": 0.5,
            "KstK": 0.5,
            "KPiPi": 0.5,
            "PiPiPi": 0.5
        },
        "Fixed": False
    }

    #configdict["Lb2DspShape"] = {}
    #configdict["Lb2DspShape"]["BeautyMass"] = {}
    #configdict["Lb2DspShape"]["BeautyMass"]["type"]    = "Gaussian"
    #configdict["Lb2DspShape"]["BeautyMass"]["sigma"]   = {"Run2": {"All": 20.0}, "Fixed":False}
    #configdict["Lb2DspShape"]["BeautyMass"]["mean"]    = {"Run2": {"All": 5650.0}, "Fixed":False}

    configdict["Bd2DsstPiShape"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"]["type"] = "ShiftedSignal"
    configdict["Bd2DsstPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 86.8
        },
        "Fixed": True
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["scale1"] = {
        "Run2": {
            "All": 1.00
        },
        "Fixed": True
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["scale2"] = {
        "Run2": {
            "All": 1.00
        },
        "Fixed": True
    }

    #expected yields
    configdict["Yields"] = {}
    configdict["Yields"]["Bs2DsstRho"] = {
        "2015": {
            "KKPi": 9000.
        },
        "2016": {
            "KKPi": 9000.
        },
        "2017": {
            "KKPi": 9000.
        },
        "2018": {
            "KKPi": 9000.
        },
        "Fixed": False
    }
    configdict["Yields"]["Bd2DsstPi"] = {
        "2015": {
            "KKPi": 0.
        },
        "2016": {
            "KKPi": 0.
        },
        "2017": {
            "KKPi": 0.
        },
        "2018": {
            "KKPi": 0.
        },
        "Fixed": True
    }
    configdict["Yields"]["Bs2DsRho"] = {
        "2015": {
            "KKPi": 20000.
        },
        "2016": {
            "KKPi": 20000.
        },
        "2017": {
            "KKPi": 20000.
        },
        "2018": {
            "KKPi": 20000.
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2015": {
            "KKPi": 30000.
        },
        "2016": {
            "KKPi": 30000.
        },
        "2017": {
            "KKPi": 30000.
        },
        "2018": {
            "KKPi": 30000.
        },
        "Fixed": False
    }
    configdict["Yields"]["Signal"] = {
        "2015": {
            "KKPi": 25000.
        },
        "2016": {
            "KKPi": 25000.
        },
        "2017": {
            "KKPi": 25000.
        },
        "2018": {
            "KKPi": 25000.
        },
        "Fixed": False
    }
    #configdict["Yields"]["Lb2Dsp"]  = {"2016": { "NonRes":1000.,  "PhiPi":1000.,  "KstK":1000.,  "KPiPi":1000.,  "PiPiPi":1000.},
    #                                   "2017": { "NonRes":1000.,  "PhiPi":1000.,  "KstK":1000.,  "KPiPi":1000.,  "PiPiPi":1000.},
    #                                   "2018": { "NonRes":1000.,  "PhiPi":1000.,  "KstK":1000.,  "KPiPi":1000.,  "PiPiPi":1000.}, "Fixed":False}

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit plotting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig", "CombBkg", "Bd2DsstPi", "Bs2DsRho", "Bs2DsstRho", "Lb2Dsp"
    ]
    configdict["PlotSettings"]["colors"] = [
        kRed - 7, kBlue - 6, kOrange, kBlue - 10, kGreen + 3, kRed
    ]
    #configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Bs2DsRho", "Bs2DsstRho"]
    #configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kBlue-10, kGreen+3]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9]
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.75,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.2, 0.9],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }

    return configdict
