
#Signal Bs2DsstPi KKPi 2016
/afs/cern.ch/work/a/abertoli/public/DsstrPi/bdt-Run2MC/
Filter-Run2MC_Bs2DsstPi_Signal_dw.root
Filter-Run2MC_Bs2DsstPi_Signal_up.root
tuple
tuple
###

#PIDK Kaon 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_K_Turbo15_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_K_Turbo15_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Kaon 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_K_Turbo16_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_K_Turbo16_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Kaon 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_K_Turbo17_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_K_Turbo17_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Kaon 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_K_Turbo18_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_K_Turbo18_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Pion 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_Pi_Turbo15_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_Pi_Turbo15_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Pion 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_Pi_Turbo16_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_Pi_Turbo16_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Pion 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_Pi_Turbo17_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_Pi_Turbo17_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Pion 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_Pi_Turbo18_MagDown_highMom_nTracks_Brunel_Brunel_P.root
PerfHists_Pi_Turbo18_MagUp_highMom_nTracks_Brunel_Brunel_P.root
###

#PIDK Proton 2015
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_P_Turbo15_MagDown_highMomProton_nTracks_Brunel_Brunel_P.root
PerfHists_P_Turbo15_MagUp_highMomProton_nTracks_Brunel_Brunel_P.root
###

#PIDK Proton 2016
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_P_Turbo16_MagDown_highMomProton_nTracks_Brunel_Brunel_P.root
PerfHists_P_Turbo16_MagUp_highMomProton_nTracks_Brunel_Brunel_P.root
###

#PIDK Proton 2017
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_P_Turbo17_MagDown_highMomProton_nTracks_Brunel_Brunel_P.root
PerfHists_P_Turbo17_MagUp_highMomProton_nTracks_Brunel_Brunel_P.root
###

#PIDK Proton 2018
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/PIDHist/
PerfHists_P_Turbo18_MagDown_highMomProton_nTracks_Brunel_Brunel_P.root
PerfHists_P_Turbo18_MagUp_highMomProton_nTracks_Brunel_Brunel_P.root
###

#DataMC 2015 Sim09c
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping24r1_Sim09c/
weights_DPi_2015_dw.root
weights_DPi_2015_up.root
###

#DataMC 2016 Sim09h
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping28r1_Sim09h/
weights_DPi_2016_dw.root
weights_DPi_2016_up.root
###

#DataMC 2017 Sim09h
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping29r2_Sim09h/
weights_DPi_2017_dw.root
weights_DPi_2017_up.root
###

#DataMC 2018 Sim09h
/eos/lhcb/wg/b2oc/TD_Bs2Dsh_Run2/Bd2DPi_data2MC_weights/Run2/p_nTracks/Stripping34_Sim09h/
weights_DPi_2018_dw.root
weights_DPi_2018_up.root
###
