###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsstPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    #----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    #----------------------------------------------------------------------#

    configdict["pdfList"] = {}
    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsstPi"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsstPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsstPi"]["BeautyMass"][
        "IpatiaPlusJohnsonSU"] = {
            "Title": "B_{s}#rightarrowD^{*}_{s}#pi",
            "Bins": 200,
            "Min": 5100.0,
            "Max": 5600.0,  # 5500.0
            "Unit": "MeV/c^{2}",
            "mean": [5370, 5300, 5440],
            "sigmaI": [37.0, 27.0, 47.0],
            "sigmaJ": [23.0, 13.0, 33.0],
            "zeta": [0.0],
            "fb": [0.0],
            "a1": [1.9, 0.4, 2.4],
            "a2": [3.0, 1.0, 5.0],
            "n1": [1.1, 0.1, 2.1],
            "n2": [1.0, 0.4, 4.0],
            "l": [-30.0, -50.0, -10.0],
            "tau": [0.2, 0.0, 0.4],
            "nu": [5.0, 2.5, 10.0],
            "fracI": [0.4, 0.01, 0.99]
        }
    configdict["pdfList"]["Signal"]["Bs2DsstPi"]["BeautyMass"][
        "DoubleCrystalBall"] = {
            "Title": "B_{d}#rightarrowD#pi",
            "Bins": 200,
            "Min": 5100.0,
            "Max": 5600.0,  # 5500.
            "Unit": "MeV/c^{2}",
            "mean": [5370, 5300, 5400],
            "sameMean": True,
            "sigma1": [30.0, 15.0, 60.0],
            "sigma2": [20.0, 10.0, 40.0],
            "alpha1": [2.0, 1.0, 4.0],
            "alpha2": [-1.0, -2.0, 0.0],
            "n1": [1.0, 0.1, 10.0],
            "n2": [12.0, 5.0, 20.0],
            "frac": [0.5, 0.001, 0.999]
        }

    configdict["pdfList"]["Signal"]["Bs2DsstPi"]["CharmMass"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsstPi"]["CharmMass"][
        "IpatiaPlusJohnsonSU"] = {
            "Title": "m(D_{s},#gamma)",
            "Bins": 200,
            "Min": 2080.0,
            "Max": 2150.0,
            "Unit": "MeV/c^{2}",
            "mean": [2110, 1990, 2130],
            "sigmaI": [20.0, 15.0, 25.0],
            "sigmaJ": [12.5, 11.5, 13.5],
            "zeta": [0.0],
            "fb": [0.0],
            "l": [-2.0, -4.0, -0.5],
            "a1": [0.8, 0.4, 1.6],
            "a2": [0.05, 0.01, 0.15],
            "n1": [0.0],
            "n2": [1.1, 0.5, 2.0],
            "nu": [1.0, 0.5, 2.0],
            "tau": [0.5, 0.2, 0.8],
            "fracI": [0.4, 0.001, 0.999]
        }
    configdict["pdfList"]["Signal"]["Bs2DsstPi"]["CharmMass"][
        "DoubleCrystalBall"] = {
            "Title": "m(D_{s},#gamma)",
            "Bins": 200,
            "Min": 2080.0,
            "Max": 2150.0,
            "Unit": "MeV/c^{2}",
            "mean": [2110, 1990, 2130],
            "sameMean": True,
            "sigma1": [10.0, 5.0, 20.0],
            "sigma2": [8.0, 4.0, 16.0],
            "alpha1": [1.5, 0.75, 3.0],
            "alpha2": [-1.0, -2.0, 0.0],
            "n1": [2.5, 1.0, 5.0],
            "n2": [5.0, 1.0, 10.0],
            "frac": [0.5, 0.001, 0.999]
        }

    configdict["pdfList"]["Bs2DsRho"] = {}
    configdict["pdfList"]["Bs2DsRho"]["Bs2DsPi"] = {}
    configdict["pdfList"]["Bs2DsRho"]["Bs2DsPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsRho"]["Bs2DsPi"]["BeautyMass"]["HORNSdini"] = {
        "Title": "B_{s}#rightarrowD_{s}#rho",
        "Bins": 100,
        "Min": 5000.0,
        "Max": 5400.0,
        "Unit": "MeV/c^{2}",
        "a": [3500, 3000, 4500],
        "b": [5200, 4300, 5500],
        "csi": [1.1, -2.0, 6.0],
        "sigma": [40.0, 2.0, 80.0],
        "shift": [0.0],
        "R": [5.8, 0.5, 40.0],
        "frac": [0.98, 0.0, 1.0],
    }

    configdict["pdfList"]["Bs2DsstRho"] = {}
    configdict["pdfList"]["Bs2DsstRho"]["Bs2DsPi"] = {}
    configdict["pdfList"]["Bs2DsstRho"]["Bs2DsPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsstRho"]["Bs2DsPi"]["BeautyMass"][
        "HORNSdini"] = {
            "Title": "B_{s}#rightarrowD_{s}#rho",
            "Bins": 100,
            "Min": 5000.0,
            "Max": 5800.0,
            "Unit": "MeV/c^{2}",
            "a": [3000],
            "b": [5.0905e+03, 4300, 5500],
            "csi": [0.0],
            "sigma": [4.3209e+01],
            "shift": [0.0],
            "R": [10.0, 0.5, 80.0],
            "frac": [9.9923e-01]
        }

    #Dataset prefix
    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Bs2DsstPi_",
        "Bs2DsRho": "dataSetMC_Bs2DsRho_",
        "Bs2DsstRho": "dataSetMC_Bs2DsstRho_",
    }

    #Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"] = {
        "BeautyMass": {
            "Bs2DsstPi": "m(D_{s}^{*-}#pi^{+}) [MeV/c^{2}]"
        },
        "CharmMass": {
            "Bs2DsstPi": "m(hhh,#gamma) [MeV/c^{2}]",
        }
    }

    return configdict
