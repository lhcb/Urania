###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    from Bs2DsstPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    # range to accomodate all sideband events: [1950,6000]
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1950, 6000],
        "InputName": "FDsst_DeltaM_M"
    }
    # for shifted SB the default range can be used
    # configdict["BasicVariables"]["CharmMass"]     = { "Range" : [2080,    2150    ], "InputName" : "FDsst_DeltaM_M"}

    # do not fit combinatorial where there are little events: FBs_LF_M<5600.
    # /!\ for fake data the sideband range is shifted to the signal one: [195,205] -> [111.5,181.5]
    # additional cuts applied to data sets
    app = "&&"
    l0_cuts = "((FBs_L0HadDec_TOS+FBs_L0Global_TIS)>0.)"
    hlt1_cuts = "((FBs_Hlt1TrkMVA_TOS[0]+FBs_Hlt1TrkMVA_TOS[1])>0.)"
    hlt2_cuts = "((FBs_Hlt2Topo_TOS[0]+FBs_Hlt2Topo_TOS[1]+FBs_Hlt2Topo_TOS[2])>0.)"
    eta_cuts = "(FBs_Eta>2.&&FBs_Eta<5.)&&(FBac_Eta>2.&&FBac_Eta<5.)&&(FKp_Eta>2.&&FKp_Eta<5.)&&(FKm_Eta>2.&&FKm_Eta<5.)&&(FPi_Eta>2.&&FPi_Eta<5.)"
    ntrks_cuts = "(FnTracks>15.&&FnTracks<500.)"
    p_trks_cuts = "(FKp_P>2000.&&FKp_P<150000.)&&(FKm_P>2000.&&FKm_P<150000.)&&(FPi_P>2000.&&FPi_P<150000.)"
    pt_trks_cuts = "(FKp_Ptr>250.&&FKp_Ptr<45000.)&&(FKm_Ptr>250.&&FKm_Ptr<45000.)&&(FPi_Ptr>250.&&FPi_Ptr<45000.)"
    pt_bac_cuts = "(FBac_Ptr>500.&&FBac_Ptr<45000.)"
    sl_veto_cuts = "FisMuon_Sum==0."
    spec_cuts_1 = "FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.06"
    spec_cuts_2 = "FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>225.&&(FDsst_M-FDs_M)<250."  # sidebands DeltaM selection
    # PID cuts
    has_rich_cuts = "FhasRich_Sum==15."
    pidk_k_cuts = "(FKp_PID[0]>5.&&((abs(FKm_ID)==321&&FKm_PID[0]>5.)||(abs(FPi_ID)==321&&FPi_PID[0]>5.)))"
    pidk_pi_cuts = "((abs(FKm_ID)==211&&(FKm_PID[0]<0.&&FKm_PID[0]!=-1000.))||(abs(FPi_ID)==211&&(FPi_PID[0]<0.&&FPi_PID[0]!=-1000.)))"
    bac_pid_cuts = "(FBac_PIDK<0.&&FBac_PIDK!=-1000.)"
    full_cuts=l0_cuts+app+hlt1_cuts+app+hlt2_cuts+app+ \
              eta_cuts+app+ntrks_cuts+app+p_trks_cuts+app+pt_trks_cuts+app+pt_bac_cuts+app+sl_veto_cuts+app+ \
              spec_cuts_1+app+spec_cuts_2
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data":
        full_cuts + app + has_rich_cuts + app + pidk_k_cuts + app +
        pidk_pi_cuts + app + bac_pid_cuts,
        "MC":
        full_cuts
    }
    configdict["AdditionalCuts"]["KKPi"] = {
        "Data": "(FDs_Dec>0.&&FDs_Dec<4.)",
        "MC": "(FDs_Dec>0.&&FDs_Dec<4.)"
    }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "KKPi": 5.3711e+03
        },
        "Fixed": True
    }  # from Signal MC
    configdict["SignalShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "KKPi": 1.
        },
        "Fixed": True
    }

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "RooHILLidni"
    configdict["CombBkgShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "KKPi": 5100.
        },
        "Fixed": True
    }
    configdict["CombBkgShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "KKPi": 0.0
        },
        "Fixed": True
    }
    configdict["CombBkgShape"]["BeautyMass"]["b"] = {
        "Run2": {
            "KKPi": 5500.
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BeautyMass"]["csi"] = {
        "Run2": {
            "KKPi": 2.
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "KKPi": 8.e+02
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "KKPi": 1.
        },
        "Fixed": False
    }
    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "KKPi": 0.9
        },
        "Fixed": False
    }

    configdict["Yields"] = {}
    configdict["Yields"]["CombBkg"] = {
        "2015": {
            "KKPi": 4000.0
        },
        "2016": {
            "KKPi": 4000.0
        },
        "2017": {
            "KKPi": 4000.0
        },
        "2018": {
            "KKPi": 4000.0
        },
        "Fixed": False
    }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit plotting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig", "CombBkg"]
    configdict["PlotSettings"]["colors"] = [kRed - 7, kBlue - 6]

    return configdict
