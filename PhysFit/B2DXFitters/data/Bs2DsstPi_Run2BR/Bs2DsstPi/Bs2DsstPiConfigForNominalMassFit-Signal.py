###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():

    configdict = {}
    diffBdBs = 87.23
    from math import pi
    from math import log

    # considered decay mode
    configdict["Decay"] = "Bs2DsstPi"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = ["Bs2DsRho", "Bs2DsstRho", "Bd2DsstPi"]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2016"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {
            "Down": 0.5600,
            "Up": 0.4200
        },
        "2012": {
            "Down": 0.9912,
            "Up": 0.9988
        },
        "2015": {
            "Down": 0.18695,
            "Up": 0.14105
        },
        "2016": {
            "Down": 0.85996,
            "Up": 0.80504
        },
        "2017": {
            "Down": 0.87689,
            "Up": 0.83311
        },
        "2018": {
            "Down": 1.04846,
            "Up": 1.14154
        },
    }

    configdict[
        "dataName"] = "../data/DsstPi2DsPi/config_Bs2DsstPi-SignalFit.txt"
    #settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsstPi",
        "Extension": "pdf"
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5100, 5600],
        "InputName": "FBs_LF_M"
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [2080, 2150],
        "InputName": "FDsst_DeltaM_M"
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 0.0],
        "InputName": "FBac_PIDK"
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "FBac_P"
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "FBac_Ptr"
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "FnTracks"
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "FBac_ID"
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [-1.0, 1.0],
        "InputName": "FBDT_Var"
    }

    # additional cuts applied to data sets
    app = "&&"
    l0_cuts = "((FBs_L0HadDec_TOS+FBs_L0Global_TIS)>0.)"
    hlt1_cuts = "((FBs_Hlt1TrkMVA_TOS[0]+FBs_Hlt1TrkMVA_TOS[1])>0.)"
    hlt2_cuts = "((FBs_Hlt2Topo_TOS[0]+FBs_Hlt2Topo_TOS[1]+FBs_Hlt2Topo_TOS[2])>0.)"
    eta_cuts = "(FBs_Eta>2.&&FBs_Eta<5.)&&(FBac_Eta>2.&&FBac_Eta<5.)&&(FKp_Eta>2.&&FKp_Eta<5.)&&(FKm_Eta>2.&&FKm_Eta<5.)&&(FPi_Eta>2.&&FPi_Eta<5.)"
    ntrks_cuts = "(FnTracks>15.&&FnTracks<500.)"
    p_trks_cuts = "(FKp_P>2000.&&FKp_P<150000.)&&(FKm_P>2000.&&FKm_P<150000.)&&(FPi_P>2000.&&FPi_P<150000.)"
    pt_trks_cuts = "(FKp_Ptr>250.&&FKp_Ptr<45000.)&&(FKm_Ptr>250.&&FKm_Ptr<45000.)&&(FPi_Ptr>250.&&FPi_Ptr<45000.)"
    pt_bac_cuts = "(FBac_Ptr>250.&&FBac_Ptr<45000.)"
    sl_veto_cuts = "FisMuon_Sum==0."
    spec_cuts_1 = "FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.06"
    spec_cuts_2 = "FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>111.5&&(FDsst_M-FDs_M)<181.5"
    # PID cuts
    has_rich_cuts = "FhasRich_Sum==15."
    pidk_k_cuts = "(FKp_PID[0]>5.&&((abs(FKm_ID)==321&&FKm_PID[0]>5.)||(abs(FPi_ID)==321&&FPi_PID[0]>5.)))"
    pidk_pi_cuts = "((abs(FKm_ID)==211&&(FKm_PID[0]<0.&&FKm_PID[0]!=-1000.))||(abs(FPi_ID)==211&&(FPi_PID[0]<0.&&FPi_PID[0]!=-1000.)))"
    bac_pid_cuts = "(FBac_PIDK<0.&&FBac_PIDK!=-1000.)"
    full_cuts=l0_cuts+app+hlt1_cuts+app+hlt2_cuts+app+ \
              eta_cuts+app+ntrks_cuts+app+p_trks_cuts+app+pt_trks_cuts+app+pt_bac_cuts+app+sl_veto_cuts+app+ \
              spec_cuts_1+app+spec_cuts_2
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data":
        full_cuts + app + has_rich_cuts + app + pidk_k_cuts + app +
        pidk_pi_cuts + app + bac_pid_cuts,
        "MC":
        full_cuts
    }
    configdict["AdditionalCuts"]["KKPi"] = {
        "Data": "(FDs_Dec>0.&&FDs_Dec<4.)",
        "MC": "(FDs_Dec>0.&&FDs_Dec<4.)"
    }

    #weighting templates by PID eff/misID
    configdict["WeightingMassTemplates"] = {
        '''
        "PIDBachEff": {
            "2015": {
                "FileLabel":
                "#PIDK Pion 2015",
                "Var": ["FnTracks", "FBac_P"],
                "HistName":
                "Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
            "2016": {
                "FileLabel":
                "#PIDK Pion 2016",
                "Var": ["FnTracks", "FBac_P"],
                "HistName":
                "Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
            "2017": {
                "FileLabel":
                "#PIDK Pion 2017",
                "Var": ["FnTracks", "FBac_P"],
                "HistName":
                "Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
            "2018": {
                "FileLabel":
                "#PIDK Pion 2018",
                "Var": ["FnTracks", "FBac_P"],
                "HistName":
                "Pi_DLLK < 0 && IsMuon ==0.0 && HasRich ==1.0 && DLLe < 5.0_All;"
            },
        },
        '''
        "RatioDataMC": {
            "2015": {
                "FileLabel": "#DataMC 2015 Sim09c",
                "Var": ["FBac_P", "FnTracks"],
                "HistName": "histRatio"
            },
            "2016": {
                "FileLabel": "#DataMC 2016 Sim09h",
                "Var": ["FBac_P", "FnTracks"],
                "HistName": "histRatio"
            },
            "2017": {
                "FileLabel": "#DataMC 2017 Sim09h",
                "Var": ["FBac_P", "FnTracks"],
                "HistName": "histRatio"
            },
            "2018": {
                "FileLabel": "#DataMC 2018 Sim09h",
                "Var": ["FBac_P", "FnTracks"],
                "HistName": "histRatio"
            },
        },
        "Shift": {
            "2011": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
            "2012": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
            "2015": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
            "2016": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
            "2017": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
            "2018": {
                "BeautyMass": 0.0,
                "CharmMass": 0.0
            },
        }
    }

    #############################################################################################################
    #################################                FITTING                #####################################
    #############################################################################################################

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run1": {
            "All": 5367.14257
        },
        "Run2": {
            "All": 5367.20514
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run1": {
            "All": 27.52421
        },
        "Run2": {
            "All": 22.79421
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run1": {
            "All": 14.28993
        },
        "Run2": {
            "All": 14.1756
        },
        "Fixed": False
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run1": {
            "All": 0.60279
        },
        "Run2": {
            "All": 1.0704
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run1": {
            "All": 2.24025
        },
        "Run2": {
            "All": 2.51503
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run1": {
            "All": 1.72701
        },
        "Run2": {
            "All": 1.33568
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run1": {
            "All": 2.18067
        },
        "Run2": {
            "All": 1.84587
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run1": {
            "All": -2.17063
        },
        "Run2": {
            "All": -1.70339
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run1": {
            "All": 0.0
        },
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run1": {
            "All": 0.0
        },
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run1": {
            "All": -0.27557
        },
        "Run2": {
            "All": -0.40407
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run1": {
            "All": 0.33575
        },
        "Run2": {
            "All": 0.29956
        },
        "Fixed": True
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run1": {
            "All": 0.13392
        },
        "Run2": {
            "All": 0.31522
        },
        "Fixed": True
    }

    single = True
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    if single:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
            "Run1": {
                "All": -1.4315e-03
            },
            "Run2": {
                "All": -1.1186e-03
            },
            "Fixed": False
        }
    else:
        configdict["CombBkgShape"] = {}
        configdict["CombBkgShape"]["BeautyMass"] = {}
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
            "Run1": {
                "All": -7.0421e-04
            },
            "Run2": {
                "All": -1.1186e-03
            },
            "Fixed": True
        }
        configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
            "Run1": {
                "All": -2.4427e-03
            },
            "Run2": {
                "All": -3.9865e-02
            },
            "Fixed": False
        }
        configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
            "Run1": {
                "All": 2.1079e-01
            },
            "Run2": {
                "All": 4.3067e-01
            },
            "Fixed": False
        }

    configdict["Bs2DsstPiShape"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"][
        "type"] = "HILLdiniPlusHORNSdini"
    configdict["Bs2DsstPiShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 5.0721e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["ahill"] = {
        "Run2": {
            "All": 4.6315e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {
            "All": 5.0997e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["bhill"] = {
        "Run2": {
            "All": 5.3104e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {
            "All": 5.2070e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["csihill"] = {
        "Run2": {
            "All": -1.1509e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["csihorns"] = {
        "Run2": {
            "All": 9.6358e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 3.1800e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["frachorns"] = {
        "Run2": {
            "All": 1.7398e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 2.9948e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["sigmahorns"] = {
        "Run2": {
            "All": 1.4226e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    configdict["Bd2DsstPiShape"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"][
        "type"] = "HILLdiniPlusHORNSdini"
    configdict["Bd2DsstPiShape"]["BeautyMass"]["R"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["R"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["ahill"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstPiShape"]["BeautyMass"]["ahill"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstPiShape"]["BeautyMass"]["ahorns"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["bhill"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstPiShape"]["BeautyMass"]["bhill"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {
            "All":
            configdict["Bs2DsstPiShape"]["BeautyMass"]["bhorns"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True
    }

    configdict["Bd2DsstPiShape"]["BeautyMass"]["csihill"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["csihill"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["csihorns"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["csihorns"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["frac"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["frac"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["frachorns"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["frachorns"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["sigma"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["sigma"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["sigmahorns"] = configdict[
        "Bs2DsstPiShape"]["BeautyMass"]["sigmahorns"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    configdict["Bs2DsRhoShape"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bs2DsRhoShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 6.4109e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "All": 4.1431e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["b"] = {
        "Run2": {
            "All": 5.2276e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["csi"] = {
        "Run2": {
            "All": 4.3152e+00
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 9.6730e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 1.5504e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    configdict["Bd2DsRhoShape"] = {}
    configdict["Bd2DsRhoShape"]["BeautyMass"] = {}
    configdict["Bd2DsRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bd2DsRhoShape"]["BeautyMass"]["R"] = configdict[
        "Bs2DsRhoShape"]["BeautyMass"]["R"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "All":
            configdict["Bs2DsRhoShape"]["BeautyMass"]["a"]["Run2"]["All"] -
            diffBdBs
        },
        "Fixed": True
    }
    configdict["Bd2DsRhoShape"]["BeautyMass"]["b"] = {
        "Run2": {
            "All":
            configdict["Bs2DsRhoShape"]["BeautyMass"]["b"]["Run2"]["All"] -
            diffBdBs
        },
        "Fixed": True
    }

    configdict["Bd2DsRhoShape"]["BeautyMass"]["csi"] = configdict[
        "Bs2DsRhoShape"]["BeautyMass"]["csi"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["frac"] = configdict[
        "Bs2DsRhoShape"]["BeautyMass"]["frac"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["sigma"] = configdict[
        "Bs2DsRhoShape"]["BeautyMass"]["sigma"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": -diffBdBs
        },
        "Fixed": True
    }

    configdict["Bs2DsstRhoShape"] = {}
    configdict["Bs2DsstRhoShape"]["BeautyMass"] = {}
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "All": 6.0000e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["a"] = {
        "Run2": {
            "All": 3.0000e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["b"] = {
        "Run2": {
            "All": 5.0910e+03
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["csi"] = {
        "Run2": {
            "All": 5.0000e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "All": 9.9643e-01
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["sigma"] = {
        "Run2": {
            "All": 4.2153e+01
        },
        "Fixed": True
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["shift"] = {
        "Run2": {
            "All": 0.0
        },
        "Fixed": True
    }

    configdict["Bd2DsPiShape"] = {}
    configdict["Bd2DsPiShape"]["BeautyMass"] = {}
    if configdict["SignalShape"]["BeautyMass"][
            "type"] == "IpatiaPlusJohnsonSU":
        configdict["Bd2DsPiShape"]["BeautyMass"][
            "type"] = "ShiftedSignalIpatiaJohnsonSU"
        configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
            "Run1": {
                "All": -diffBdBs
            },
            "Run2": {
                "All": -diffBdBs
            },
            "Fixed": True
        }
    else:
        configdict["Bd2DsPiShape"]["BeautyMass"]["type"] = "ShiftedSignal"
        configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
            "Run2": {
                "All": diffBdBs
            },
            "Fixed": True
        }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale1"] = {
        "Run1": {
            "All": 1.00808721452
        },
        "Run2": {
            "All": 1.00808721452
        },
        "Fixed": True
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale2"] = {
        "Run1": {
            "All": 1.03868673310
        },
        "Run2": {
            "All": 1.03868673310
        },
        "Fixed": True
    }

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run1": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Run2": {
            "All": {
                "Both": {
                    "CentralValue": 0.5,
                    "Range": [0.0, 1.0]
                }
            }
        },
        "Fixed": False
    }

    configdict["Yields"] = {}
    configdict["Yields"]["Bd2DPi"] = {
        "Run2": {
            "KKPi": [2972.6, 300.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bd2DsPi"] = {
        "2011": {
            "KKPi": 1077.43
        },
        "2012": {
            "KKPi": 1077.43
        },
        "2015": {
            "KKPi": 196.60
        },
        "2016": {
            "KKPi": 1077.43
        },
        "2017": {
            "KKPi": 982.239
        },
        "2018": {
            "KKPi": 1275.19
        },
        "Fixed": False
    }
    configdict["Yields"]["Bd2DsstPi"] = {
        "2011": {
            "KKPi": 1077.43
        },
        "2012": {
            "KKPi": 1077.43
        },
        "2015": {
            "KKPi": 196.60
        },
        "2016": {
            "KKPi": 1077.43
        },
        "2017": {
            "KKPi": 982.239
        },
        "2018": {
            "KKPi": 1275.19
        },
        "Fixed": False
    }
    configdict["Yields"]["Bd2DsRho"] = {
        "2011": {
            "KKPi": 1077.43
        },
        "2012": {
            "KKPi": 1077.43
        },
        "2015": {
            "KKPi": 196.60
        },
        "2016": {
            "KKPi": 1077.43
        },
        "2017": {
            "KKPi": 982.239
        },
        "2018": {
            "KKPi": 1275.19
        },
        "Fixed": False
    }
    configdict["Yields"]["Lb2LcPi"] = {
        "Run2": {
            "KKPi": [2230.0, 223.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsK"] = {
        "Run2": {
            "KKPi": [1289.0, 130.0]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstPi"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Bs2DsRho"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Bs2DsstRho"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }
    configdict["Yields"]["Signal"] = {
        "2011": {
            "KKPi": 100000.0
        },
        "2012": {
            "KKPi": 100000.0
        },
        "2015": {
            "KKPi": 100000.0
        },
        "2016": {
            "KKPi": 100000.0
        },
        "2017": {
            "KKPi": 100000.0
        },
        "2018": {
            "KKPi": 100000.0
        },
        "Fixed": False
    }

    #############################################################################################################
    ################################              Plotting              #########################################
    #############################################################################################################

    from ROOT import (kRed, kBlue, kOrange, kMagenta, kGreen, kSolid)

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Lb2LcPi",
        "Bd2DPi",
        "Bs2DsK",
        "Bd2DsPi",
        "Bd2DsstPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#d7301f",
        "#cccccc",
        "#238443",
        "#fbb4b9",
        "#08519c",
        "#c51b8a",
        "#7a0177",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
    ]
    configdict["PlotSettings"]["pattern"] = [
        kSolid, 3004, kSolid, 3005, kSolid, 3005, 3005, kSolid, kSolid, kSolid
    ]
    configdict["PlotSettings"]["patterncolor"] = [
        "#d7301f",
        "#969696",
        "#238443",
        "#feebe2",
        "#08519c",
        "#feebe2",
        "#feebe2",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2
    }

    return configdict
