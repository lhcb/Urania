#ifndef Lb2XhModels_H
#define Lb2XhModels_H 1


#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooResolutionModel.h"
#include "RooWorkspace.h"
#include "RooAddPdf.h"
#include "RooHistPdf.h"
#include "RooProdPdf.h"
#include "RooArgList.h"

namespace Lb2XhModels {
  

  //===============================================================================
  // Background 3D model for Lb->Dsp mass fitter.
  //===============================================================================

  RooAbsPdf* build_Lb2Dsp_BKG( RooWorkspace* work,
				  RooWorkspace* workInt,
				  std::vector <RooAbsReal*> obs,
				  std::vector <TString> types,
				  TString &samplemode,
				  std::vector<TString> merge,
				  bool debug);
  


}

#endif
