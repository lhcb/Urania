/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//---------------------------------------------------------------------------//
//                                                                           //
//  General utilities                                                        //
//                                                                           //
//  Header file                                                              //
//                                                                           //
//  Authors: Agnieszka Dziurda, Eduardo Rodrigues                            //
//  Date   : 12 / 04 / 2012                                                  //
//                                                                           //
//---------------------------------------------------------------------------//

#ifndef B2DXFITTERS_SFITUTILS_H
#  define B2DXFITTERS_SFITUTILS_H 1

// STL includes
#  include <iostream>
#  include <string>
#  include <vector>

// ROOT and RooFit includes
#  include "MDFitterSettings.h"
#  include "PlotSettings.h"
#  include "RooAbsData.h"
#  include "RooAbsPdf.h"
#  include "RooDataHist.h"
#  include "RooDataSet.h"
#  include "RooHistPdf.h"
#  include "RooKeysPdf.h"
#  include "RooRealVar.h"
#  include "RooWorkspace.h"
#  include "TCut.h"
#  include "TFile.h"
#  include "TH1F.h"
#  include "TString.h"
#  include "TTree.h"

namespace SFitUtils {
  /**
   * Read observables tVar, tagVar, tagOmegaVar, idVar from sWeights file
   * Name of file is read from filesDirand signature sig
   * time_{up,down} - range for tVar
   * part means mode (DsPi, DsK and so on)
   *
   * @param storageFile file to be used as RooTreeDataStore backend. Defaults to
   *                    nullptr to use VectorStore backend.
   * @param maxEntries max number of entries to read from file. Read all if <= 0
   *                   (default).
   */
  RooWorkspace* ReadDataFromSWeights( TString& pathFile, TString& treeName, MDFitterSettings* mdSet,
                                      std::vector<TString> sm, TString hypo, bool weighted = true, bool toys = false,
                                      bool applykfactor = false, bool sWeightsCorr = false,
                                      RooWorkspace* workspace = NULL, TString label = "", bool debug = false,
                                      TFile* storageFile = nullptr, Long64_t maxEntries = 0 );

  //===========================================================================
  // Create Mistag templates
  //===========================================================================
  RooArgList* CreateMistagTemplates( RooDataSet* data, MDFitterSettings* mdSet, Int_t bins, bool save = false,
                                     bool debug = false );

  //===========================================================================
  // Create Mistag templates for different taggers
  //===========================================================================
  RooArgList* CreateDifferentMistagTemplates( RooDataSet* data, MDFitterSettings* mdSet, Int_t bins, bool save = false,
                                              TString label = "", bool debug = false );

  //===========================================================================
  // Copy Data for Toys, change RooCategory to RooRealVar
  //===========================================================================
  RooDataSet* CopyDataForToys( TTree* tree, TString& mVar, TString& mDVar, TString& PIDKVar, TString& tVar,
                               TString& terrVar, TString& tagVar, TString& tagOmegaVar, TString& idVar,
                               TString& trueIDVar, TString& dataName, bool debug = false );

  RooWorkspace* ReadLbLcPiFromSWeights( TString& pathFile, TString& treeName, double P_down, double P_up,
                                        double PT_down, double PT_up, double nTr_down, double nTr_up, double PID_down,
                                        double PID_up, TString& mVar, TString& mDVar, TString& pVar, TString& ptVar,
                                        TString& nTrVar, TString& pidVar, RooWorkspace* workspace = NULL,
                                        PlotSettings* plotSet = NULL, bool debug = false );

} // namespace SFitUtils

//=============================================================================

#endif // B2DXFITTERS_SFITUTILS_H

// vim: ts=8
