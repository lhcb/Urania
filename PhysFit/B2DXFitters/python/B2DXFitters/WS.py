###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
@file WS.py

@author Manuel Schiller <manuel.schiller@nikhef.nl>
@date 2014-04-29

@brief build Gaussian constraints
"""
from __future__ import print_function

from ROOT import RooFit


def WS(ws, obj, *opts):
    """ "swallow" object into a workspace, returns swallowed object """
    name = obj.GetName()
    wsobj = ws.obj(name)
    if obj.InheritsFrom('RooAbsArg') or obj.InheritsFrom(
            'RooAbsData') or obj.InheritsFrom('RooAbsReal'):
        if None == wsobj:
            getattr(ws, 'import')(obj, *opts)
            wsobj = ws.obj(name)
        else:
            print("Object {} already contained in workspace, wont import.".
                  format(name))
            if wsobj.Class() != obj.Class():
                raise TypeError()
    elif obj.InheritsFrom('RooArgSet'):
        if None == wsobj:
            ws.defineSet(name, obj, True)
            wsobj = ws.set(name)
        else:
            print("Object {} already contained in workspace, wont import.".
                  format(name))
            if wsobj.Class() != obj.Class():
                raise TypeError()
    else:
        if None == wsobj:
            getattr(ws, 'import')(obj, name, *opts)
            wsobj = ws.obj(name)
        else:
            print("Object {} already contained in workspace, wont import.".
                  format(name))
            if wsobj.Class() != obj.Class():
                raise TypeError()
    return wsobj
