###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import sys


def setEnv():

    config = "../data/settings/fakeconfing.py"
    last = config.rfind("/")
    directory = config[:last + 1]
    configName = config[last + 1:]
    p = configName.rfind(".")
    configName = configName[:p]

    import sys
    sys.path.append(directory)

    myconfigfilegrabber = __import__(
        configName, fromlist=['getconfig']).getconfig
    myconfigfile = myconfigfilegrabber()

    from B2DXFitters.MDFitSettingTranslator import Translator
    mdt = Translator(myconfigfile, "MDSettings", True)
    MDSettings = mdt.getConfig()
