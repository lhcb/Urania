###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# --------------------------------------------------------------------------- #
#                                                                             #
#   Python script to compare datas or pdfs                                    #
#                                                                             #
#   Example usage:                                                            #
#      python comparePDF.py                                                   #
#                                                                             #
#   Author: Agnieszka Dziurda                                                 #
#   Date  : 28 / 06 / 2012                                                    #
#                                                                             #
# --------------------------------------------------------------------------- #

# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
""":"
# This part is run by the shell. It does some setup which is convenient to save
# work in common use cases.

# make sure the environment is set up properly
if test -n "$CMTCONFIG" \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersDict.so \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersLib.so; then
    # all ok, software environment set up correctly, so don't need to do
    # anything
    true
else
    if test -n "$CMTCONFIG"; then
        # clean up incomplete LHCb software environment so we can run
        # standalone
        echo Cleaning up incomplete LHCb software environment.
        PYTHONPATH=`echo $PYTHONPATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export PYTHONPATH
        LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
	export LD_LIBRARY_PATH
        exec env -u CMTCONFIG -u B2DXFITTERSROOT "$0" "$@"
    fi
    # automatic set up in standalone build mode
    if test -z "$B2DXFITTERSROOT"; then
        cwd="$(pwd)"
        # try to find from where script is executed, use current directory as
        # fallback
        tmp="$(dirname $0)"
        tmp=${tmp:-"$cwd"}
        # convert to absolute path
        tmp=`readlink -f "$tmp"`
        # move up until standalone/setup.sh found, or root reached
        while test \( \! -d "$tmp"/standalone \) -a -n "$tmp" -a "$tmp"\!="/"; do
            tmp=`dirname "$tmp"`
        done
        if test -d "$tmp"/standalone; then
            cd "$tmp"/standalone
            . ./setup.sh
        else
            echo `basename $0`: Unable to locate standalone/setup.sh
            exit 1
        fi
            cd "$cwd"
        unset tmp
        unset cwd
    fi
fi
# figure out which custom allocators are available
# prefer jemalloc over tcmalloc
for i in libjemalloc libtcmalloc; do
    for j in `echo "$LD_LIBRARY_PATH" | tr ':' ' '` \
            /usr/local/lib /usr/lib /lib; do
        for k in `find "$j" -name "$i"'*.so.?' | sort -r`; do
            if test \! -e "$k"; then
                continue
            fi
            echo adding $k to LD_PRELOAD
            if test -z "$LD_PRELOAD"; then
                export LD_PRELOAD="$k"
                break 3
            else
                export LD_PRELOAD="$LD_PRELOAD":"$k"
                break 3
            fi
        done
    done
done
# set batch scheduling (if schedtool is available)
schedtool="`which schedtool 2>/dev/zero`"
if test -n "$schedtool" -a -x "$schedtool"; then
    echo "enabling batch scheduling for this job (schedtool -B)"
    schedtool="$schedtool -B -e"
else
    schedtool=""
fi

# set ulimit to protect against bugs which crash the machine: 2G vmem max,
# no more then 8M stack
ulimit -v $((2048 * 1024))
ulimit -s $((   8 * 1024))

# trampoline into python
exec $schedtool /usr/bin/time -v env python -O -- "$0" "$@"
"""
from __future__ import print_function

__doc__ = """ real docstring """
# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
from B2DXFitters import *
from ROOT import *

from optparse import OptionParser
from math import pi, log
import os, sys, gc

from B2DXFitters.settings import setEnv
setEnv()

gROOT.SetBatch()


#------------------------------------------------------------------------------
def runComparePDF(debug, fileName, workName, obs):

    work = GeneralUtils.LoadWorkspace(
        TString(fileName), TString(workName), debug)
    work.Print("v")

    obs = GeneralUtils.GetObservable(work, TString(obs), debug)
    gROOT.SetBatch()

    pdf_lb2lcpi = work.pdf("Lb2LcPiEPDF_m_both_nonres_2018")
    pdf_bd2dpi = work.pdf("Bd2DPiEPDF_m_both_nonres_2018")
    pdf_bs2dsk = work.pdf("Bs2DsKEPDF_m_both_nonres_2018")
    pdf_bd2dspi = work.pdf("PhysBkgBd2DsPiPdf_m_both_nonres_2018_Tot")
    pdf_bs2dsstpi = work.pdf("PhysBkgBs2DsstPiPdf_m_both_nonres_2018_Tot")

    from B2DXFitters import TLatexUtils

    color = [kRed, kOrange, kGreen + 3, kBlue - 9, kBlue - 9]
    desc = [
        str(TLatexUtils.DecDescrToTLatex("Lb2LcPi")),
        str(TLatexUtils.DecDescrToTLatex("Bd2DPi")),
        str(TLatexUtils.DecDescrToTLatex("Bs2DsK")),
        str("B_{d}^{0} #rightarrow D_{s}^{-} #pi^{+}"),
        str("B_{s}^{0} #rightarrow D_{s}^{*-} #pi^{+}")
    ]

    print(desc)

    frame = obs.frame(5300, 5550)
    frame.GetYaxis().SetTitleOffset(0.9)
    frame.GetXaxis().SetTitleOffset(1.1)
    frame.GetXaxis().SetLabelOffset(0.02)
    frame.GetYaxis().SetTitleSize(0.08)
    frame.GetYaxis().SetLabelSize(0.06)
    frame.GetXaxis().SetTitleSize(0.08)
    frame.GetXaxis().SetLabelSize(0.06)
    frame.GetXaxis().SetTitleFont(132)
    frame.GetXaxis().SetLabelFont(132)
    frame.GetYaxis().SetTitleFont(132)
    frame.GetYaxis().SetLabelFont(132)
    frame.GetXaxis().SetNdivisions(508)
    frame.GetYaxis().SetTitle("a.u")
    frame.GetXaxis().SetTitle("m(D_{s}^{-}#pi^{+}) MeV/c^{2}")
    frame.SetTitle("")

    legend = TLegend(0.50, 0.45, 0.88, 0.85)
    legend.SetTextSize(0.07)
    legend.SetTextFont(12)
    legend.SetFillColor(4000)
    legend.SetShadowColor(0)
    legend.SetBorderSize(0)
    legend.SetTextFont(132)

    line = [TLine(), TLine(), TLine(), TLine(), TLine()]
    for i in range(0, 5):
        line[i].SetLineColor(color[i])
        line[i].SetLineWidth(4)
        if i == 4:
            line[i].SetLineStyle(kDashed)
        legend.AddEntry(line[i], desc[i], "L")

    canv = TCanvas("canv", "canv", 1200, 1000)
    canv.SetBottomMargin(0.20)
    canv.SetLeftMargin(0.15)
    canv.SetTopMargin(0.10)
    canv.SetRightMargin(0.05)
    canv.SetFillColor(0)
    canv.cd()
    pdf_lb2lcpi.plotOn(frame, RooFit.LineColor(kRed))
    pdf_bd2dpi.plotOn(frame, RooFit.LineColor(kOrange))
    pdf_bs2dsk.plotOn(frame, RooFit.LineColor(kGreen + 3))
    pdf_bd2dspi.plotOn(frame, RooFit.LineColor(kBlue - 9),
                       RooFit.Normalization(0.25, RooAbsReal.Relative))
    pdf_bs2dsstpi.plotOn(frame, RooFit.LineColor(kBlue - 9),
                         RooFit.LineStyle(kDashed),
                         RooFit.Normalization(0.25, RooAbsReal.Relative))

    frame.Draw()
    legend.Draw("SAME")

    canv.Print("templates_beautymass.pdf")

    ###########################################################

    obs2 = GeneralUtils.GetObservable(work, TString("CharmMass"), debug)

    frame2 = obs2.frame()
    frame2.GetYaxis().SetTitleOffset(0.9)
    frame2.GetXaxis().SetTitleOffset(1.1)
    frame2.GetXaxis().SetLabelOffset(0.02)
    frame2.GetYaxis().SetTitleSize(0.08)
    frame2.GetYaxis().SetLabelSize(0.06)
    frame2.GetXaxis().SetTitleSize(0.08)
    frame2.GetXaxis().SetLabelSize(0.06)
    frame2.GetXaxis().SetTitleFont(132)
    frame2.GetXaxis().SetLabelFont(132)
    frame2.GetYaxis().SetTitleFont(132)
    frame2.GetYaxis().SetLabelFont(132)
    frame2.GetXaxis().SetNdivisions(508)
    frame2.GetYaxis().SetTitle("a.u")
    frame2.GetXaxis().SetTitle("m(K^{-}K^{+}#pi^{-}) MeV/c^{2}")
    frame2.SetTitle("")

    legend2 = TLegend(0.50, 0.30, 0.88, 0.50)
    legend2.SetTextSize(0.07)
    legend2.SetTextFont(12)
    legend2.SetFillColor(4000)
    legend2.SetShadowColor(0)
    legend2.SetBorderSize(0)
    legend2.SetTextFont(132)

    legend2.AddEntry(line[0], desc[0], "L")
    legend2.AddEntry(line[1], desc[1], "L")

    canv2 = TCanvas("canv", "canv", 1200, 1000)
    canv2.SetBottomMargin(0.20)
    canv2.SetLeftMargin(0.15)
    canv2.SetTopMargin(0.10)
    canv2.SetRightMargin(0.05)
    canv2.SetFillColor(0)
    canv2.cd()
    pdf_lb2lcpi.plotOn(frame2, RooFit.LineColor(kRed),
                       RooFit.Normalization(10.0, RooAbsReal.Relative))
    pdf_bd2dpi.plotOn(frame2, RooFit.LineColor(kOrange),
                      RooFit.Normalization(10.0, RooAbsReal.Relative))

    frame2.Draw()
    legend2.Draw("SAME")

    canv2.Print("templates_charmmass.pdf")


#------------------------------------------------------------------------------
_usage = '%prog [options]'

parser = OptionParser(_usage)

parser.add_option(
    '-d',
    '--debug',
    action='store_true',
    dest='debug',
    default=False,
    help='print debug information while processing')

parser.add_option(
    '--name1', dest='name1', default='PIDKShape_Bs2DsPi_up_nonres')

parser.add_option(
    '--file1',
    dest='file1',
    default='WS_MDFit_Bs2DsPi_All_PIDCorr_BKGCAT_13.root')

parser.add_option('--work1', dest='work1', default='FitMeToolWS')
parser.add_option('--text1', dest='text1', default='text1')

parser.add_option(
    '--name2', dest='name2', default='PIDKShape_Bs2DsPi_up_phipi')

parser.add_option(
    '--file2',
    dest='file2',
    default='work_dspi_pid_53005800_PIDK0_5M_BDTGA.root')

parser.add_option('--work2', dest='work2', default='workspace')

parser.add_option('--text2', dest='text2', default='text2')

parser.add_option('--obs', dest='obs', default='lab1_PIDK')

parser.add_option('--data', dest='data', action='store_true', default=False)

# -----------------------------------------------------------------------------

if __name__ == '__main__':
    (options, args) = parser.parse_args()

    if len(args) > 0:
        parser.print_help()
        exit(-1)

    import sys
    sys.path.append("../data/")

    runComparePDF(options.debug, options.file1, options.work1, options.obs)
# -----------------------------------------------------------------------------
