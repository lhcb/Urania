# The URANIA project

Urania is, together with [Castelao](https://gitlab.cern.ch/lhcb/Castelao), a high-level physics analysis software repository (or collection of packages used by various working groups) for the LHCb experiment, based on heptools. Documentation can be found at [this link](https://lhcbdoc.web.cern.ch/lhcbdoc/urania/). 

## Use this project from the nightlies

```
lb-run -c best --nightly lhcb-head Urania/HEAD bash
```

In case the ```lhcb-head``` slot has not been deployed to CVMFS yet, you might need to specify a different day:

```
lb-run -c best --nightly lhcb-head/Yesterday Urania/HEAD bash
```

## Tips and tricks

* <b>Solving merge conflicts</b>: please have a look to the [documentation](https://lhcb-core-doc.web.cern.ch/lhcb-core-doc/Development.html#merge-conflict-in-gitlab) (cloning the whole repository for this task is acceptable).

## Mailing list

For issues, questions and problems; please send an e-mail to:

[<b>lhcb-urania-developers@cern.ch</b>](mailto:lhcb-urania-developers@cern.ch)

## About 

Urania (Ancient Greek: Οὐρανία, Ourania; meaning "heavenly" or "of heaven"), also spelt Ourania, was, in Greek mythology, the muse of astronomy and a daughter of Zeus by Mnemosyne and also a great granddaughter of Uranus ([+info](https://en.wikipedia.org/wiki/Urania)). 
<b>How to correctly pronounce Urania?</b> Click [here](https://www.youtube.com/watch?v=H7iD_22t_So).

